class Db {

  constructor(configFile, configKey) {
    this.dbConfigFile = configFile
    this.mysql = require('mysql')
    this.fs = require('fs')
    this.dbConfig = this.getDbConfig(configKey)
    if (this.dbConfig) {
      this.pool = this.mysql.createPool({
        connectionLimit: 140,
        host: this.dbConfig.config.host,
        user: this.dbConfig.config.user,
        password: this.dbConfig.config.password,
        database: this.dbConfig.config.database
      })
    } else this.pool = null;
  }

  getDbConfig(key) {
    // console.log(key)
    let dbConfigData = this.fs.readFileSync(this.dbConfigFile, {
      encoding: 'utf8'
    })
    // console.log(dbConfigData)
    try {
      let dbConfigs = (JSON.parse(dbConfigData)).db; // console.log(dbConfigs)
      for (let i = 0; i < dbConfigs.length; i++) {
        let config = dbConfigs[i]; // console.log(config)
        if (config.key == key) return config;
      }
      return null;
    } catch (err) {
      console.error(err);
      return null;
    }
  }

  getRooms(callback) {
    let sql = "SELECT rid, name, open, date_create, gid FROM rooms " +
      "WHERE open = 1"; //console.log(sql)
    this.executeQuery(sql, callback);
  }

  executeQuery(sql, callback) {
    // console.log(this.pool)
    if (this.pool) {
      this.pool.getConnection(function (err, connection) {
        // console.log(err)
        if (connection) {
          connection.query(sql, function (error, results, fields) {
            connection.release()
            if (error) throw error;
            if (typeof callback == 'function') callback(error, results, fields)
          })
        } else if (typeof callback == 'function') callback(err)
      });
    }
  }

  insertMessage(message, rid, uid, callback) {
    message = message.replace(/\\/g, "\\\\");
    message = message.replace(/\'/g, "\\'");
    let sql = "INSERT INTO messages (message, rid, uid) VALUES ('" + message + "','" + rid + "','" + uid + "')";
    this.executeQuery(sql, callback);
  }

  insertChannelMessage(message, room, user, channel, callback) {
    if (this.pool) {
      this.pool.getConnection(function (err, connection) { // console.log(err)
        if (connection) {
          connection.beginTransaction(function (err) {
            if (err) {
              throw err;
            }
            message = message.replace(/\\/g, "\\\\");
            message = message.replace(/\'/g, "\\'");
            let sql = "INSERT INTO messages (message, rid, uid) VALUES ('" + message + "','" + room.rid + "','" + user.uid + "')";
            connection.query(sql, function (error, results, fields) {
              if (error) {
                return connection.rollback(function () {
                  throw error;
                });
              }
              // console.log(results)
              let mid = results.insertId;
              sql = "INSERT INTO messages_channel (mid, cid) VALUES ('" + mid + "','" + channel.cid + "')";
              connection.query(sql, function (error, results, fields) {
                if (error) {
                  return connection.rollback(function () {
                    throw error;
                  });
                }
                // console.log(results)
                connection.commit(function (err) {
                  if (err) {
                    return connection.rollback(function () {
                      throw err;
                    });
                  }
                  // console.log(results)
                  connection.release();
                  if (typeof callback == 'function')
                    callback(err, results, fields);
                });
              });
            });
          });
        } else if (typeof callback == 'function') callback(err)
      });
    }
  }

  getRoomMessages(rid, callback) {
    let sql = "SELECT * FROM (SELECT m.mid, m.message, m.mdate, m.uid, u.username FROM messages m LEFT JOIN users u ON u.uid = m.uid ";
    sql += "WHERE m.rid = '" + rid + "' AND m.mid NOT IN (SELECT mid FROM messages_channel) ORDER BY m.mid DESC LIMIT 100) m ORDER BY m.mid ASC"
    this.executeQuery(sql, callback);
  }

}

module.exports = Db
module.exports = {
  getClients: function(io) {
    let clients = [];
    let connecteds = io.of('/kit').connected;
    let keys = Object.keys(connecteds);
    keys.forEach(key => {
      let client = connecteds[key];
      clients.push({
        key: key,
        id: client.conn.id
      })
    });
    return clients;
  },
  getRooms: function (socket, dbRooms) {
    let roomKeys = Object.keys(socket.adapter.rooms);
    dbRooms.forEach(dbRoom => {
      dbRoom.users = [];
      roomKeys.forEach(room => {
        let r = socket.adapter.rooms[room];
        if(r) {
          let sKeys = Object.keys(r['sockets']);
          let users = [];
          sKeys.forEach(s => {
            users.push(s);
          });
          if(room == dbRoom.name) {
            dbRoom.users = users;
            return;
          }
        }
      }); // console.log(rooms);
    });
    return dbRooms;
  },
  getServerRooms: function(socket) {
    let roomKeys = Object.keys(socket.adapter.rooms);
    let rooms = [];
    roomKeys.forEach(room => {
      let r = socket.adapter.rooms[room]
      let users = [];
      if(r) {
        let sKeys = Object.keys(r['sockets']);
        sKeys.forEach(s => {
          users.push(s);
        });
      } 
      rooms.push({
        name: room,
        users: users
      });
    }); // console.log(rooms);
    return rooms;
  },
  getRoomUsers: function(socket, room) { // console.log(socket, room);
    if(!room) return [];
    let users = [];
    let r = socket.adapter.rooms[room] // console.log(r)
    if(r) {
      let sKeys = Object.keys(r['sockets']);
      sKeys.forEach(s => {
        // users.push(s.match(/\#(.+)$/i)[1]); // strip namespace
        users.push(s)
      });
    }
    return users;
  },
  out: function(tag, ...data) {
    console.log('\x1b[93m%s\x1b[0m','\n\n[T:' + Date.now() + '][E:' + tag + ']')
    console.log('\x1b[092m%s\x1b[0m', 'Data:')
    data.forEach(d => {
      console.log(d)  
    });
  }
};
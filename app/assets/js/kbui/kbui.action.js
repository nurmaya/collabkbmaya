// Constructor.
var Interface = function (name, methods) {
  if (arguments.length != 2) {
    throw new Error("Interface constructor called with " + arguments.length +
      "arguments, but expected exactly 2.");
  }
  this.name = name;
  this.methods = [];
  for (var i = 0, len = methods.length; i < len; i++) {
    if (typeof methods[i] !== 'string') {
      throw new Error("Interface constructor expects method names to be " +
        "passed in as a string.");
    }
    this.methods.push(methods[i]);
  }
};

// Static class method.
Interface.ensureImplements = function (object) {
  if (arguments.length < 2) {
    throw new Error("Function Interface.ensureImplements called with " +
      arguments.length + "arguments, but expected at least 2.");
  }
  for (var i = 1, len = arguments.length; i < len; i++) {
    var iface = arguments[i];
    if (iface.constructor !== Interface) {
      throw new Error("Function Interface.ensureImplements expects arguments" +
        "two and above to be instances of Interface.");
    }
    for (var j = 0, methodsLen = iface.methods.length; j < methodsLen; j++) {
      var method = iface.methods[j];
      if (!object[method] || typeof object[method] !== 'function') {
        throw new Error("Function Interface.ensureImplements: object " +
          "does not implement the " + iface.name +
          " interface. Method " + method + " was not found.");
      }
    }
  }
};

var ReversibleCommand = new Interface('ReversibleCommand', ['redo', 'undo']);
ReversibleCommand.undoStack = [];
ReversibleCommand.redoStack = [];

class Action {

  constructor() {
    this.undoButton = '#bt-undo';
    this.redoButton = '#bt-redo';
    this.enabled = true;
    this.eventListeners = [];
    return this;
  }

  enable(enabled) {
    this.enabled = enabled;
  }

  attachEventListener(listener) { // console.log(listener);
    this.eventListeners.push(listener);
  }

  push(command) { // console.log(this);

    if(!this.enabled) return;

    Interface.ensureImplements(command, ReversibleCommand);
    this.command = command;
    ReversibleCommand.undoStack.push(this.command);
    ReversibleCommand.redoStack = [];
    this.updateUI();
  }

  undo() { // console.log(this);
    if (ReversibleCommand.undoStack.length > 0) {
      let command = ReversibleCommand.undoStack.pop();
      let undoCommand = command.undo();
      this.eventListeners.forEach(listener => {
        console.log(listener);
        if(listener != null && typeof listener.onActionEvent == 'function')
        listener.onActionEvent(undoCommand.action, undoCommand.data);
      });
      ReversibleCommand.redoStack.push(command);
    }
    this.updateUI();
  }

  redo() {
    if (ReversibleCommand.redoStack.length > 0) {
      let command = ReversibleCommand.redoStack.pop();
      let redoCommand = command.redo();
      this.eventListeners.forEach(listener => {
        if(listener != null && typeof listener.onActionEvent == 'function')
        listener.onActionEvent(redoCommand.action, redoCommand.data);
      });
      ReversibleCommand.undoStack.push(command);
    }
    this.updateUI();
  }

  updateUI() {
    if (ReversibleCommand.undoStack.length) $(this.undoButton).removeClass('disabled');
    else $(this.undoButton).addClass('disabled');
    if (ReversibleCommand.redoStack.length) $(this.redoButton).removeClass('disabled');
    else $(this.redoButton).addClass('disabled');
    return this;
  }

  clearStack() {
    ReversibleCommand.undoStack = [];
    ReversibleCommand.redoStack = [];
    this.updateUI();
  }

}
class KitBuildApp {

  constructor(canvas) {
    this.canvas = canvas;
    this.gui = new GUI();
    this.session = new Session(baseUrl);
    this.ajax = Ajax.ins(this.gui);
    this.controller = 'denny';
    this.page = 'icmapkit';
    this.nextPage = 'postmapping';

    this.material = {
      mid: null,
      name: null,
      content: null
    }

    this.goalmap = {
      gmid: null,
      name: null
    }

    this.eventListeners = [];

    this.handleEvent();
    this.canvas.attachEventListener(this);

  }

  getCanvas() {
    return this.canvas;
  }

  getGUI() {
    return this.gui;
  }

  getSession() {
    return this.session;
  }

  handleEvent() {
    let app = this;

    $('#bt-logout').on('click', function() {
      app.gui.confirm('Logout? Sisa waktu yang disediakan untuk concept mapping akan tetap berjalan.', {
        'positive-callback': function() {
          window.location.href = baseUrl + app.controller + '/signOut';
        }
      });
    })

    $('#bt-show-material').on('click', function () {
      $('#popup-material').fadeToggle({
        duration: 200,
        complete: function () {
          let shown = $(this).is(':visible');
          app.eventListeners.forEach(listener => {
            if (listener && typeof listener.onAppEvent == 'function') {
              if (shown) listener.onAppEvent('show-material');
              else listener.onAppEvent('hide-material');
            }
          })
        }
      });
    });
    $('#popup-material .bt-close').on('click', function () {
      $('#popup-material .check-open').prop('checked', false);
      $('#popup-material').fadeOut({
        duration: 200,
        complete: function () {
          app.eventListeners.forEach(listener => {
            if (listener && typeof listener.onAppEvent == 'function') {
              listener.onAppEvent('hide-material');
            }
          })
        }
      });
    });
    $('#bt-open-map').on('click', function () {
      let mid = app.material.mid;
      let uid = app.user.uid;
      app.ajax.get(baseUrl + 'kitbuildApi/getUserGoalmaps/' + mid + '/' + uid).then(function (goalmaps) {
        // console.log(goalmaps);
        let mapList = '<em>No map found on database.</em>';
        if (goalmaps && goalmaps.length) {
          mapList = '';
          goalmaps.forEach(gm => {
            mapList += '<div class="row align-items-center list list-hover list-pointer justify-content-between pt-2 pb-2 pl-5 pr-5" data-gmid="' + gm.gmid + '" data-name="' + gm.name + '">'
            mapList += '<span>' + gm.name + ' <span class="badge badge-info">' + gm.type + '</span></span>' + '<div><span class="badge badge-warning">Concepts:' + gm.concepts_count + ' Links:' + gm.links_count + '</span>' + ' <span class="badge badge-primary">Saved: ' + gm.create_time + '</span></div>'
            mapList += '</div>'
          });
        }
        $('#modal-open-map .map-list').html(mapList);
      });
      $('#modal-open-map').modal('show');
    });
    $('#modal-open-map .map-list').on('click', '.row', function () {
      let gmid = $(this).data('gmid');
      let name = $(this).data('name');
      // console.log(gmid, name);
      if (app.canvas.getCy().nodes().length) {
        let dialog = app.gui.confirm('You have map on canvas. Opening the map will replace existing map on canvas. Open map "' + name + '"?', {
          'positive-callback': function () {
            app.loadSavedGoalmap(gmid, function () {
              dialog.modal('hide');
              $('#modal-open-map').modal('hide');
            });
          }
        });
        return;
      } else {
        app.loadSavedGoalmap(gmid, function () {
          $('#modal-open-map').modal('hide');
        });
      }
    })
    $('#bt-save').on('click', function () {
      // let uid = app.kit.user.uid;
      let mid = app.material.mid;
      if (mid != null && uid != null) {
        app.saveGoalmapDraft(mid, uid, function(gmid) {
          if (gmid == null) {
            app.gui.notify('Unable to save map', {
              type: 'warning'
            });
            return;
          }
          app.gui.notify('Map saved.', {
            type: 'success'
          })
        });
      } else {
        app.gui.notify('Invalid map data', {type: 'danger'});
      }
    });
    $('#bt-finalize').on('click', function () {
      if (app.getCanvas().getCy().nodes().length != 0) {
        let imageData = app.getCanvas().getCy().png({
          scale: 2,
          full: true
        });
        $('#popup-map').find('img.cmap').attr('src', imageData);
        setTimeout(function () {
          $('#popup-map').show().center().center();
        }, 100);
      } else {
        confirmFinalize();
      }
    });
    $('#popup-map').on('click', '.bt-cancel', function () {
      $('#popup-map').hide();
    });

    $('#popup-map').on('click', '.bt-download', function () {
      let mid = app.material.mid;
      let uid = app.user.uid;
      let username = app.user.username;
      var a = document.createElement("a"); //Create <a>
      a.href = $('#popup-map').find('img.cmap').attr('src');
      a.download = "concept-map-m" + mid + "-u" + uid + "-" + username + ".png"; //File name Here
      a.click(); //Downloaded file
    })
    let confirmFinalize = function () {
      let mid = app.material.mid;
      let uid = app.user.uid;
      if (mid != null && uid != null) {
        let confirm = app.gui.confirm('Save and finalize the map? <br>Any previously saved draft concept maps will be deleted, preserving the finalized one.', {
          'positive-callback': function () {
            confirm.modal('hide');
            $('#modal-map-name').modal('show');
            $('#modal-map-name .map-name-input').val('final-m' + mid + '-u' + uid + '-' + app.user.username).focus().select();
          },
          'negative-callback': function () {
            confirm.modal('hide');
          }
        })
      } else app.gui.notify('Invalid ID', {type: 'danger'});
    }
    $('#popup-map').on('click', '.bt-continue', function () {
      confirmFinalize();
    });
    $('#modal-map-name').on('click', '.bt-ok', function () {
      let mid = app.material.mid;
      let uid = app.user.uid;
      if (mid != null && uid != null) {
        let name = $('#modal-map-name .map-name-input').val().trim()
        if (name == '')
          app.gui.dialog('Concept map name should not be empty.', {width: '350px'})
        else {
          app.saveGoalmapFinalize(mid, uid, name, function (gmid) {
            $('#modal-map-name').modal('hide');
            $('#popup-map').hide();
            app.gui.notify('Map finalized', {
              type: 'success'
            })
            app.eventListeners.forEach(listener => {
              if (listener && typeof listener.onAppEvent == 'function') {
                listener.onAppEvent('finalize-map', {
                  gmid: gmid
                });
              }
            });
            app.session.set('page', app.nextPage, function () {
              window.location.href = baseUrl + app.controller + '/' + app.nextPage;
              return;
            }, function (error) {
              app.gui.dialog('Error: ' + error);
            })
          });
        }
      } else app.gui.dialog('Invalid ID');
    });
    $('.material-content').on('mouseup', function (e) {
      let s = window.getSelection();
      if(s.anchorNode == null) return;
      let oRange = s.getRangeAt(0); //get the text range
      let text = oRange.toString().trim();
      if (text.length) {
        let oRect = oRange.getBoundingClientRect();
        // console.log(s, oRange, oRect);
        // console.log(app.canvas.settings);
        if(!app.canvas.settings.enableConceptCreation)
          $('#popup-selection .menu-item[data-type="concept"]').hide();
        else $('#popup-selection .menu-item[data-type="concept"]').show();
        if(!app.canvas.settings.enableLinkCreation)
        $('#popup-selection .menu-item[data-type="link"]').hide(); 
        else $('#popup-selection .menu-item[data-type="link"]').show();
        $('#popup-selection .selected-text').html(text);
        $('#popup-selection').show()
          .css('top', oRect.top)
          .css('left', oRect.left + oRect.width + 10);
        e.stopImmediatePropagation();
      } else $('#popup-selection').hide();
    });
    $('#popup-selection').on('click', '.menu-item', function () {
      let text = $(this).find('strong').text();
      let type = $(this).data('type');
      $('#input-node-label').val(text);
      app.eventListeners.forEach(listener => {
        if (listener && typeof listener.onAppEvent == 'function') {
          listener.onAppEvent('new-' + type + '-from-selection', {
            type: type,
            text: text
          });
        }
      })
      switch (type) {
        case 'concept':
          $('#bt-new-concept').click();
          $('#input-node-label').val(text);
          break;
        case 'link':
          $('#bt-new-link').click();
          $('#input-node-label').val(text);
          break;
      }
      $('#popup-selection').hide()
    });
    $('.material-content').on('scroll', function () {
      let s = window.getSelection();
      if(s.anchorNode == null) return;
      let oRange = s.getRangeAt(0); //get the text range
      let oRect = oRange.getBoundingClientRect();
      if (oRect.top - $(this).offset().top > -24 &&
        oRect.top < $(this).offset().top + $(this).height()) {
        $('#popup-selection').show();
      } else $('#popup-selection').hide();
      if ($('#popup-selection').is(':visible'))
        $('#popup-selection').css('top', oRect.top).css('left', oRect.left + oRect.width + 10);
    });
    $('#popup-material').on('mouseup', function () {
      $('#popup-selection').hide();
    });
    $(document).on('mouseup', function (e) {
      var container = $("#popup-material");
      var button = $('#bt-show-material')
      if (!container.is(e.target) && container.has(e.target).length === 0 &&
        !button.is(e.target) && button.has(e.target).length === 0) {
        $('#popup-selection').hide();
        if ($('#popup-material .check-open').prop('checked')) return;
        if (container.is(':visible')) {
          container.fadeOut();
          app.eventListeners.forEach(listener => {
            if (listener && typeof listener.onAppEvent == 'function') {
              listener.onAppEvent('hide-material');
            }
          });
        }
      }
    });
  }

  attachEventListener(listener) {
    this.eventListeners.push(listener);
  }

  autoSave() {
    try {
      console.log('Attempting autosave...');
      let app = this;
      let mid = app.material.mid;
      let uid = app.user.uid;
      if(mid && uid) 
        this.saveGoalmapAuto(mid, uid, function(data){
          if(data != null) console.log('Autosave OK. GMID: ' + data);
        });
      else throw 'Invalid ID';
    } catch(error) {
      console.log('autosave error: ' + error);
    }
    setTimeout(this.autoSave.bind(this), 300000);
  }

  // Commanding Kit-Build App Interface

  signIn(uid, username, name, rid, gids) {
    $('#bt-login').hide();
    $('#bt-logout').show();
    $('#bt-logout-username').html(": " + username);
    $('#bt-open-kit').prop('disabled', false);
    // this.kit.enableMessage();
    this.enableSaveDraft();
    this.enableFinalize();
  };

  signOut() {
    $('#bt-login').show();
    $('#bt-logout').hide();
    $('#bt-open-kit').prop('disabled', true);
    $('#bt-show-material').prop('disabled', true);
    this.enableSaveDraft(false);
    this.enableFinalize(false);
    this.unsetMaterial();
  }

  enableMaterial(enabled = true) {
    $('#bt-show-material').prop('disabled', !enabled);
  }

  enableSaveDraft(enable = true) {
    $('#bt-save').prop('disabled', !enable);
    $('#bt-load-draft').prop('disabled', !enable);
  }

  enableFinalize(enable = true) {
    $('#bt-finalize').prop('disabled', !enable);
  }

  loadMaterial(mid, confirmDialog) {
    let app = this;
    app.ajax.get(
      baseUrl + 'kitbuildApi/getMaterialCollabByMid/' + mid
    ).then(function (material) {
      if (confirmDialog) confirmDialog.modal('hide');
      app.session.set('mid', material.mid);
      app.setMaterial(material.mid, material.name, material.content);
      // app.kit.sendMaterial(material.mid, material.name, material.content);
      $('#modal-kit').modal('hide');
    })
    $('#bt-show-material').prop('disabled', false);
  }

  setMaterial(mid, name, content) {
    this.material.mid = mid;
    this.material.name = name;
    this.material.content = content;
    $('#popup-material .material-title').html(this.material.name)
    // /<\/?[a-z][\s\S]*>/i.test(this.material.content);
    $('#popup-material .material-content').html(this.material.content);
  }

  unsetMaterial() {
    this.material.mid = null;
    this.material.name = null;
    this.material.content = null;
  }

  setGoalmap(goalmap) {
    this.goalmap = goalmap;
    this.goalmap.gmid = goalmap.goalmap.gmid;
    this.goalmap.name = goalmap.goalmap.name;
  }

  unsetGoalmap() {
    this.goalmap.gmid = null;
    this.goalmap.name = null;
  }

  saveGoalmap(mid, uid, type, name, callback) { // console.log(name);
    let app = this;
    let goalmaps = {
      name: name ? name : type + '-m' + mid + '-u' + uid,
      type: type,
      mid: mid,
      creator_id: uid,
      updater_id: uid,
      concepts: [],
      links: []
    }
    let cs = app.canvas.getNodes('[type="concept"]');
    let ls = app.canvas.getNodes('[type="link"]');
    let es = app.canvas.getEdges();

    if(cs.length == 0 && ls.length == 0) {
      if(typeof callback == 'function') callback(null);
      return;
    };
    // console.log(cs, ls, es);
    cs.forEach(c => {
      goalmaps.concepts.push({
        cid: c.data.id.substr(2),
        gmid: null,
        label: c.data.name,
        locx: c.position.x,
        locy: c.position.y
      });
    });
    ls.forEach(l => {
      let tl = {
        lid: l.data.id.substr(2),
        gmid: null,
        label: l.data.name,
        locx: l.position.x,
        locy: l.position.y,
        source: null,
        target: null
      };
      es.forEach((e) => {
        if (e.data.source == l.data.id) {
          if (e.data.type == 'right') tl.target = e.data.target.substr(2);
          if (e.data.type == 'left') tl.source = e.data.target.substr(2);
          return;
        }
      });
      goalmaps.links.push(tl);
    })
    app.ajax.post(
      baseUrl + 'kitbuildApi/saveGoalmap',
      goalmaps
    ).then(function (gmid) {
      app.eventListeners.forEach(listener => {
        if (listener && typeof listener.onAppEvent == 'function') {
          listener.onAppEvent('map-save-' + type, {
            gmid: gmid
          });
        }
      })
      if (callback) callback(gmid)
    })
  }

  saveGoalmapDraft(mid, uid, callback) {
    this.saveGoalmap(mid, uid, 'draft', null, callback)
  }

  saveGoalmapFinalize(mid, uid, name, callback) {
    this.saveGoalmap(mid, uid, 'fix', name, callback)
  }

  saveGoalmapAuto(mid, uid, callback) {
    this.saveGoalmap(mid, uid, 'auto', null, callback)
  }

  loadKit(gmid, callback) {
    let app = this;
    app.ajax.get(
      baseUrl + 'kitbuildApi/openKit/' + gmid
    ).then(function (kit) {
      console.log(kit);
      let goalmap = kit.goalmap;
      let concepts = kit.concepts;
      let links = kit.links;
      app.setGoalmap(kit);
      app.session.set('gmid', app.goalmap.gmid);
      let nodes = [];
      concepts.forEach(c => {
        nodes.push({
          group: "nodes",
          data: {
            id: "c-" + c.cid,
            name: c.label,
            type: 'concept'
          },
          position: {
            x: parseInt(c.locx),
            y: parseInt(c.locy)
          }
        })
      })
      // links.forEach(l => {
      //   nodes.push({
      //     group: "nodes",
      //     data: {
      //       id: "l-" + l.lid,
      //       name: l.label,
      //       type: 'link'
      //     },
      //     position: {
      //       x: parseInt(l.locx),
      //       y: parseInt(l.locy)
      //     }
      //   })
      // })
      // console.log(nodes)
      app.getCanvas().getCy().elements().remove();
      app.getCanvas().getCy().add(nodes);
      app.getCanvas().getCy().layout({
        name: 'grid',
        fit: false,
        condense: true,
        stop: function () {
          let nodes = app.getCanvas().getCy().nodes().toArray(); // console.log(nodes);
          let eles = [];
          nodes.forEach(node => {
            eles.push({
              group: 'nodes',
              data: {
                id: node.id(),
                name: node.data('name'),
                type: node.data('type')
              },
              position: {
                x: node.position().x,
                y: node.position().y
              }
            })
          })
          $('#bt-center').click();
        }
      }).run();
      if (typeof callback == 'function') callback();
    })
  }

  loadSavedGoalmap(gmid, callback) {
    let app = this;
    app.ajax.get(
      baseUrl + 'kitbuildApi/openKit/' + gmid
    ).then(function (cmap) {
      if (cmap == null) {
        app.gui.notify('Error loading concept map.')
        return;
      }
      let goalmap = cmap.goalmap;
      let concepts = cmap.concepts;
      let links = cmap.links;
      let eles = [];
      concepts.forEach((c) => {
        eles.push({
          group: 'nodes',
          data: {
            id: 'c-' + c.cid,
            name: c.label,
            type: 'concept'
          },
          position: {
            x: parseInt(c.locx),
            y: parseInt(c.locy)
          }
        })
      })
      links.forEach((l) => {
        eles.push({
          group: 'nodes',
          data: {
            id: 'l-' + l.lid,
            name: l.label,
            type: 'link'
          },
          position: {
            x: parseInt(l.locx),
            y: parseInt(l.locy)
          }
        })
        if (l.source != null) {
          eles.push({
            group: 'edges',
            data: {
              source: 'l-' + l.lid,
              target: 'c-' + l.source,
              type: 'left'
            }
          })
        }
        if (l.target != null) {
          eles.push({
            group: 'edges',
            data: {
              source: 'l-' + l.lid,
              target: 'c-' + l.target,
              type: 'right'
            }
          })
        }
      })
      app.canvas.clearCanvas();
      app.canvas.getCy().add(eles);
      app.gui.notify('Concept map loaded.')
      // app.kit.sendMap(eles);
      $('#bt-center').click();
      app.eventListeners.forEach(listener => {
        if (listener && typeof listener.onAppEvent == 'function') {
          listener.onAppEvent('load-concept-map', {
            gmid: parseInt(goalmap.gmid)
          });
        }
      })
      if (typeof callback == 'function') callback();
    })
  }

  friendlyTime(d) {
    d = Number(d);
    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);

    var hDisplay = h > 0 ? (h < 10 ? "0" + h : h) + ":" : "";
    var mDisplay = m > 0 ? (m < 10 ? "0" + m : m) + ":" : "00:";
    var sDisplay = s > 0 ? (s < 10 ? "0" + s : s) : "00";
    return hDisplay + mDisplay + sDisplay; 
  }

  tick() {
    let app = this;
    this.remaining--;
    // console.log(this.remaining);
    $('#remaining').html(app.friendlyTime(this.remaining) + " left.");
    if (this.remaining > 0) setTimeout(this.tick.bind(this), 1000);
    else {
      let mid = app.material.mid;
      let uid = app.user.uid;
      let username = app.user.username;
      let name = 'final-force-m' + mid + '-u' + uid + '-' + username;
      app.saveGoalmapFinalize(mid, uid, name, function(gmid) {
        app.gui.dialog('Time up. Your map has been saved.<br>Session will be ended in 5s.');
        setTimeout(function() {
          app.logger.log('finalize-map', {
            gmid: gmid
          });
          app.session.setMulti({
            page: app.nextPage,
            seq: app.logger.seq + 1
          }, function () {
            window.location.href = baseUrl + app.controller + '/' + app.nextPage;
          }, function (error) {
            gui.dialog(error, {
              width: '300px'
            });
          });
        }, 5000);
      });
    }
  }

  countdown(remaining) {
    this.remaining = remaining;
    setTimeout(this.tick.bind(this), 1000);
  }

}

jQuery.fn.center = function (parent) {
  parent = parent ? $(parent) : window
  this.css({
    "position": "absolute",
    "top": ((($(parent).height() - this.outerHeight()) / 2) + $(parent).scrollTop() + "px"),
    "left": ((($(parent).width() - this.outerWidth()) / 2) + $(parent).scrollLeft() + "px")
  });
  return this;
}

var BRIDGE = {};

$(function () {

  var canvas = new Canvas('cy', {
    // isDirected: false,
    // enableToolbar: false,
    // enableNodeCreation: false,
    enableConceptCreation: false,
    // enableLinkCreation: false,
    // enableUndoRedo: false,
    // enableZoom: false,
    // enableAutoLayout: false,
    // enableSaveImage: false,
    enableClearCanvas: false,
    // enableNodeTools: false,
    enableNodeModificationTools: false,
    // enableConnectionTools: false,
  }).init();

  let app = new KitBuildApp(canvas); // the app
  let logger = new Logger(uid ? uid : null, seq ? seq : null, sessId ? sessId : null);
  let eventListener = new EventListener(logger, canvas);

  canvas.attachEventListener(eventListener);
  app.attachEventListener(eventListener);

  BRIDGE.app = app;
  BRIDGE.logger = eventListener.getLogger();
  BRIDGE.app.logger = logger;

  BRIDGE.app.session.getAll(function(sess){
    console.log(sess);
    let user = sess.user ? sess.user : null;
    if(!user) return;
    BRIDGE.app.user = user;
    BRIDGE.app.signIn(user.uid, user.username, user.name, user.role_id, user.gids);

    let mid = sess.mid ? sess.mid : null;
    BRIDGE.app.loadMaterial(mid);
    BRIDGE.logger.setMid(mid); // karena ini tetep goalmapping.
  
    let gmid = sess.gmid ? sess.gmid : null;
    BRIDGE.app.loadKit(gmid);
    BRIDGE.app.autoSave();
    BRIDGE.app.countdown(sess.remaining ? sess.remaining : 0);
  });

});
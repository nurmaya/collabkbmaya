var BRIDGE = {};

class Read {

  constructor() {
    this.gui = new GUI();
    this.session = new Session(baseUrl);
    this.ajax = Ajax.ins(this.gui);
    this.controller = 'denny';
    this.nextPage = 'prapretest';
    this.logger = new Logger(uid ? uid : null, seq ? seq : null, sessId ? sessId : null);
    this.user = null;
    console.log(this.logger);
    this.handleEvent(this);

    this.uid = uid ? uid : null;
    this.mid = null;

  }

  handleEvent(app) {

    $('#bt-continue').on('click', function () {

      let confirm = app.gui.confirm('Lanjut ke tahap selanjutnya?', {
        'positive-callback': function () {
          confirm.modal('hide');
          setTimeout(function () {
            app.logger.log('finish-read', {
              uid: uid,
              mid: app.mid
            });
            app.session.setMulti({
              page: app.nextPage,
              seq: app.logger.seq + 1 // because of 'sign-in'
            }, function () {
              window.location.href = baseUrl + app.controller + '/' + app.nextPage;
            }, function (error) {
              gui.dialog(error, {
                width: '300px'
              });
            });
          }, 100);
        }
      })
    });
  }

  friendlyTime(d) {
    d = Number(d);
    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);

    var hDisplay = h > 0 ? (h < 10 ? "0" + h : h) + ":" : "";
    var mDisplay = m > 0 ? (m < 10 ? "0" + m : m) + ":" : "00:";
    var sDisplay = s > 0 ? (s < 10 ? "0" + s : s) : "00";
    return hDisplay + mDisplay + sDisplay; 
  }

  tick() {
    let app = this;
    this.remaining--;
    // console.log(this.remaining);
    $('#remaining').html(app.friendlyTime(this.remaining) + " left.");
    if (this.remaining > 0) setTimeout(this.tick.bind(this), 1000);
    else {
      // this.saveTextAreas();
      // this.saveMultipleChoices();
      app.gui.dialog('Waktu yang disediakan telah habis, Anda akan dialihkan ke halaman selanjutnya dalam waktu 5 detik.');
      setTimeout(function () {
        app.logger.log('finish-read', {
          uid: uid,
          mid: app.mid
        });
        app.session.setMulti({
          page: app.nextPage,
          seq: app.logger.seq + 1 // because of 'sign-in'
        }, function () {
          window.location.href = baseUrl + app.controller + '/' + app.nextPage;
        }, function (error) {
          gui.dialog(error, {
            width: '300px'
          });
        });
      }, 5000);
    }
  }

  countdown(remaining) {
    this.remaining = remaining;
    setTimeout(this.tick.bind(this), 1000);
  }

}

$(function () {
  BRIDGE.app = new Read();
  BRIDGE.app.session.getAll(function(sess){ console.log(sess);
    BRIDGE.app.mid = sess.mid ? sess.mid : null;
    BRIDGE.app.countdown(sess.remaining ? sess.remaining : 0);
  })
});
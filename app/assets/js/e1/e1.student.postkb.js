var BRIDGE = {};

class PostKB {
  constructor(canvas) {

    this.gui = new GUI();
    this.ajax = Ajax.ins(this.gui);
    this.analyzerLib = new AnalyzerLib();
    this.session = new Session(baseUrl);
    this.logger = new Logger(uid ? uid : null, seq ? seq : null, sessId ? sessId : null); console.log(this.logger);
    
    this.controller = 'e1'
    this.nextPage = 'sbcollab'

    this.canvas = canvas;
    this.goalmap = {
      goalmap: null,
      concepts: [],
      links: []
    }
    this.learnermap = null;
    this.rid = null;

    this.handleEvent(this);

  }

  getCanvas() {
    return this.canvas;
  }

  handleEvent(app) {

    $('#bt-continue').on('click', function() {
      let confirm = app.gui.confirm("Lanjutkan ke tahap selanjutnya?", {
        'positive-callback': function() {
          app.logger.log('begin-scratch-collab', {
            gmid: app.goalmap.goalmap.gmid,
            lmid: app.learnermap.lmid,
            rid: app.rid,
            uid: uid
          });
          app.session.setMulti({
            lmid: app.learnermap.lmid,
            page: app.nextPage,
            seq: app.logger.seq + 1
          },function(){
            window.location.href = baseUrl + app.controller + '/' + app.nextPage;
          },function(error){
            app.gui.dialog(error);
          })
          confirm.modal('hide')
        }
      })
    });

  }

  loadGoalmap(gmid, callback) {
    let app = this;
    app.ajax.get(baseUrl + 'experimentApi/openKit/' + gmid).then(function (result) { // console.log(response)
      let goalmap = result.goalmap;
      let concepts = result.concepts;
      let links = result.links;
      app.setGoalmap(goalmap, concepts, links);
      if (typeof callback == 'function') callback(result);
    })
  }

  setGoalmap(goalmap, concepts, links) {
    this.goalmap.goalmap = goalmap;
    this.goalmap.concepts = concepts;
    this.goalmap.links = links;
  }

  getLearnermap(gmid, uid, callback) {
    let app = this;
    app.ajax.get(baseUrl + 'experimentApi/getPairLearnermap/' + gmid + '/' + uid).then(function (learnermap) {
      console.log(learnermap);
      app.learnermap = learnermap;
      let compareResult = app.analyzerLib.compare(learnermap, app.goalmap); // console.log(compareResult);
      app.analyzerLib.drawAnalysisLinks(app.getCanvas().getCy(), 
        compareResult.match, compareResult.leave, compareResult.excess, compareResult.miss);
      if (typeof callback == 'function') callback();
    })
  }

  

}

$(function () {

  var canvas = new Canvas('cy', {
    // isDirected: false,
    // enableToolbar: false,
    enableNodeCreation: false,
    // enableConceptCreation: false,
    // enableLinkCreation: false,
    enableUndoRedo: false,
    // enableZoom: false,
    enableAutoLayout: false,
    // enableSaveImage: false,
    enableClearCanvas: false,
    enableNodeTools: false,
    enableNodeModificationTools: false,
    enableConnectionTools: false,
  }).init();

  let app = new PostKB(canvas);

  // create bridges to server-side javascript session handler 
  BRIDGE.app = app;

});
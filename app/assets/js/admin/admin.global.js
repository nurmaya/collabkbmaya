var BRIDGE = {}

class AdminMap {

  // This class act as a BRIDGE between clients and socket.io server
  // Responsible in communication among clients
  // This class also features a simple single-room chatting app 

  constructor(options) {

    // Default settings
    let defaultSettings = {
      port: 3000
    }

    // Override default settings by options
    this.settings = Object.assign({}, defaultSettings, options);

    this.gui = new GUI();
    this.ajax = Ajax.ins(this.gui);

    // Set collaboration server URL
    let url = new URL(window.location.origin);
    url.port = this.settings.port;

    // Setup socket.io
    var socket = io(url.toString() + 'kit', {
      extraHeaders: {
        SameSite: "Lax",
        Secure: false
      }
    });

    // Client identifiers
    this.socketId = null;

    // Get client's socket ID
    let kit = this;
    socket.on('connect', function () {
      kit.socketId = socket.io.engine.id;
    });
    this.socket = socket;

    // Holder for socket event listeners
    this.eventListeners = [];

    this.handleEvent();
    this.handleSocketEvent();
    // this.getRooms();
    $('#bt-refresh-lobby').click();
    $('#bt-refresh-room').click();
    $('#bt-refresh-material').click();
    $('#bt-refresh-group').click();
    $('#bt-refresh-role').click();
    $('#bt-refresh-user').click();

  }

  handleEvent() {

    let app = this;
    let eventListeners = app.eventListeners;

    this.handleLobbyEvent(app);

  }

  handleLobbyEvent(app) {
    $('#bt-refresh-lobby').on('click', function () { // console.log('click');
      app.getUsers(function(users) { // console.log(users);
        app.updateUsers(users);
      });
    });

    $('#lobby-list').on('click', '.bt-message', function() {
      let row = $(this).closest('.row');
      let name = row.data('name');
      let key = row.data('key'); // console.log(name, key);
      $('#bt-lobby-message-send').attr('data-key', key);
      $('#admin-message-socket-id').html(name + '<span class="badge badge-info ml-3">' + key + '</badge>');
    });

    $('#bt-lobby-message-send').on('click', function() {
      let socketId = $(this).attr('data-key');
      let message = $('#lobby-message').val();
      console.log(socketId, message);
      app.sendMessage(socketId, message, {
        delay: $('#admin-message-delay').val(),
        type: $('#admin-message-type').val()
      }, function() {

      });
    });
  }

  // Socket IO Event

  handleSocketEvent() {

    let connectionOK = function (isOK = true, e = 'Connection OK') {
      if (isOK) {
        $('#bt-error').addClass('btn-outline-secondary').removeClass('btn-warning').prop('title', e);
        $('#bt-error i').addClass('fa-check').removeClass('fa-exclamation-triangle');
      } else {
        $('#bt-error').removeClass('btn-outline-secondary').addClass('btn-warning').prop('title', e);
        $('#bt-error i').removeClass('fa-check').addClass('fa-exclamation-triangle')
        $('[data-toggle="tooltip"]').tooltip()
      }
    }

    let kit = this;
    let socket = this.socket;
    let eventListeners = this.eventListeners;

    socket.on('error', (e) => {
      console.log(e)
      $('#bt-error').removeClass('btn-outline-secondary').addClass('btn-warning');
      $('#bt-error i').removeClass('fas-check').addClass('fas-exclamation-triangle');
    });

    socket.on('connect_error', (e) => {
      connectionOK(false, e)
    });

    socket.on('pong', (latency) => {
      console.log('pong', latency)
      connectionOK();
    });

    socket.on('reconnect', function (attemptNumber) {
      kit.socketId = socket.io.engine.id;
      if (kit.room != null) kit.joinRoom(kit.room);
      eventListeners.forEach(listener => {
        if (listener && typeof listener.onKitEvent == 'function') {
          listener.onKitEvent('reconnect', attemptNumber);
        }
      });
      connectionOK();
    });

    // socket.on('room-update', function (rooms) {
    //   eventListeners.forEach(listener => {
    //     if (listener && typeof listener.onKitEvent == 'function') {
    //       listener.onKitEvent('room-update', rooms);
    //     }
    //   });
    //   // kit.updateRooms(rooms);
    // });

    socket.on('user-update', function (users) { // console.log(users)
      eventListeners.forEach(listener => {
        if (listener && typeof listener.onKitEvent == 'function') {
          listener.onKitEvent('user-update', users);
        }
      });
      kit.updateUsers(users);
    });

    // socket.on('chat-message', function (data) {
    //   let message = data.message;
    //   let sender = data.sender == null ? data.socketId : data.sender.username;
    //   kit.appendMessage(message, {
    //     from: 'other',
    //     sender: sender
    //   });
    //   kit.updateUnreadMessageCount();
    //   eventListeners.forEach(listener => {
    //     if (listener && typeof listener.onKitEvent == 'function') {
    //       listener.onKitEvent('chat-message', data);
    //     }
    //   });
    // });

    // socket.on('request-map-data', function (data, callback) {
    //   console.log('request-map-data received');
    //   // console.log(data)
    //   eventListeners.forEach(listener => {
    //     if (listener && typeof listener.onKitEvent == 'function') {
    //       // let kit-build app know that map data are requested
    //       let mapData = listener.onKitEvent('request-map-data');
    //       callback(mapData); // send back map data to server
    //     }
    //   });
    // });

    // socket.on('sync-map-data', function (data) {
    //   console.log('sync-map-data received');
    //   //console.log(data)
    //   eventListeners.forEach(listener => {
    //     if (listener && typeof listener.onKitEvent == 'function') {
    //       listener.onKitEvent('sync-map-data', data);
    //     }
    //   });
    // })

    // socket.on('sync-map-data-error', function (data) {
    //   console.log('sync-map-data-error received');
    //   // console.log(data)
    //   eventListeners.forEach(listener => {
    //     if (listener && typeof listener.onKitEvent == 'function') {
    //       listener.onKitEvent('sync-map-data-error', data); // { error: 'message' }
    //     }
    //   });
    // })

    // socket.on('create-node', function (data) {
    //   console.log('create-node received');
    //   eventListeners.forEach(listener => {
    //     if (listener && typeof listener.onKitEvent == 'function') {
    //       listener.onKitEvent('receive-create-node', data);
    //     }
    //   });
    // });

    // socket.on('move-node', function (data) {
    //   console.log('move-node received');
    //   eventListeners.forEach(listener => {
    //     if (listener && typeof listener.onKitEvent == 'function') {
    //       listener.onKitEvent('receive-move-node', data);
    //     }
    //   });
    // });

    // socket.on('connect-node', function (data) {
    //   console.log('connect-node received');
    //   eventListeners.forEach(listener => {
    //     if (listener && typeof listener.onKitEvent == 'function') {
    //       listener.onKitEvent('receive-connect-node', data);
    //     }
    //   });
    // });

    // socket.on('disconnect-node', function (data) {
    //   console.log('disconnect-node received');
    //   eventListeners.forEach(listener => {
    //     if (listener && typeof listener.onKitEvent == 'function') {
    //       listener.onKitEvent('receive-disconnect-node', data);
    //     }
    //   });
    // });

    // socket.on('duplicate-node', function (data) {
    //   console.log('duplicate-node received');
    //   eventListeners.forEach(listener => {
    //     if (listener && typeof listener.onKitEvent == 'function') {
    //       listener.onKitEvent('receive-duplicate-node', data);
    //     }
    //   });
    // });

    // socket.on('delete-node', function (data) {
    //   console.log('delete-node received');
    //   eventListeners.forEach(listener => {
    //     if (listener && typeof listener.onKitEvent == 'function') {
    //       listener.onKitEvent('receive-delete-node', data);
    //     }
    //   });
    // });

    // socket.on('update-node', function (data) {
    //   console.log('update-node received');
    //   eventListeners.forEach(listener => {
    //     if (listener && typeof listener.onKitEvent == 'function') {
    //       listener.onKitEvent('receive-update-node', data);
    //     }
    //   });
    // });

    // socket.on('center-link', function (data) {
    //   console.log('center-link received');
    //   eventListeners.forEach(listener => {
    //     if (listener && typeof listener.onKitEvent == 'function') {
    //       listener.onKitEvent('receive-center-link', data);
    //     }
    //   });
    // });

    // socket.on('switch-direction', function (data) {
    //   console.log('switch-direction received');
    //   eventListeners.forEach(listener => {
    //     if (listener && typeof listener.onKitEvent == 'function') {
    //       listener.onKitEvent('receive-switch-direction', data);
    //     }
    //   });
    // });

  }


  // Socket IO Command

  getUsers(callback) {
    let kit = this;
    let socket = this.socket;
    let eventListeners = this.eventListeners;
    socket.emit('get-users', null, function (users) {
      if(typeof callback == 'function') callback(users);
    });
    eventListeners.forEach(listener => {
      if (listener && typeof listener.onKitEvent == 'function') {
        listener.onKitEvent('on-get-users', users);
      }
    });
  }

  sendMessage(socketId, message, options, callback) {
    let kit = this;
    let socket = this.socket;
    let eventListeners = this.eventListeners;
    socket.emit('admin-message', {
      socketId: socketId,
      message: message,
      options: options
    }, function (status) {
      if(typeof callback == 'function') callback(status);
    });
  }

  updateUsers(users) {
    let userList = '';
    for(let key in users) {
      let user = users[key];
      userList += '<div class="row align-items-center list" data-key="' + key + '" data-name="' + user.name + '">';
      userList += '<div class="col-sm-6">' + user.name + ' <span class="badge badge-info">'+ key +'</span></div>';
      userList += '<div class="col-sm-6 text-right"><div class="btn-group btn-sm">';
      userList += '<button class="bt-message btn btn-sm btn-outline-primary"><i class="fas fa-comment"></i></button>';
      // userList += '<button class="bt-delete btn btn-sm btn-outline-danger"><i class="fas fa-trash"></i></button>';
      userList += '</div></div>';
      userList += '</div>';
    };
    $('#lobby-list').html(userList);
  }

}


$(function(){
  BRIDGE.app = new AdminMap();
})
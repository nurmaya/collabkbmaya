var BRIDGE = {};

class LobbyApp {

  constructor() {
    this.gui = new GUI();
    this.session = new Session(baseUrl);
    this.ajax = Ajax.ins(this.gui);
    this.controller = 'wibisono';
    this.page = 'lobby';
    this.nextPage = 'postlobby';

    this.material = {
      mid: null,
      name: null,
      content: null
    }
    this.handleEvent();
  }

  handleEvent() {
    let app = this;

    $('#bt-continue').on('click', function (e) {
      let mid = $(this).attr('data-mid');
      let gmid = $(this).attr('data-gmid');
      if (isNaN(parseInt(mid)) || parseInt(mid) == 0) {
        app.gui.notify('Select a material.', {
          type: 'danger'
        });
        return;
      }
      if (isNaN(parseInt(gmid)) || parseInt(gmid) == 0) {
        app.gui.notify('Unable to start Kit-Building without kit.<br>Please select a kit.', {
          type: 'danger'
        });
        return;
      }
      let confirmDialog = app.gui.confirm('Start kit-building?', {
        'positive-callback': function () {
          confirmDialog.modal('hide');
          app.session.setMulti({
            page: app.nextPage
          }, function () {
            setTimeout(function () {
              window.location.href = baseUrl + app.controller + '/' + app.nextPage;
            }, 500)
          });
        }
      })
    });
    $('#bt-refresh').on('click', function (e) {
      app.kit.getRooms();
    });
    $('#bt-logout').on('click', function (e) {
      let confirm = app.gui.dialog('Do you want to sign out?', {
        'positive-callback': function () {
          window.location.href = baseUrl + app.controller + "/signOut";
        },
        'negative-callback': function () {
          confirm.modal('hide');
        }
      })
    });

    $('#kit-list').on('click', '.row', function () {
      let gmid = $(this).data('gmid')
      let mid = $(this).data('mid')
      $(this).find('i').show();
      $('#bt-continue').attr('data-gmid', gmid);
      app.session.setMulti({
        gmid: gmid
      })
    });

    $('#material-list').on('click', '.row', function () {
      $('#material-list .row i').hide();
      $(this).find('i').show();
      $('#bt-continue').attr('data-mid', $(this).data('mid'));
      let mid = $('#bt-continue').attr('data-mid');
      app.loadMaterial(mid, function (material) {
        if (!material) {
          app.gui.dialog('Unable to load material.');
          return;
        }
        app.session.setMulti({
          mid: mid
        });
        app.setMaterial(material.mid, material.name, material.content);
        app.loadGoalmaps(material.mid, 'kit', function (goalmaps) {
          if (goalmaps.length) $('#kit-list .row').first().click();
        });
      });
    });

  }

  loadMaterial(id, callback) {
    let app = this;
    app.ajax.get(
      baseUrl + 'experimentApi/getMaterialCollabByMidOrFid/' + id
    ).then(function (material) { // console.log(material);
      if (!material) {
        if (typeof callback == 'function') callback(null);
        return;
      }
      if (typeof callback == 'function') callback(material);
    })
    $('#bt-show-material').prop('disabled', false);
  }

  setMaterial(mid, name, content) {
    this.material.mid = mid;
    this.material.name = name;
    this.material.content = content;
    $('#popup-material .material-title').html(this.material.name)
    $('#popup-material .material-content').html(this.material.content);
  }

  loadGoalmaps(mid, type = 'kit', callback) {
    let app = this;
    $('#kit-list').html('<em>Loading kits from server...</em>');
    app.ajax.get(
      baseUrl + 'kitbuildApi/getGoalmaps/' + mid + '/' + type
    ).then(function (goalmaps) { // console.log(goalmaps)
      let goalmapList = (!goalmaps || goalmaps.length == 0) ? '<p><em>No kit were found.</em></p>' : '';
      if (goalmaps && goalmaps.length) goalmaps.forEach(goalmap => {
        goalmapList += '<div class="row list pt-2 pb-2 pl-5 pr-5 text-center d-flex align-items-center justify-content-between mx-auto list-hover list-pointer" data-mid="' + goalmap.mid + '" data-gmid="' + goalmap.gmid + '"><span class="kit-name">' + goalmap.name + '</span><i class="fas fa-check text-success ml-3" style="display: none"></i></div>';
      });
      $('#kit-list').html(goalmapList);
      if (typeof callback == 'function') callback(goalmaps);
    });
  }

}

$(function () {

  let app = new LobbyApp(); // the app
  let logger = new Logger(uid ? uid : null, seq ? seq : null, sessId ? sessId : null);
  app.logger = logger;
  let eventListener = new EventListener(logger);
  BRIDGE.app = app;
  BRIDGE.logger = eventListener.getLogger();

});
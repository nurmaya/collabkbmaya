var BRIDGE = {};

class PreTest {

  constructor() {
    this.gui = new GUI();
    this.session = new Session(baseUrl);
    this.ajax = Ajax.ins(this.gui);
    this.controller = 'wibisono';
    this.nextPage = 'premapping';
    this.logger = new Logger(uid ? uid : null, seq ? seq : null, sessId ? sessId : null);
    this.user = null;
    console.log(this.logger);
    this.handleEvent(this);

    this.uid = uid ? uid : null;
    this.qsid = null;
    this.gid = null;
  }

  saveTextAreas() {
    let app = this;
    let answers = [];
    let inputs = $('textarea.input-free');
    for (let inp of inputs) {
      answers.push({
        qid: $(inp).data('qid'),
        answer: $(inp).val().trim()
      });
    }
    console.log(answers);
    if(answers.length == 0) return;
    app.session.setMulti({
      preansweressay: answers
    }, function () {
      app.ajax.post(baseUrl + 'experimentApi/saveAnswersEssay', {
        uid: app.uid,
        qsid: app.qsid,
        gid: app.gid,
        answers: answers
      }).then(function () {
        app.gui.notify('Essay answers saved.', {
          type: 'success',
          delay: 1000
        })
      });
    }, function (error) {
      app.gui.notify(error, {delay: 0});
    });
  }

  saveMultipleChoices() {
    let app = this;
    let checkAnswers = $('input[type="checkbox"]:checked');
    let cAnswers = [];
    for (let ca of checkAnswers) {
      // console.log($(ra).data('qid'), $(ra).data('qoid'))
      cAnswers.push({
        qid: $(ca).data('qid'),
        qoid: $(ca).data('qoid'),
      });
    }
    let radioAnswers = $('input[type="radio"]:checked');
    let rAnswers = [];
    for (let ra of radioAnswers) {
      // console.log($(ra).data('qid'), $(ra).data('qoid'))
      rAnswers.push({
        qid: $(ra).data('qid'),
        qoid: $(ra).data('qoid'),
      });
    }
    // console.log(answers);
    app.session.setMulti({
      preanswer: rAnswers.concat(cAnswers)
    }, function () {
      app.ajax.post(baseUrl + 'experimentApi/saveAnswers', {
        uid: app.uid,
        qsid: app.qsid,
        answers: rAnswers
      }).then(function () {
        app.ajax.post(baseUrl + 'experimentApi/saveAnswersMulti', {
          uid: app.uid,
          qsid: app.qsid,
          answers: cAnswers
        }).then(function () {
          app.gui.notify('Multiple choice answers saved.', {
            type: 'success',
            delay: 1000
          })
        });
      });
    }, function (error) {
      app.gui.notify(error, {delay:0});
    })
  }

  handleEvent(app) {

    $('input[type="checkbox"]').on('change', function () {
      app.saveMultipleChoices();
    })

    $('input[type="radio"]').on('change', function () {
      app.saveMultipleChoices();
    })

    $('textarea.input-free').on('focusout', function () {
      app.saveTextAreas();
    });

    $('#bt-continue').on('click', function () {

      let confirm = app.gui.confirm('Finish and submit all your answers?<br><span class="text-danger">Your session will also be ended.</span>', {
        'positive-callback': function () {
          confirm.modal('hide');
          app.saveTextAreas();
          app.saveMultipleChoices();
          app.gui.dialog('Your answers has been saved.<br>Session will be ended in 5s.');
          setTimeout(function () {
            app.logger.log('finish-pre-test', {
              uid: uid,
              qsid: app.qsid
            });
            app.session.setMulti({
              page: app.nextPage,
              seq: app.logger.seq + 1 // because of 'sign-in'
            }, function () {
              window.location.href = baseUrl + app.controller + '/' + app.nextPage;
            }, function (error) {
              gui.dialog(error, {
                width: '300px'
              });
            });
          }, 5000);
        }
      })
    });
  }

  loadUser() {
    let app = this;
    app.ajax.get(baseUrl + 'kitbuildApi/loadUser/' + uid).then(function (user) {
      app.user = user;
      if (app.user.gids) app.user.gids = app.user.gids.split(",");
      if (app.user.grups) app.user.grups = app.user.grups.split(",");
      // console.log(app.user);
    });
  }

  friendlyTime(d) {
    d = Number(d);
    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);

    var hDisplay = h > 0 ? (h < 10 ? "0" + h : h) + ":" : "";
    var mDisplay = m > 0 ? (m < 10 ? "0" + m : m) + ":" : "00:";
    var sDisplay = s > 0 ? (s < 10 ? "0" + s : s) : "00";
    return hDisplay + mDisplay + sDisplay; 
  }

  tick() {
    let app = this;
    this.remaining--;
    // console.log(this.remaining);
    $('#remaining').html(app.friendlyTime(this.remaining) + " left.");
    if (this.remaining > 0) setTimeout(this.tick.bind(this), 1000);
    else {
      this.saveTextAreas();
      this.saveMultipleChoices();
      app.gui.dialog('Time up. Your answers has been saved.<br>Session will be ended in 5s.');
      setTimeout(function () {
        app.logger.log('finish-pre-test', {
          uid: uid
        });
        app.session.setMulti({
          page: app.nextPage,
          seq: app.logger.seq + 1 // because of 'sign-in'
        }, function () {
          window.location.href = baseUrl + app.controller + '/' + app.nextPage;
        }, function (error) {
          gui.dialog(error, {
            width: '300px'
          });
        });
      }, 5000);
    }
  }

  countdown(remaining) {
    this.remaining = remaining;
    setTimeout(this.tick.bind(this), 1000);
  }

}

$(function () {
  BRIDGE.app = new PreTest();
  BRIDGE.app.session.getAll(function(sess){ console.log(sess);
    BRIDGE.app.qsid = (sess.qsid) ? sess.qsid : null;
    BRIDGE.app.countdown(sess.remaining ? sess.remaining : 0);
  })
});
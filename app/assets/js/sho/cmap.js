class CmapApp {

  constructor(canvas) {
    this.canvas = canvas;
    this.gui = new GUI();
    this.session = new Session(baseUrl);
    this.ajax = Ajax.ins(this.gui);

    this.controller = 'sho';

    this.user = {
      uid: null,
      username: null,
      name: null,
      rid: null,
      gids: []
    }

    this.material = {
      mid: null,
      name: null,
      content: null
    }

    this.goalmap = null;
    this.learnermap = null;

    this.eventListeners = [];

    this.handleEvent();
    this.canvas.attachEventListener(this);
    // this.canvas.getToolbar().enableNodeCreation(false);
    let app = this;
    app.session.get('user', function (u) {
      if (u != null) // console.log(u);
        app.signIn(u.uid, u.username, u.name, u.role_id, u.gids);
    });
    app.session.get('mid', function (mid) {
      if(mid) {
        app.loadMaterial(mid, function(material) {
          if(material) app.setMaterial(material.mid, material.name, material.content);
        })
        app.session.get('gmid', function (gmid) {
          if(gmid) {
            app.loadGoalmap(gmid, function(goalmap) {
              if(goalmap) {
                app.setGoalmap(goalmap);
                app.showMap(goalmap);
              }
            })
          }
        });
      }
    });
  }

  getCanvas() {
    return this.canvas;
  }

  getGUI() {
    return this.gui;
  }

  getSession() {
    return this.session;
  }

  attachEventListener(listener) {
    this.eventListeners.push(listener);
  }

  handleEvent() {
    let app = this;

    $('#bt-login').on('click', function (e) {
      $('#modal-login').modal('show');
      $('#modal-login .input-username').focus();
    });
    let doSignIn = function () {
      let username = $('#modal-login .input-username').val().trim();
      let password = $('#modal-login .input-password').val().trim();
      app.ajax.post(
        baseUrl + 'kitbuildApi/signIn', {
          username: username,
          password: password
        }
      ).then(function (user) {
        let uid = user.uid;
        let rid = user.role_id;
        let name = user.name;
        let gids = user.gids ? user.gids.split(",") : [];
        app.session.setMulti({
          user: user
        }, () => {
          app.signIn(uid, username, name, rid, gids);
        });
        $('#modal-login').modal('hide');
        $('#input-username').val('');
        $('#input-password').val('');
      })
    }
    $('#modal-login').on('click', '.bt-ok', doSignIn);
    $('#modal-login .input-password').on('keyup', function (e) {
      if (e.keyCode == 13) doSignIn();
    });
    $('#bt-logout').on('click', function (e) {
      let confirmDialog = app.gui.confirm('ログアウトしますか？', {
        'positive-callback': function () {
          confirmDialog.modal('hide');
          app.session.destroy(function () {
            app.signOut();
          })
          // window.location.href = baseUrl + app.controller + '/signOut';
        }
      });
    });
    // $('#bt-sync').on('click', (e) => {
    //   // console.log('sync')
    //   if (this.canvas.getCy().nodes().length) {
    //     let text = 'You currently have concept map on canvas.<br>If you sync the map, it will be <strong class="text-danger">replaced</strong>.<br>Continue?';
    //     let dialog = this.gui.confirm(text, {
    //       'positive-callback': function () {
    //         app.kit.syncMap();
    //         app.eventListeners.forEach(listener => {
    //           if (listener && typeof listener.onAppEvent == 'function') {
    //             listener.onAppEvent('request-sync');
    //           }
    //         })
    //         dialog.modal('hide');
    //       }
    //     })
    //   } else app.kit.syncMap();
    // });
    // $('#bt-open-kit').on('click', function () {
    //   app.ajax.post(
    //     baseUrl + 'kitbuildApi/getMaterialsWithGids', {
    //       gids: app.kit.user.gids
    //     }
    //   ).then(function (materials) {
    //     let materialList = (!materials || materials.length == 0) ?
    //       '<em class="text-secondary">No material available</em>' :
    //       '';
    //     materials.forEach((material) => {
    //       materialList += '<div class="row align-items-center list list-hover list-pointer justify-content-between pt-2 pb-2 pl-5 pr-5" data-mid="' + material.mid + '" data-name="' + material.name + '">'
    //       materialList += '<span class="material-name">' + material.name + '</span>'
    //       materialList += '<i class="fas fa-check text-primary" style="display:none"></i>'
    //       materialList += '</div>'
    //     });
    //     $('#modal-kit .material-list').html(materialList);
    //     $('#modal-kit').modal('show', {
    //       'width': '200px'
    //     });
    //   })
    // });
    // $('#modal-kit .material-list').on('click', '.row', function () {
    //   if ($(this).hasClass('selected')) return;


    //   let mid = $(this).data('mid');
    //   let name = $(this).data('name');
    //   $('#modal-kit .bt-open').data('mid', mid);
    //   $('#modal-kit .bt-open').data('name', name);
    // });
    // $('#modal-kit').on('click', '.bt-open', function () {
    //   let name = ($(this).data('name'))
    //   let mid = ($(this).data('mid'))
    //   let confirm = app.gui.confirm('Open material <strong>\'' + name + '\'</strong>?', {
    //     'positive-callback': function () {
    //       app.loadMaterial(mid, confirm)
    //     },
    //     'width': '300px'
    //   })
    // });
    // $('#bt-show-material').on('click', function () {
    //   $('#popup-material').fadeToggle({
    //     duration: 200,
    //     complete: function () {
    //       let shown = $(this).is(':visible');
    //       app.eventListeners.forEach(listener => {
    //         if (listener && typeof listener.onAppEvent == 'function') {
    //           if (shown) listener.onAppEvent('show-material');
    //           else listener.onAppEvent('hide-material');
    //         }
    //       })
    //     }
    //   });
    // });
    // $('#popup-material .bt-close').on('click', function () {
    //   $('#popup-material .check-open').prop('checked', false);
    //   $('#popup-material').fadeOut({
    //     duration: 200,
    //     complete: function () {
    //       app.eventListeners.forEach(listener => {
    //         if (listener && typeof listener.onAppEvent == 'function') {
    //           listener.onAppEvent('hide-material');
    //         }
    //       })
    //     }
    //   });
    // });
    // $('#popup-material .bt-top').on('click', function () {
    //   $('#popup-material .material-content')
    //     .animate({
    //       scrollTop: 0
    //     }, 500, 'swing');
    // });
    // $('#popup-material .bt-more').on('click', function () {
    //   let scrollTop = $('#popup-material .material-content').scrollTop();
    //   let height = $('#popup-material .material-content').height();
    //   $('#popup-material .material-content')
    //     .animate({
    //       scrollTop: scrollTop + height - 24
    //     }, 500, 'swing');
    // });
    $('#bt-open-map').on('click', function () {
      $('#modal-kit .bt-open').removeAttr('data-mid');
      $('#modal-kit .bt-open').removeAttr('data-gmid');
      $('#modal-kit .bt-new').removeAttr('data-mid')
      app.ajax.get(baseUrl + 'kitbuildApi/getMaterials').then(materials => {
        let materialList = (!materials || materials.length == 0) ?
          '<em>データベースでトピックが見つかりませんでした。</em>' :
          '';
        materials.forEach((material) => {
          materialList += '<div class="row align-items-center list list-hover list-pointer justify-content-between pl-2 pr-2 pt-1 pb-1" data-mid="' + material.mid + '" data-name="' + material.name + '">'
          materialList += '<span class="material-name">' + material.name + '</span>'
          materialList += '<i class="fas fa-check text-primary d-none"></i>'
          materialList += '</div>'
        });
        $('#modal-kit .material-list').html(materialList);
        app.gui.modal('#modal-kit', {
          'width': '600px'
        });
      });
    });
    $('#modal-kit .material-list').on('click', '.row', function () {
      let mid = $(this).data('mid');
      $('#modal-kit .bt-open').attr('data-mid', mid);
      $('#modal-kit .bt-new').attr('data-mid', mid);
      $('#modal-kit .material-list .row').find('i').addClass('d-none');
      $('#modal-kit .material-list .row').removeClass('selected');
      $(this).find('i').removeClass('d-none');
      $(this).addClass('selected')
      app.loadGoalmaps(mid, function (goalmaps) {
        // console.log(goalmaps)
        let goalmapList = (!goalmaps || goalmaps.length == 0) ? '<small>マップが見つかりません。</small>' : '';
        if (goalmaps.length) {
          goalmaps.forEach(goalmap => {
            goalmapList += '<div class="row align-items-center list list-hover list-pointer justify-content-between pl-2 pr-2 pt-1 pb-1" data-gmid="' + goalmap.gmid + '" data-name="' + goalmap.name + '">'
            goalmapList += '<span class="goalmap-name">' + goalmap.name + '</span>'
            goalmapList += '<i class="fas fa-check text-primary d-none"></i>'
            goalmapList += '</div>'
          });
        }
        $('#modal-kit .goalmap-list').html(goalmapList)
      });
    });
    $('#modal-kit .goalmap-list').on('click', '.row', function () {
      let gmid = $(this).data('gmid');
      $('#modal-kit .bt-open').attr('data-gmid', gmid);
      $('#modal-kit .bt-new').attr('data-gmid', gmid);
      $('#modal-kit .goalmap-list .row').find('i').addClass('d-none');
      $('#modal-kit .goalmap-list .row').removeClass('selected');
      $(this).find('i').removeClass('d-none');
      $(this).addClass('selected')
    });
    $('#modal-kit').on('click', '.bt-new', function () {
      let mid = $(this).attr('data-mid');
      if (!mid) {
        app.gui.notify('トピックを選択してください。', {
          type: 'warning'
        });
        return;
      }
      app.loadMaterial(mid, function (material) {
        console.log(material);
        if (!material) {
          app.gui.notify('トピックを読み込めません。', {
            type: 'danger'
          })
          return;
        }
        let doLoadMaterial = function () {
          $('#modal-kit').modal('hide');
          app.setMaterial(material.mid, material.name, material.content);
          app.session.set('mid', material.mid);
          app.enableSaveAs();
          app.setGoalmap(null);
        }
        if (app.canvas.getCy().nodes().length) {
          let confirm = app.gui.confirm('キャンバスにマップがあります。 トピックを開くと, キャンバスがクリアされます。 継続する？', {
            'positive-callback': function () {
              app.canvas.clearCanvas();
              app.canvas.reset();
              confirm.modal('hide');
              doLoadMaterial();
            }
          })
        } else doLoadMaterial();
      });
    });
    $('#modal-kit').on('click', '.bt-open', function () {
      let mid = $(this).attr('data-mid');
      let gmid = $(this).attr('data-gmid');
      if (!mid) {
        app.gui.notify('トピックを選択してください。', {
          type: 'warning'
        });
        return;
      }
      if (!gmid) {
        app.gui.notify('マップを選択してください。', {
          type: 'warning'
        });
        return;
      }
      app.loadMaterial(mid, function (material) {
        // console.log(material);
        if (!material) {
          app.gui.notify('トピックを読み込めません。', {
            type: 'danger'
          })
          return;
        }
        app.loadGoalmap(gmid, function (goalmap) {
          // console.log(goalmap);
          if (!goalmap) {
            app.gui.notify('マップを読み込めません。', {
              type: 'danger'
            })
            return;
          }
          let doLoadGoalmap = function () {
            $('#modal-kit').modal('hide');
            app.session.setMulti({
              'mid': material.mid,
              'gmid': goalmap.goalmap.gmid
            });
            app.setMaterial(material.mid, material.name, material.content);
            app.setGoalmap(goalmap);
            app.showMap(goalmap);
            app.enableSaveAs();
          }
          if (app.canvas.getCy().nodes().length) {
            let confirm = app.gui.confirm('キャンバスにマップがあります。 ゴールマップを開くと, キャンバスがクリアされます。 継続する？', {
              'positive-callback': function () {
                app.canvas.clearCanvas();
                app.canvas.reset();
                confirm.modal('hide');
                doLoadGoalmap();
              }
            })
          } else doLoadGoalmap();

        });
      });
    });
    $('#bt-save').on('click', function () {
      let mid = app.material.mid;
      let uid = app.user.uid;
      let gmid = app.goalmap ? app.goalmap.goalmap.gmid : null
      console.log(app, uid, mid, gmid);
      if(!gmid) {
        $('#bt-save-as').click();
        return;
      }
      app.saveGoalmapOverwrite(gmid, function (gmid) {
        app.gui.notify('Map saved.', {
          type: 'success'
        })
      });      
    });

    $('#bt-save-as').on('click', function () {
      if (app.getCanvas().getCy().nodes().length != 0) {
        let imageData = app.getCanvas().getCy().png({
          scale: 2,
          full: true
        });
        $('#popup-map').find('img.cmap').attr('src', imageData);
        setTimeout(function () {
          $('#popup-map').show().center().center();
        }, 100);
      } else {
        app.gui.notify('保存するものはありません。<br>マップが空です。', {
          type: 'warning'
        })
      }
      // let mid = app.material.mid;
      // let uid = app.user.uid;
      // let gmid = app.goalmap ? app.goalmap.goalmap.gmid : null
      // console.log(app, uid, mid, gmid);
      // if(!gmid) {
      //   $('#bt-save-as').click();
      //   return;
      // }
      // app.saveGoalmapOverwrite(gmid, function (gmid) {
      //   app.gui.notify('Map saved.', {
      //     type: 'success'
      //   })
      // });      
    });

    $('#popup-map').on('click', '.bt-cancel', function () {
      $('#popup-map').hide();
    });

    $('#popup-map').on('click', '.bt-download', function () {
      let mid = app.material.mid;
      let uid = app.user.uid;
      var a = document.createElement("a"); //Create <a>
      a.href = $('#popup-map').find('img.cmap').attr('src');
      a.download = "concept-map-m" + mid + "-u" + uid + ".png"; //File name Here
      a.click(); //Downloaded file
    })

    $('#popup-map').on('click', '.bt-continue', function () {
      let mid = app.material.mid;
      let uid = app.user.uid;
      if (mid != null && uid != null) {
        $('#modal-map-name').modal('show');
        $('#modal-map-name .map-name-input').val('draft-m' + mid + '-u' + uid + '-' + app.user.username).focus().select();
      } else app.gui.notify('無効なユーザーまたはトピック。', {type: 'danger'});
    });

    $('#modal-map-name').on('click', '.bt-ok', function () {
      let mid = app.material.mid;
      let uid = app.user.uid;
      if (mid != null && uid != null) {
        let name = $('#modal-map-name .map-name-input').val().trim()
        if (name == '')
          app.gui.dialog('コンセプトマップ名を入力してください。')
        else {
          app.saveGoalmapDraft(mid, uid, name, function (gmid) {
            $('#modal-map-name').modal('hide');
            $('#popup-map').hide();
            app.gui.notify('コンセプトマップを保存しました。', {
              type: 'success'
            })
            app.loadGoalmap(gmid, function(goalmap) {
              app.setGoalmap(goalmap)
            });
            app.eventListeners.forEach(listener => {
              if (listener && typeof listener.onAppEvent == 'function') {
                listener.onAppEvent('save-map', {
                  gmid: gmid
                });
              }
            })
            // app.kit.sendRoomMessage('change-page', null, {
            //   page: 'finish'
            // }, function () {
            //   app.session.set('page', 'finish', function () {
            //     window.location.href = baseUrl + 'home/finish';
            //     return;
            //   }, function (error) {
            //     app.gui.dialog('Error: ' + error);
            //   })
            // });
          });
        }
      } else {
        if (!mid) {
          app.gui.dialog('Please open a material to save the map into.<br>A map should refer to a material.');
          return;
        }
        if (!rid) {
          app.gui.dialog('Please join a room to save the map into.<br>In a collaboration environment, a saved maps should relate with a collaboration group. Therefore, you need to join a room before you create and save a concept map.');
          return;
        }
      }
    });




    $('#modal-open-map .map-list').on('click', '.row', function () {
      let gmid = $(this).data('gmid');
      let name = $(this).data('name');
      // console.log(gmid, name);
      if (app.canvas.getCy().nodes().length) {
        let dialog = app.gui.confirm('You have map on canvas. Opening the map will replace existing map on canvas. Open map "' + name + '"?', {
          'positive-callback': function () {
            app.loadSavedGoalmap(gmid, function () {
              dialog.modal('hide');
              $('#modal-open-map').modal('hide');
            });
          }
        });
        return;
      } else {
        app.loadSavedGoalmap(gmid, function () {
          $('#modal-open-map').modal('hide');
        });
      }
    })
    
    $('#bt-load-draft').on('click', function () {
      let mid = app.material.mid;
      let rid = app.kit.room.rid;
      if (mid != null && rid != null) {
        let currentEles = app.canvas.getCy().nodes();
        if (currentEles.length) {
          let confirm = app.gui.confirm('You currently have existing map data on canvas. Loading saved draft map data will <strong class="text-danger">REPLACE</strong> current map on canvas and across <strong class="text-danger">ALL users</strong> in current room.<br>Continue?', {
            'positive-callback': function () {
              app.loadSavedGoalmapDraft(mid, rid, function () {
                confirm.modal('hide');
              });
            }
          })
        } else app.loadSavedGoalmapDraft(mid, rid);
      } else {
        if (!mid) app.gui.dialog('Please select a material. A concept map saved draft is related with a material.')
        if (!rid) app.gui.dialog('Please select a room. A collaborative concept map saved draft is related with a collaboration room.')
      }
    });
    $('#bt-finalize').on('click', function () {
      if (app.getCanvas().getCy().nodes().length != 0) {
        let imageData = app.getCanvas().getCy().png({
          scale: 2,
          full: true
        });
        $('#popup-map').find('img.cmap').attr('src', imageData);
        setTimeout(function () {
          $('#popup-map').show().center().center();
        }, 100);
      } else {
        confirmFinalize();
      }
    });

    
    $('.material-content').on('mouseup', function (e) {
      let s = window.getSelection();
      if(s.anchorNode == null) return;
      let oRange = s.getRangeAt(0); //get the text range
      let text = oRange.toString().trim();
      if (text.length) {
        let oRect = oRange.getBoundingClientRect();
        // console.log(s, oRange, oRect);
        $('#popup-selection .selected-text').html(text);
        $('#popup-selection').show()
          .css('top', oRect.top)
          .css('left', oRect.left + oRect.width + 10);
        e.stopImmediatePropagation();
      } else $('#popup-selection').hide();
    });
    $('#popup-selection').on('click', '.menu-item', function () {
      let text = $(this).find('strong').text();
      let type = $(this).data('type');
      $('#input-node-label').val(text);
      app.eventListeners.forEach(listener => {
        if (listener && typeof listener.onAppEvent == 'function') {
          listener.onAppEvent('new-' + type + '-from-selection', {
            type: type,
            text: text
          });
        }
      })
      switch (type) {
        case 'concept':
          $('#bt-new-concept').click();
          $('#input-node-label').val(text);
          break;
        case 'link':
          $('#bt-new-link').click();
          $('#input-node-label').val(text);
          break;
      }
      $('#popup-selection').hide()
    });
    $('.material-content').on('scroll', function () {
      let s = window.getSelection();
      if(s.anchorNode == null) return;
      let oRange = s.getRangeAt(0); //get the text range
      let oRect = oRange.getBoundingClientRect();
      if (oRect.top - $(this).offset().top > -24 &&
        oRect.top < $(this).offset().top + $(this).height()) {
        $('#popup-selection').show();
      } else $('#popup-selection').hide();
      if ($('#popup-selection').is(':visible'))
        $('#popup-selection').css('top', oRect.top).css('left', oRect.left + oRect.width + 10);
    });
    $('#popup-material').on('mouseup', function () {
      $('#popup-selection').hide();
    });
    $(document).on('mouseup', function (e) {
      var container = $("#popup-material");
      var button = $('#bt-show-material')
      if (!container.is(e.target) && container.has(e.target).length === 0 &&
        !button.is(e.target) && button.has(e.target).length === 0) {
        $('#popup-selection').hide();
        if ($('#popup-material .check-open').prop('checked')) return;
        if (container.is(':visible')) {
          container.fadeOut();
          app.eventListeners.forEach(listener => {
            if (listener && typeof listener.onAppEvent == 'function') {
              listener.onAppEvent('hide-material');
            }
          });
        }
      }
    });
  }

  onCanvasToolClicked(tool, node) { console.log(tool, node);

    // for listener purposes
    let action = null;
    let data = null;
    let listenerHandled = false;
    let app = this;

    switch (tool.type) {
      case 'discuss-channel':
        let nodeId = node.id();
        let nodeType = node.data('type');
        let nodeLabel = node.data('name');
        app.kit.openChannel(nodeId, nodeType, nodeLabel, function (channel) {
          action = 'open-discuss-channel';
          app.eventListeners.forEach(listener => {
            if (listener && typeof listener.onCollabEvent == 'function') {
              listener.onCollabEvent(action, {
                cid: channel.cid,
                rid: channel.rid,
                node_id: channel.node_id,
                node_type: channel.node_type,
                node_label: channel.node_label
              });
            }
          })
        });
        listenerHandled = true;
        break;
      case 'delete-node':
      case 'delete-selected-nodes':
      case 'center-link':
      case 'switch-direction':
        listenerHandled = true;
        break;
    }
    if (listenerHandled) return;
    this.eventListeners.forEach(listener => {
      if (listener && typeof listener.onCollabEvent == 'function') {
        listener.onCollabEvent(action, data);
      }
    })
  }


  // Commanding Kit-Build App Interface

  signIn(uid, username, name, role, gids) {
    let app = this;
    app.user.uid = uid;
    app.user.username = username;
    app.user.name = name;
    app.user.role = role;
    app.user.gids = gids;
    $('#bt-login').addClass('d-none')
    $('#bt-logout').removeClass('d-none')
    $('#bt-logout-username').html(": " + username);
    $('#bt-open-kit').prop('disabled', false);
    this.enableOpenMap();
    this.enableSave();
    switch (role) {
      case 'CON':
        this.enableConceptMapperToolbar();
        break;
      case 'KIT':
        this.enableKitBuilderToolbar();
        break;
    }
  };

  signOut() {
    this.user.uid = null;
    this.user.username = null;
    this.user.name = null;
    this.user.role = null;
    $('#bt-login').removeClass('d-none')
    $('#bt-logout').addClass('d-none')
    $('#bt-open-kit').prop('disabled', true);
    $('#bt-show-material').prop('disabled', true);
    this.enableOpenMap(false);
    this.enableSave(false);
    this.enableConceptMapperToolbar(false);
    this.enableKitBuilderToolbar(false);
  }

  enableOpenMap(enabled = true) {
    $('#bt-open-map').prop('disabled', !enabled);
  }

  enableConceptMapperToolbar(enabled = true) {
    if (enabled) $('.concept-mapper-toolbar').removeClass('d-none');
    else $('.concept-mapper-toolbar').addClass('d-none');
  }

  enableKitBuilderToolbar(enabled = true) {
    if (enabled) $('.kit-builder-toolbar').removeClass('d-none');
    else $('.kit-builder-toolbar').addClass('d-none');
  }

  enableMaterial(enabled = true) {
    $('#bt-show-material').prop('disabled', !enabled);
  }

  enableSave(enable = true) {
    $('#bt-save').prop('disabled', !enable);
    $('#bt-load-draft').prop('disabled', !enable);
  }

  enableSaveAs(enable = true) {
    $('#bt-save-as').prop('disabled', !enable);
  }

  enableFinalize(enable = true) {
    $('#bt-finalize').prop('disabled', !enable);
  }

  loadMaterial(mid, callback) {
    let app = this;
    app.ajax.get(
      baseUrl + 'kitbuildApi/getMaterialCollabByMid/' + mid
    ).then(function (material) {
      if (typeof callback == 'function') callback(material);
    })
  }

  setMaterial(mid, name, content) { // console.log(this);
    this.material.mid = mid;
    this.material.name = name;
    this.material.content = content;
    $('#toolbar-material .material-name').html(name);
    // $('#popup-material .material-title').html(this.material.name)
    // $('#popup-material .material-content').html(this.material.content);
  }

  unsetMaterial() {
    this.material.mid = null;
    this.material.name = null;
    this.material.content = null;
  }

  loadGoalmaps(mid, callback) {
    let app = this;
    app.ajax.get(
      baseUrl + 'kitbuildApi/getGoalmaps/' + mid
    ).then(function (goalmaps) {
      if (typeof callback == 'function') callback(goalmaps);
    })
  }

  loadGoalmap(gmid, callback) {
    let app = this;
    app.ajax.get(
      baseUrl + 'kitbuildApi/openKit/' + gmid
    ).then(function (goalmap) {
      if (typeof callback == 'function') callback(goalmap);
    })
  }

  setGoalmap(goalmap) {
    let app = this;
    app.goalmap = goalmap;
    if(goalmap) $('#toolbar-material .map-name').html(goalmap.goalmap.name);
    else $('#toolbar-material .map-name').html('マップが読み込まれていません。');
  }

  showMap(cmap) {
    let app = this;
    let goalmap = cmap.goalmap;
    let concepts = cmap.concepts;
    let links = cmap.links;
    let eles = [];
    concepts.forEach((c) => {
      eles.push({
        group: 'nodes',
        data: {
          id: 'c-' + c.cid,
          name: c.label,
          type: 'concept'
        },
        position: {
          x: parseInt(c.locx),
          y: parseInt(c.locy)
        }
      })
    })
    links.forEach((l) => {
      eles.push({
        group: 'nodes',
        data: {
          id: 'l-' + l.lid,
          name: l.label,
          type: 'link'
        },
        position: {
          x: parseInt(l.locx),
          y: parseInt(l.locy)
        }
      })
      if (l.source != null) {
        eles.push({
          group: 'edges',
          data: {
            source: 'l-' + l.lid,
            target: 'c-' + l.source,
            type: 'left'
          }
        })
      }
      if (l.target != null) {
        eles.push({
          group: 'edges',
          data: {
            source: 'l-' + l.lid,
            target: 'c-' + l.target,
            type: 'right'
          }
        })
      }
    })
    app.canvas.clearCanvas();
    app.canvas.getCy().add(eles);
    app.canvas.getCanvasTool().clear();
    app.canvas.getCanvasTool().resetState();
    $('#bt-center').click();
    app.eventListeners.forEach(listener => {
      if (listener && typeof listener.onAppEvent == 'function') {
        listener.onAppEvent('load-concept-map', {
          gmid: parseInt(goalmap.gmid)
        });
      }
    })
  }

  saveGoalmap(mid, uid, type, name, callback) { // console.log(name);
    let app = this;
    let goalmap = {
      name: name ? name : type + '-m' + mid + '-u' + uid,
      type: type,
      mid: mid,
      creator_id: uid,
      updater_id: uid,
      concepts: [],
      links: [],
    }
    let cs = app.canvas.getNodes('[type="concept"]');
    let ls = app.canvas.getNodes('[type="link"]');
    let es = app.canvas.getEdges();
    // console.log(cs, ls, es);
    cs.forEach(c => {
      goalmap.concepts.push({
        cid: c.data.id.substr(2),
        gmid: null,
        label: c.data.name,
        locx: c.position.x,
        locy: c.position.y
      });
    });
    ls.forEach(l => {
      let tl = {
        lid: l.data.id.substr(2),
        gmid: null,
        label: l.data.name,
        locx: l.position.x,
        locy: l.position.y,
        source: null,
        target: null
      };
      es.forEach((e) => {
        if (e.data.source == l.data.id) {
          if (e.data.type == 'right') tl.target = e.data.target.substr(2);
          if (e.data.type == 'left') tl.source = e.data.target.substr(2);
          return;
        }
      });
      goalmap.links.push(tl);
    })
    app.ajax.post(
      baseUrl + 'kitbuildApi/saveGoalmap',
      goalmap
    ).then(function (gmid) {
      app.eventListeners.forEach(listener => {
        if (listener && typeof listener.onAppEvent == 'function') {
          listener.onAppEvent('map-save-' + type, {
            gmid: gmid
          });
        }
      })
      if (typeof callback == 'function') callback(gmid)
    })
  }

  saveGoalmapOverwrite(gmid, callback) { console.log(gmid);
    let app = this;
    let goalmap = {
      gmid: gmid,
      updater_id: uid,
      concepts: [],
      links: []
    }
    let cs = app.canvas.getNodes('[type="concept"]');
    let ls = app.canvas.getNodes('[type="link"]');
    let es = app.canvas.getEdges();
    // console.log(cs, ls, es);
    cs.forEach(c => {
      goalmap.concepts.push({
        cid: c.data.id.substr(2),
        gmid: null,
        label: c.data.name,
        locx: c.position.x,
        locy: c.position.y
      });
    });
    ls.forEach(l => {
      let tl = {
        lid: l.data.id.substr(2),
        gmid: null,
        label: l.data.name,
        locx: l.position.x,
        locy: l.position.y,
        source: null,
        target: null
      };
      es.forEach((e) => {
        if (e.data.source == l.data.id) {
          if (e.data.type == 'right') tl.target = e.data.target.substr(2);
          if (e.data.type == 'left') tl.source = e.data.target.substr(2);
          return;
        }
      });
      goalmap.links.push(tl);
    })
    app.ajax.post(
      baseUrl + 'kitbuildApi/saveGoalmap',
      goalmap
    ).then(function (gmid) {
      app.eventListeners.forEach(listener => {
        if (listener && typeof listener.onAppEvent == 'function') {
          listener.onAppEvent('map-save-overwrite', {
            gmid: gmid
          });
        }
      })
      if (typeof callback == 'function') callback(gmid)
    })
  }

  saveGoalmapDraft(mid, uid, name, callback) {
    this.saveGoalmap(mid, uid, 'draft', name, callback)
  }

  saveGoalmapFinalize(mid, uid, name, callback) {
    this.saveGoalmap(mid, uid, 'fix', name, callback)
  }

  saveGoalmapAuto(mid, uid, callback) {
    this.saveGoalmap(mid,  uid, 'auto', null, callback)
  }

  checkAvailableGoalmapDraft(mid, rid) {
    let app = this;
    app.ajax.get(
      baseUrl + 'kitbuildApi/getLastDraftGoalmap/' + mid + '/' + rid
    ).then(function (gmid) {
      if (gmid) $('#bt-load-draft').prop('disabled', false);
    })
  }

  loadSavedGoalmapDraft(mid, rid, callback) { // console.log(mid, uid, rid);
    let app = this;
    app.ajax.get(
      baseUrl + 'kitbuildApi/loadLastDraftGoalmapCollab/' + mid + "/" + rid
    ).then(function (draft) {
      if (draft == null) {
        app.gui.notify('Nothing to load. No previously saved draft maps found.')
        return;
      }
      let goalmap = draft.goalmap;
      let concepts = draft.concepts;
      let links = draft.links;
      let eles = [];
      concepts.forEach((c) => {
        eles.push({
          group: 'nodes',
          data: {
            id: 'c-' + c.cid,
            name: c.label,
            type: 'concept'
          },
          position: {
            x: parseInt(c.locx),
            y: parseInt(c.locy)
          }
        })
      })
      links.forEach((l) => {
        eles.push({
          group: 'nodes',
          data: {
            id: 'l-' + l.lid,
            name: l.label,
            type: 'link'
          },
          position: {
            x: parseInt(l.locx),
            y: parseInt(l.locy)
          }
        })
        if (l.source != null) {
          eles.push({
            group: 'edges',
            data: {
              source: 'l-' + l.lid,
              target: 'c-' + l.source,
              type: 'left'
            }
          })
        }
        if (l.target != null) {
          eles.push({
            group: 'edges',
            data: {
              source: 'l-' + l.lid,
              target: 'c-' + l.target,
              type: 'right'
            }
          })
        }
      })
      app.canvas.clearCanvas();
      app.canvas.getCy().add(eles);
      app.gui.notify('Concept map loaded from last saved drafts.')
      app.kit.sendMap(eles);
      $('#bt-center').click();
      app.eventListeners.forEach(listener => {
        if (listener && typeof listener.onAppEvent == 'function') {
          listener.onAppEvent('load-last-draft', {
            gmid: parseInt(goalmap.gmid)
          });
        }
      })
      if (typeof callback == 'function') callback();
    })
  }

  loadSavedGoalmap(gmid, callback) { // console.log(mid, uid, rid);
    let app = this;
    app.ajax.get(
      baseUrl + 'kitbuildApi/openKit/' + gmid
    ).then(function (cmap) {
      if (cmap == null) {
        app.gui.notify('Error loading concept map.')
        return;
      }
      let goalmap = cmap.goalmap;
      let concepts = cmap.concepts;
      let links = cmap.links;
      let eles = [];
      concepts.forEach((c) => {
        eles.push({
          group: 'nodes',
          data: {
            id: 'c-' + c.cid,
            name: c.label,
            type: 'concept'
          },
          position: {
            x: parseInt(c.locx),
            y: parseInt(c.locy)
          }
        })
      })
      links.forEach((l) => {
        eles.push({
          group: 'nodes',
          data: {
            id: 'l-' + l.lid,
            name: l.label,
            type: 'link'
          },
          position: {
            x: parseInt(l.locx),
            y: parseInt(l.locy)
          }
        })
        if (l.source != null) {
          eles.push({
            group: 'edges',
            data: {
              source: 'l-' + l.lid,
              target: 'c-' + l.source,
              type: 'left'
            }
          })
        }
        if (l.target != null) {
          eles.push({
            group: 'edges',
            data: {
              source: 'l-' + l.lid,
              target: 'c-' + l.target,
              type: 'right'
            }
          })
        }
      })
      app.canvas.clearCanvas();
      app.canvas.getCy().add(eles);
      app.gui.notify('Concept map loaded.')
      app.kit.sendMap(eles);
      $('#bt-center').click();
      app.eventListeners.forEach(listener => {
        if (listener && typeof listener.onAppEvent == 'function') {
          listener.onAppEvent('load-concept-map', {
            gmid: parseInt(goalmap.gmid)
          });
        }
      })
      if (typeof callback == 'function') callback();
    })
  }

}

jQuery.fn.center = function (parent) {
  parent = parent ? $(parent) : window
  this.css({
    "position": "absolute",
    "top": ((($(parent).height() - this.outerHeight()) / 2) + $(parent).scrollTop() + "px"),
    "left": ((($(parent).width() - this.outerWidth()) / 2) + $(parent).scrollLeft() + "px")
  });
  return this;
}

var BRIDGE = {};

$(function () {

  var canvas = new Canvas('cy', {
    isDirected: false,
    // enableToolbar: false,
    // enableNodeCreation: false,
    // enableConceptCreation: false,
    // enableLinkCreation: false,
    // enableUndoRedo: false,
    // enableZoom: false,
    // enableAutoLayout: false,
    // enableSaveImage: false,
    // enableClearCanvas: false
  }).init();

  let app = new CmapApp(canvas); // the app
  let logger = new Logger(uid ? uid : null, seq ? seq : null, sessId ? sessId : null);
  let eventListener = new EventListener(logger, canvas);
  // let collabChannel = new CollabChannel().init(canvas);
  // let collabKit = new CollabKit();

  // collabKit.attachGUI(app.getGUI());
  // collabKit.attachChannelTool(collabChannel);

  canvas.attachEventListener(eventListener);
  // app.attachKit(collabKit);
  app.attachEventListener(eventListener);
  // app.getCanvas().attachTool(collabChannel);

  BRIDGE.app = app;
  // BRIDGE.collabKit = collabKit;
  // BRIDGE.logger = eventListener.getLogger();

});
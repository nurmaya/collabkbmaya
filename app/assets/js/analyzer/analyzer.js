var BRIDGE = {};

$(function () {

  var canvas = new Canvas('cy', {
    // isDirected: false,
    // enableToolbar: false,
    enableNodeCreation: false,
    // enableConceptCreation: false,
    // enableLinkCreation: false,
    enableUndoRedo: false,
    // enableZoom: false,
    enableAutoLayout: false,
    // enableSaveImage: false,
    enableClearCanvas: false,
    enableNodeTools: false,
    enableNodeModificationTools: false,
    enableConnectionTools: false,
  }).init();

  let app = new AnalyzerApp(canvas);

  // create bridges to server-side javascript session handler 
  // BRIDGE.app = kitbuild.getApp();
  // BRIDGE.ui = kitbuild.getUI();
  // BRIDGE.chat = chat;
  // BRIDGE.kit = kit;

});
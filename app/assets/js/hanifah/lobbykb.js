var BRIDGE = {};

class LobbyApp {

  constructor(canvas) {
    // this.canvas = canvas;
    this.gui = new GUI();
    this.session = new Session(baseUrl);
    this.ajax = Ajax.ins(this.gui);
    this.controller = 'hanifah';
    this.page = 'lobby';
    this.nextPage = 'postlobby';

    this.material = {
      mid: null,
      name: null,
      content: null
    }

    this.goalmap = {
      gmid: null,
      name: null
    }

    this.eventListeners = [];

    this.kit = null; // holder for kit socket communication
    this.room = null;
    this.users = [];
    this.roomUsers = [];
    this.handleEvent();
    // this.canvas.attachEventListener(this);
  }

  getCanvas() {
    return this.canvas;
  }

  getGUI() {
    return this.gui;
  }

  getSession() {
    return this.session;
  }

  handleEvent() {
    let app = this;

    $('#bt-continue').on('click', function (e) {
      let mid = $(this).attr('data-mid');
      let gmid = $(this).attr('data-gmid');
      if (isNaN(parseInt(mid)) || parseInt(mid) == 0) {
        app.gui.notify('Select a material.', {
          type: 'danger'
        });
        return;
      }
      if (isNaN(parseInt(gmid)) || parseInt(gmid) == 0) {
        app.gui.notify('Unable to start Kit-Building without kit.<br>Please select a kit.', {
          type: 'danger'
        });
        return;
      }
      if (app.roomUsers.length < 2) app.gui.notify('Silakan tunggu anggota kelompok Anda yang lain untuk bergabung dalam room yang sama.', {
        type: 'warning'
      });
      else {
        let confirmDialog = app.gui.confirm('Start kit-building?', {
          'positive-callback': function () {
            confirmDialog.modal('hide');
            app.kit.sendCommand('set-mid-gmid', app.room, {
              mid: mid,
              gmid: gmid
            });
            app.kit.sendRoomMessage('change-page', app.room, {
              page: app.nextPage,
              from: app.page
            }, function () {
              app.session.setMulti({
                page: app.nextPage
              });
              setTimeout(function () {
                window.location.href = baseUrl + app.controller + '/' + app.nextPage;
              }, 500)
            });
          }
        })
      }
    });
    $('#bt-refresh').on('click', function (e) {
      app.kit.getRooms();
    });
    $('#bt-logout').on('click', function (e) {
      let confirm = app.gui.dialog('Do you want to sign out?', {
        'positive-callback': function () {
          window.location.href = baseUrl + app.controller + "/signOut";
        },
        'negative-callback': function () {
          confirm.modal('hide');
        }
      })
    });

    $('#kit-list').on('click', '.row', function () {
      let gmid = $(this).data('gmid')
      let mid = $(this).data('mid')
      $(this).find('i').show();
      $('#bt-continue').attr('data-gmid', gmid);
      app.session.setMulti({
        gmid: gmid
      })
      app.kit.sendCommand('set-mid-gmid', app.room, {
        mid: mid,
        gmid: gmid
      });
    });

    $('#material-list').on('click', '.row', function () {
      $('#material-list .row i').hide();
      $(this).find('i').show();
      $('#bt-continue').attr('data-mid', $(this).data('mid'));
      let mid = $('#bt-continue').attr('data-mid');
      app.loadMaterial(mid, function (material) {
        if (!material) {
          app.gui.dialog('Unable to load material.');
          return;
        }
        app.session.setMulti({
          mid: mid
        });
        app.setMaterial(material.mid, material.name, material.content);
        app.loadGoalmaps(material.mid, 'kit', function (goalmaps) {
          if (goalmaps.length) $('#kit-list .row').first().click();
        });
      });
    });

  }

  attachEventListener(listener) {
    this.eventListeners.push(listener);
  }

  attachKit(kit) { // console.log(kit);
    this.kit = kit;
    this.kit.attachEventListener(this);
    this.kit.enableRoom();
  }

  onKitEvent(event, data) { // console.log(event, data);
    let app = this;
    let gui = this.gui;
    let kit = this.kit;
    let canvas = this.canvas;
    switch (event) {
      case 'join-room':
        this.session.set('room', data.room);
        // this.canvas.clearCanvas();
        kit.rooms.forEach(room => {
          // console.log(room, kit)
          if (room.name == kit.room.name && room.users.length) {
            // kit.syncMap();
            // kit.syncMessages();
            return;
          }
        })
        $('#bt-sync').prop('disabled', false)
        break;
      case 'leave-room':
        this.session.unset('room');
        break;
      case 'user-update':
        this.users = data;
        break;
      case 'room-update':
        kit.rooms = data;
        if (app.room != null) {
          kit.rooms.forEach(room => {
            if (room.rid == app.room.rid) {
              let participantList = '';
              app.roomUsers = [];
              for (let user of room.users) {
                let u = app.users[user];
                // console.log(app.users, u)
                if (u) {
                  let exists = false;
                  for (let ru of app.roomUsers) {
                    console.log(ru, u);
                    if (ru.uid == u.uid) {
                      exists = true;
                      break;
                    }
                  }
                  if (exists) continue;
                  participantList += '<div class="row list pt-2 pb-2 ml-5 mr-5 text-center">' + u.name + '</div>';
                  app.roomUsers.push(u);
                }
              }
              // room.users.forEach(user => {

              // });
              $('#participant-list').html(participantList);
              app.gui.notify('Participant list updated.');
              // if(this.roomUsers.length > 1) 
              //   $('#bt-continue').attr('disabled', false);
              // else $('#bt-continue').attr('disabled', true);
            }
          })
        }
        break;
      case 'user-sign-in':
        // this.session.set('user', data);
        break;
      case 'user-sign-out':
        // this.session.unset('user');
        break;
      case 'command-sign-out':
        this.signOut();
        this.kit.leaveRoom(this.kit.room);
        this.kit.enableRoom(false)
        this.kit.enableMessage(false)
        this.gui.dialog('You have been signed out');
        break;
      case 'command-leave-room':
        // this.signOut();
        // this.kit.leaveRoom(this.kit.room);
        // this.kit.enableRoom(false)
        // this.kit.enableMessage(false)
        // this.gui.dialog('You have been signed out');
        break;
      case 'change-page':
        if(!data.from || data.from != app.page) break;
        this.session.setMulti({ // mid sudah diset dari peer/click
          page: data.page
        }, function () {
          app.gui.dialog('Your friend had started the process, you will be automatically redirected to the next page in 3s.');
          setTimeout(() => {
            window.location.href = baseUrl + app.controller + '/' + data.page;
          }, 3000);     
          return;
        }, function (error) {
          app.gui.dialog('Error: ' + error);
        })
        break;
      case 'admin-message':
        app.gui.notify("<strong>Admin:</strong> " + data.message, data.options);
        break;
      case 'set-mid-gmid':
        this.session.setMulti({
          mid: data.mid,
          gmid: data.gmid
         }, function () {
          $('#material-list .row i').hide();
          $('#material-list .row[data-mid="' + data.mid + '"] i').show();
          let name = $('#material-list .row[data-mid="' + data.mid + '"] .material-name').html();
          app.gui.notify('Material ' + name + ' selected');
          app.loadGoalmaps(data.mid, 'kit', function (goalmaps) {
            let gmid = data.gmid;
            if (gmid) {
              $('#kit-list .row i').hide();
              $('#kit-list .row[data-gmid="' + data.gmid + '"] i').show();
              let name = $('#kit-list .row[data-gmid="' + data.gmid + '"] .kit-name').html();
              if (name) app.gui.notify('Kit: ' + name + ' selected');
            }
          });
        });
        break;
      // case 'set-gmid':
      //   this.session.set('gmid', data.gmid, function () {
      //     console.log(data);
      //     $('#bt-continue').attr('data-gmid', data.gmid);
      //     $('#kit-list .row i').hide();
      //     $('#kit-list .row[data-gmid="' + data.gmid + '"] i').show();
      //     let name = $('#kit-list .row[data-gmid="' + data.gmid + '"] .kit-name').html();
      //     console.log($('#kit-list .row[data-gmid="' + data.gmid + '"]'))
      //     if (name) app.gui.notify('Kit ' + name + ' selected');
      //   });
      //   break;

        // Collab Event From Kit

      case 'send-message':
        this.eventListeners.forEach(listener => {
          if (listener && typeof listener.onCollabEvent == 'function') {
            listener.onCollabEvent('send-room-message', data.message);
          }
        })
        break;
    }
  }

  // Commanding Kit-Build App Interface

  signIn(uid, username, name, rid, gids) {
    $('#bt-login').hide();
    $('#bt-logout').show();
    $('#bt-logout-username').html(": " + username);
    $('#bt-open-kit').prop('disabled', false);
    this.kit.enableMessage();
  };

  signOut() {
    $('#bt-login').show();
    $('#bt-logout').hide();
    $('#bt-open-kit').prop('disabled', true);
    $('#bt-show-material').prop('disabled', true);
  }

  joinRoom(uid) {
    let app = this;
    app.ajax.get(baseUrl + 'experimentApi/getUserRoom/' + uid).then(function (room) { // console.log(room);
      app.room = room;
      app.kit.joinRoom({
        rid: room.rid,
        name: room.name
      });
      app.session.setMulti({
        rid: room.rid
      });
    })
  }

  loadMaterial(id, callback) {
    let app = this;
    app.ajax.get(
      baseUrl + 'experimentApi/getMaterialCollabByMidOrFid/' + id
    ).then(function (material) { // console.log(material);
      if (!material) {
        if (typeof callback == 'function') callback(null);
        return;
      }
      if (typeof callback == 'function') callback(material);
    })
    $('#bt-show-material').prop('disabled', false);
  }

  setMaterial(mid, name, content) {
    this.material.mid = mid;
    this.material.name = name;
    this.material.content = content;
    $('#popup-material .material-title').html(this.material.name)
    $('#popup-material .material-content').html(this.material.content);
  }

  loadGoalmaps(mid, type = 'kit', callback) {
    let app = this;
    $('#kit-list').html('<em>Loading kits from server...</em>');
    app.ajax.get(
      baseUrl + 'kitbuildApi/getGoalmaps/' + mid + '/' + type
    ).then(function (goalmaps) {
      console.log(goalmaps)
      let goalmapList = (!goalmaps || goalmaps.length == 0) ? '<p><em>No kit were found.</em></p>' : '';
      if (goalmaps && goalmaps.length) goalmaps.forEach(goalmap => {
        console.log(goalmap);
        goalmapList += '<div class="row list pt-2 pb-2 pl-5 pr-5 text-center d-flex align-items-center justify-content-between mx-auto list-hover list-pointer" data-mid="' + goalmap.mid + '" data-gmid="' + goalmap.gmid + '"><span class="kit-name">' + goalmap.name + '</span><i class="fas fa-check text-success ml-3" style="display: none"></i></div>';
      });
      $('#kit-list').html(goalmapList);
      if (typeof callback == 'function') callback(goalmaps);
    });
  }

}

$(function () {

  let app = new LobbyApp(); // the app
  let logger = new Logger(uid ? uid : null, seq ? seq : null, sessId ? sessId : null);
  app.logger = logger;
  let eventListener = new EventListener(logger);
  let collabKit = new CollabKit();

  collabKit.attachGUI(app.getGUI());
  app.attachKit(collabKit);
  app.attachEventListener(eventListener);

  BRIDGE.app = app;
  BRIDGE.collabKit = collabKit;
  BRIDGE.logger = eventListener.getLogger();

});
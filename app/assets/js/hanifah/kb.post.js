var BRIDGE = {};

class StudentPostKB {

  constructor() {
    this.gui = new GUI();
    this.session = new Session(baseUrl);
    this.ajax = Ajax.ins(this.gui);
    this.nextPage = 'posttest'
    this.controller = 'hanifah'
    this.logger = new Logger(uid ? uid : null, seq ? seq : null, sessId ? sessId : null);
    this.user = null;
    console.log(this.logger);
    this.handleEvent(this);
  }

  handleEvent(app) {

    $('#bt-continue').on('click', function () {
      if(!$('#check-understand').is(':checked')) {
        app.gui.dialog('Please check the "I understand" checkbox to continue.');
        return;
      }
      let next = $(this).data('next');
      app.ajax.post(baseUrl + 'experimentApi/checkTestTakenByCustomIdAndUid', {
        type: 'post',
        customid: 'PPT-POST',
        uid: uid
      }).then(function(result){
        if(result) {
          app.gui.dialog('<span class="text-danger">You have already taken this test.</span><br>You cannot take this test anymore.');
          return;
        } else {
          let confirm = app.gui.confirm('Start Post-Test?', {
            'positive-callback': function () {
              confirm.modal('hide');
              app.logger.log('begin-post-test');
              app.session.setMulti({
                page: next,
                seq: app.logger.seq + 1
              }, function () {
                window.location.href = baseUrl + app.controller + '/' + next;
              }, function (error) {
                app.gui.dialog(error, {
                  width: '300px'
                })
              })
            }
          })
        }
      });     
    });
  }
}

$(function () {
  BRIDGE.studentPostKB = new StudentPostKB();
});
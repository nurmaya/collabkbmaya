class KitBuildApp {

  constructor(canvas) {
    this.canvas = canvas;
    this.gui = new GUI();
    this.session = new Session(baseUrl);

    this.material = {
      mid: null,
      name: null,
      content: null
    }

    this.goalmap = {
      gmid: null,
      name: null
    }

    this.kit = null; // holder for kit socket communication
    this.handleEvent();
    this.canvas.attachEventListener(this);
  }

  getCanvas() {
    return this.canvas;
  }

  getGUI() {
    return this.gui;
  }

  getSession() {
    return this.session;
  }

  handleEvent() {
    let kitbuild = this;

    $('#bt-login').on('click', function (e) {
      $('#modal-chat-login').modal('show');
      $('#modal-chat-login .input-username').focus();
    });
    let doSignIn = function () {
      let username = $('#modal-chat-login .input-username').val().trim();
      let password = $('#modal-chat-login .input-password').val().trim();
      $.ajax({
        url: baseUrl + 'kitbuildApi/signIn',
        method: 'post',
        data: {
          username: username,
          password: password
        }
      }).done(function (response) { // console.log(response)
        if (!response.status) {
          kitbuild.gui.notify(response.error, {
            type: 'danger'
          });
          return;
        }
        let user = response.result;
        let uid = user.uid;
        let rid = user.role_id;
        let gids = user.gids ? user.gids.split(",") : [];
        kitbuild.kit.setUser(uid, username, rid, gids);
        kitbuild.signIn(uid, username, rid, gids);
        $('#modal-chat-login').modal('hide');
        $('#input-username').val('');
        $('#input-password').val('');
      }).fail(function (response) {
        kitbuild.gui.notify(response, {
          type: 'danger'
        });
      });
    }
    $('#modal-chat-login').on('click', '.bt-ok', doSignIn);
    $('#modal-chat-login .input-password').on('keyup', function (e) {
      if (e.keyCode == 13) doSignIn();
    });
    $('#bt-logout').on('click', function (e) {
      let confirmDialog = kitbuild.gui.confirm('Do you want to logout?', {
        'positive-callback': function () {
          kitbuild.session.destroy(() => {
            kitbuild.signOut();
            kitbuild.kit.leaveRoom(kitbuild.kit.room);
            confirmDialog.modal('hide');
          })
        }
      });
    });
    $('#bt-sync').on('click', (e) => {
      if (this.canvas.getCy().nodes().length) {
        let text = 'You currently have concept map on canvas.<br>If you sync the map, it will be <strong class="text-danger">replaced</strong>.<br>Continue?';
        let dialog = this.gui.confirm(text, {
          'positive-callback': function () {
            kitbuild.kit.syncMap();
            dialog.modal('hide');
          }
        })
      }
    });
    $('#bt-open-kit').on('click', function () {

      if(!kitbuild.kit.room.rid) {
        kitbuild.gui.dialog('Please join a room before opening a kit.');
        return;
      }

      $.ajax({
        url: baseUrl + 'kitbuildApi/getMaterialsWithGids',
        method: 'post',
        data: {
          gids: kitbuild.kit.user.gids
        }
      }).done(function (response) { // console.log(response)
        if (!response.status) {
          kitbuild.gui.notify(response.error ? response.error : response, {
            type: 'danger'
          });
          return;
        }
        let materials = response.result;

        let materialList = (!materials || materials.length == 0) ?
          '<em class="text-secondary">No material available</em>' :
          '';
        materials.forEach((material) => {
          materialList += '<div class="row align-items-center list list-hover list-pointer justify-content-between pt-2 pb-2 pl-2 pr-2" data-mid="' + material.mid + '" data-name="' + material.name + '">'
          materialList += material.name
          materialList += '<i class="fas fa-check text-primary" style="display:none"></i>'
          materialList += '</div>'
        });
        $('#modal-kit .material-list').html(materialList);
        $('#modal-kit').modal('show', {
          'width': '200px'
        });
      }).fail(function (response) {
        kitbuild.gui.notify(response, {
          type: 'danger'
        });
      })
    });
    $('#modal-kit .material-list').on('click', '.row', function () {
      if ($(this).hasClass('selected')) return;
      $('#modal-kit .material-list .row').find('i').hide();
      $('#modal-kit .material-list .row').removeClass('selected');
      $(this).find('i').show();
      $(this).addClass('selected')
      let mid = $(this).data('mid');
      let name = $(this).data('name');
      $('#modal-kit .bt-open').data('mid', mid);
      $('#modal-kit .bt-open').data('name', name);
      $('#modal-kit .bt-open').data('gmid', null);
      $('#modal-kit .bt-open').data('gmname', null);
      $('#modal-kit .goalmap-list').html('<em class="text-secondary">Loading goalmaps...</em>');
      $.ajax({
        url: baseUrl + 'kitbuildApi/getCollabGoalmaps/' + mid + '/' + kitbuild.kit.room.rid,
      }).done(function (response) { // console.log(response)
        if (!response.status) {
          kitbuild.gui.notify(response.error ? response.error : response.responseText, {
            type: 'danger'
          })
          return;
        }
        let goalmaps = response.result;
        let goalmapList = (!goalmaps || goalmaps.length == 0) ?
          '<em class="text-secondary">No goalmap available</em>' :
          '';
        goalmaps.forEach((goalmap) => {
          goalmapList += '<div class="row align-items-center list list-hover list-pointer justify-content-between pt-2 pb-2 pl-2 pr-2" data-gmid="' + goalmap.gmid + '" data-name="' + goalmap.name + '">'
          goalmapList += goalmap.name
          goalmapList += '<i class="fas fa-check text-primary" style="display:none"></i>'
          goalmapList += '</div>'
        });
        $('#modal-kit .goalmap-list').html(goalmapList);
      }).fail(function (response) { // console.log(response)
        kitbuild.gui.notify(response.responseText, {
          type: 'danger'
        })
      })
    });
    $('#modal-kit .goalmap-list').on('click', '.row', function () {
      if ($(this).hasClass('selected')) return;
      $('#modal-kit .goalmap-list .row').find('i').hide();
      $('#modal-kit .goalmap-list .row').removeClass('selected');
      $(this).find('i').show();
      $(this).addClass('selected')
      let gmid = $(this).data('gmid');
      let name = $(this).data('name');
      $('#modal-kit .bt-open').data('gmid', gmid);
      $('#modal-kit .bt-open').data('gmname', name);
    });
    $('#modal-kit').on('click', '.bt-open', function () {
      let name = ($(this).data('name'))
      let mid = ($(this).data('mid'))
      let gmid = ($(this).data('gmid'))
      let gmname = ($(this).data('gmname'))
      if (!gmid) {
        kitbuild.gui.dialog('Please select a Kit.', {
          width: '300px'
        });
        return;
      }
      let confirm = kitbuild.gui.confirm('Open kit <strong>\'' + gmname + '\' of \'' + name + '\'</strong>?', {
        'positive-callback': function () {
          kitbuild.loadKit(gmid, function () {
            confirm.modal('hide');
            $('#modal-kit').modal('hide');
          });
        },
        'width': '300px'
      })
    });
    $('#bt-show-material').on('click', function () {
      $('#popup-material').fadeToggle(200);
    });
    $('#popup-material .bt-close').on('click', function () {
      $('#popup-material .check-open').prop('checked', false);
      $('#popup-material').fadeOut(200);
    });
    $('#bt-save').on('click', function () {
      let gmid = kitbuild.goalmap.gmid;
      let rid = kitbuild.kit.room.rid;
      let uid = kitbuild.kit.user.uid;
      console.log(kitbuild);
      if (gmid != null && uid != null && rid != null) {
        kitbuild.saveLearnermapDraft(gmid, rid, uid);
      } else {
        if (!gmid) {
          kitbuild.gui.dialog('Please open a kit to save the map into.<br>A map should refer to a kit.');
          return;
        }
        if (!rid) {
          kitbuild.gui.dialog('Please join a room to save the map into.<br>In a collaboration environment, a saved maps should relate with a collaboration group. Therefore, you need to join a room before you create and save a concept map.');
          return;
        }
      }
    });
    $('#bt-load-draft').on('click', function () {
      let gmid = kitbuild.goalmap.gmid;
      let rid = kitbuild.kit.room.rid;
      // console.log(kitbuild, gmid, rid);
      if (gmid != null && rid != null) {
        let currentEles = kitbuild.getCanvas().getCy().nodes();
        if (currentEles.length) {
          let confirm = kitbuild.gui.confirm('You currently have existing map data on canvas. Loading saved draft map data will <strong class="text-danger">REPLACE</strong> current map on canvas and across <strong class="text-danger">ALL users</strong> in current room.<br>Continue?', {
            'positive-callback': function () {
              kitbuild.loadSavedLearnermapDraft(gmid, rid, function(){
                confirm.modal('hide');
              });
            }
          })
        } else kitbuild.loadSavedLearnermapDraft(gmid, rid);
      } else {
        if (!gmid) kitbuild.gui.dialog('Please select a kit. A concept map saved draft is related with a kit.')
        if (!rid) kitbuild.gui.dialog('Please select a room. A collaborative concept map saved draft is related with a collaboration room.')
      }
    });
    $('#bt-finalize').on('click', function () {
      let gmid = kitbuild.goalmap.gmid;
      let uid = kitbuild.kit.user.uid;
      let rid = kitbuild.kit.room.rid;
      if (gmid != null && uid != null && rid != null) {
        let confirm = kitbuild.gui.confirm('Save and finalize the map? <br>Any previously saved draft concept maps will be deleted, preserving the finalized one.', {
          'positive-callback': function () {
            kitbuild.saveLearnermapFinalize(gmid, rid, uid, function () {
              confirm.modal('hide');
            });
          },
          'negative-callback': function () {
            confirm.modal('hide');
          }
        })
      } else {
        if (!gmid) {
          kitbuild.gui.dialog('Please open a kit to save the map into.<br>A map should refer to a kit.');
          return;
        }
        if (!rid) {
          kitbuild.gui.dialog('Please join a room to save the map into.<br>In a collaboration environment, a saved maps should relate with a collaboration group. Therefore, you need to join a room before you create and save a concept map.');
          return;
        }
      }
    });

  }

  attachKit(kit) { // console.log(kit);
    this.kit = kit;
    this.kit.attachEventListener(this);
  }

  onKitEvent(event, data) {
    let gui = this.gui;
    let kit = this.kit;
    let canvas = this.canvas;
    switch (event) {
      case 'join-room':
        this.session.set('room', data.room);
        kit.rooms.forEach(room => {
          // console.log(room, kit)
          if (room.name == kit.room.name && room.users.length) {
            kit.syncMap();
            kit.syncMessages();
            return;
          }
        })
        $('#bt-sync').prop('disabled', false)
        break;
      case 'leave-room':
        this.session.unset('room');
        break;
      case 'room-update':
        kit.rooms = data;
        break;
      case 'user-sign-in':
        this.session.set('user', data);
        break;
      case 'user-sign-out':
        this.session.unset('user');
        break;
      case 'command-sign-out':
        this.signOut();
        this.kit.leaveRoom(this.kit.room);
        this.kit.enableRoom(false)
        this.kit.enableMessage(false)
        this.gui.dialog('You have been signed out');
        break;
      case 'command-leave-room':
        // this.signOut();
        // this.kit.leaveRoom(this.kit.room);
        // this.kit.enableRoom(false)
        // this.kit.enableMessage(false)
        // this.gui.dialog('You have been signed out');
        break;
      case 'request-map-data':
        let nodes = this.canvas.getCy().nodes();
        let edges = this.canvas.getCy().edges();
        let elesData = [];
        nodes.forEach(n => {
          elesData.push({
            group: 'nodes',
            data: n.data(),
            position: n.position()
          });
        });
        edges.forEach(n => {
          elesData.push({
            group: 'edges',
            data: n.data(),
            position: n.position()
          });
        });
        return elesData;
      case 'request-material-data':
        return this.material;
      case 'request-goalmap-data':
        return this.goalmap;
      case 'sync-messages-data':
        break;
      case 'sync-map-data': //console.log(data);
        if(data.length == 0) break;
        console.log('replace map');
        canvas.reset();
        canvas.clearCanvas();
        canvas.showMap(data);
        canvas.centerOneToOne();
        gui.notify('Map data synchronized', {
          type: 'success'
        })
        $('#bt-center').click();
        break;
      case 'sync-material-data':
        if (data.mid == null) return;
        this.setMaterial(data.mid, data.name, data.content);
        this.enableMaterial();
        break;
      case 'sync-goalmap-data':
        if (data.gmid == null) return;
        this.setGoalmap(data.gmid, data.name);
        break;
      case 'sync-map-data-error': // console.log('sync-map-data-error received')
        this.gui.notify(data.error, {
          type: 'danger'
        })
        break;
      case 'chat-message-open':
        this.session.set('chat-window-open', data)
        break;
      case 'receive-create-node':
        let empty = (this.canvas.getCy().nodes().length == 0);
        this.canvas.createNodeFromJSON(data.node);
        if (empty) $('#bt-center').click();
        break;
      case 'receive-move-node':
        this.canvas.moveNode(data.node.id, data.node.x, data.node.y);
        break;
      case 'receive-move-node-group': // including from move-drag-group event
        data.nodes.forEach(n => {
          this.canvas.moveNode(n.id, n.x, n.y);
        });
        break;
      case 'receive-connect-node':
        this.canvas.connect(data.connection);
        break;
      case 'receive-disconnect-node':
        this.canvas.disconnect(data.connection);
        break;
      case 'receive-duplicate-node':
        this.canvas.createNodeFromJSON(data.node);
        break;
      case 'receive-delete-node':
        this.canvas.deleteNode(data.node);
        break;
      case 'receive-delete-node-group':
        this.canvas.deleteNodes(data.nodes);
        break;
      case 'receive-update-node':
        this.canvas.updateNode(data.node.nodeId, data.node.value);
        break;
      case 'receive-center-link':
        this.canvas.moveNode(data.link.id, data.link.x, data.link.y);
        break;
      case 'receive-switch-direction':
        this.canvas.switchDirection(data.linkId);
        break;
    }
  }

  onCanvasEvent(event, data) { // console.log(event, data);
    if (!this.kit) return;
    switch (event) {
      case 'create-concept':
        this.kit.createNode(data);
        break;
      case 'create-link':
        this.kit.createNode(data);
        break;
      case 'drag-concept':
        this.kit.dragNode(data);
        break;
      case 'drag-link':
        this.kit.dragNode(data);
        break;
      case 'drag-node-group':
        this.kit.dragNodeGroup(data);
        break;
      case 'move-concept':
        this.kit.moveNode(data);
        break;
      case 'move-link':
        this.kit.moveNode(data);
        break;
      case 'move-node-group':
        this.kit.moveNodeGroup(data);
        break;
      case 'connect-left':
        this.kit.connectNode(data);
        break;
      case 'connect-right':
        this.kit.connectNode(data);
        break;
      case 'disconnect-left':
        this.kit.disconnectNode(data);
        break;
      case 'disconnect-right':
        this.kit.disconnectNode(data);
        break;
      case 'duplicate-node':
        this.kit.duplicateNode(data);
        break;
      case 'delete-node':
        this.kit.deleteNode(data);
        break;
      case 'update-node':
        this.kit.updateNode(data);
        break;
      case 'center-link':
        this.kit.centerLink(data);
        break;
      case 'switch-direction':
        this.kit.switchDirection(data);
        break;
        // case 'canvas-tool-clicked':
        //   console.log(data);
        //   break;
    }
  }

  onCanvasToolClicked(tool, node) { // console.log(tool, node);
    switch (tool.type) {
      case 'discuss-channel':
        let nodeId = node.id();
        let nodeType = node.data('type');
        let nodeLabel = node.data('name');
        this.kit.openChannel(nodeId, nodeType, nodeLabel);
        break;
    }
  }


  // Commanding Kit-Build App Interface

  signIn(uid, username, rid, gids) {
    $('#bt-login').hide();
    $('#bt-logout').show();
    $('#bt-logout-username').html(": " + username);
    $('#bt-open-kit').prop('disabled', false);
    this.kit.enableMessage();
    this.kit.enableRoom();
    this.enableSaveDraft();
    this.enableFinalize();
    // this.kit.setUser(uid, username, rid, gids);
    // this.initiateGoalmap();
  };

  signOut() {
    $('#bt-login').show();
    $('#bt-logout').hide();
    $('#bt-open-kit').prop('disabled', true);
    $('#bt-show-material').prop('disabled', true);
    this.kit.unsetUser();
    this.enableSaveDraft(false);
    this.enableFinalize(false);
    this.unsetMaterial();
  }

  enableMaterial(enabled = true) {
    $('#bt-show-material').prop('disabled', !enabled);
  }

  enableSaveDraft(enable = true) {
    $('#bt-save').prop('disabled', !enable);
    $('#bt-load-draft').prop('disabled', !enable);
  }

  enableFinalize(enable = true) {
    $('#bt-finalize').prop('disabled', !enable);
  }

  loadMaterial(mid, confirmDialog) {
    let kitbuild = this;
    $.ajax({
      url: baseUrl + 'kitbuildApi/getMaterialCollabByMid/' + mid,
      method: 'get'
    }).done(function (response) {
      if (!response.status) {
        kitbuild.gui.notify(response.error ? response.error : response, {
          type: 'danger'
        });
        return;
      }
      let material = response.result; // console.log(response)
      if (confirmDialog) confirmDialog.modal('hide');
      kitbuild.session.set('mid', material.mid);
      kitbuild.setMaterial(material.mid, material.name, material.content);
      kitbuild.kit.sendMaterial(material.mid, material.name, material.content);
      $('#modal-kit').modal('hide');
    }).fail(function (response) {
      kitbuild.gui.notify(response, {
        type: 'danger'
      });
    })
    $('#bt-show-material').prop('disabled', false);
  }

  setMaterial(mid, name, content) {
    this.material.mid = mid;
    this.material.name = name;
    this.material.content = content;
    $('#popup-material .material-title').html(this.material.name)
    $('#popup-material .material-content').html(this.material.content);
  }

  unsetMaterial() {
    this.material.mid = null;
    this.material.name = null;
    this.material.content = null;
  }

  setGoalmap(gmid, name) {
    this.goalmap.gmid = gmid;
    this.goalmap.name = name;
  }

  unsetGoalmap() {
    this.goalmap.gmid = null;
    this.goalmap.name = null;
  }

  saveLearnermap(gmid, rid, uid, type, callback) {
    let kitbuild = this;
    let learnermaps = {
      type: type,
      gmid: gmid,
      uid: uid,
      concepts: [],
      links: [],
      rid: rid
    }

    let cs = kitbuild.canvas.getNodes('[type="concept"]');
    let ls = kitbuild.canvas.getNodes('[type="link"]');
    let es = kitbuild.canvas.getEdges();
    // console.log(cs, ls, es);
    cs.forEach(c => {
      learnermaps.concepts.push({
        cid: c.data.id.substr(2),
        gmid: gmid,
        locx: c.position.x,
        locy: c.position.y
      });
    });
    ls.forEach(l => {
      let tl = {
        lid: l.data.id.substr(2),
        gmid: gmid,
        locx: l.position.x,
        locy: l.position.y,
        source: null,
        target: null
      };
      es.forEach((e) => {
        if (e.data.source == l.data.id) {
          if (e.data.type == 'right') tl.target = e.data.target.substr(2);
          if (e.data.type == 'left') tl.source = e.data.target.substr(2);
          return;
        }
      });
      learnermaps.links.push(tl);
    })
    $.ajax({
      url: baseUrl + 'kitbuildApi/saveLearnermapCollab',
      method: 'post',
      data: learnermaps
    }).done(function (response) { // console.log(response)
      if (!response.status) {
        kitbuild.gui.notify(response.error ? response.error : response.responseText, {
          type: 'danger',
          delay: 0
        });
        return;
      }
      let message = type == 'fix' ? 'Map finalized' : 'Map saved';
      kitbuild.gui.notify(message, {
        type: 'success'
      })
      if (callback) callback(response)
    }).fail(function (response) { // console.log(response)
      kitbuild.gui.notify(response.responseText, {
        type: 'danger',
        delay: 0
      });
      if (callback) callback(response)
    })
  }

  saveLearnermapDraft(gmid, rid, uid, callback) {
    this.saveLearnermap(gmid, rid, uid, 'draft', callback)
  }

  saveLearnermapFinalize(gmid, rid, uid, callback) {
    this.saveLearnermap(gmid, rid, uid, 'fix', callback)
  }

  saveLearnermapAuto(gmid, rid, uid, callback) {
    this.saveLearnermap(gmid, rid, uid, 'auto', callback)
  }

  checkAvailableLearnermapDraft(gmid, rid) {
    let kitbuild = this;
    $.ajax({
      url: baseUrl + 'kitbuildApi/getLastDraftLearnermap/' + gmid + '/' + rid,
      method: 'get'
    }).done(function (response) {
      // console.log(response)
      if (!response.status) {
        kitbuild.gui.notify(response.error ? response.error : response.responseText, {
          type: 'danger'
        });
        return;
      }
      let lmid = response.result;
      if (lmid) $('#bt-load-draft').prop('disabled', false);
    }).fail(function (response) {
      kitbuild.gui.notify(response, {
        type: 'danger'
      });
    })

  }

  loadSavedLearnermapDraft(gmid, rid, callback) { // console.log(mid, uid, rid);
    let kitbuild = this;
    $.ajax({
      url: baseUrl + 'kitbuildApi/loadLastDraftLearnermap/' + gmid + "/" + rid,
      method: 'get'
    }).done(function (response) { // console.log(response)
      if (!response.status) {
        kitbuild.gui.notify(response.error ? response.error : response.responseText, {
          type: 'danger',
          delay: 0
        });
        return;
      }
      if (response.result == null) {
        kitbuild.gui.notify('Nothing to load. No previously saved draft maps found.')
        return;
      }
      let learnermap = response.result.learnermap;
      let concepts = response.result.concepts;
      let links = response.result.links;

      let eles = [];
      concepts.forEach((c) => {
        eles.push({
          group: 'nodes',
          data: {
            id: 'c-' + c.cid,
            name: c.label,
            type: 'concept'
          },
          position: {
            x: parseInt(c.locx),
            y: parseInt(c.locy)
          }
        })
      })
      links.forEach((l) => {
        eles.push({
          group: 'nodes',
          data: {
            id: 'l-' + l.lid,
            name: l.label,
            type: 'link'
          },
          position: {
            x: parseInt(l.locx),
            y: parseInt(l.locy)
          }
        })
        if (l.source != null) {
          eles.push({
            group: 'edges',
            data: {
              source: 'l-' + l.lid,
              target: 'c-' + l.source,
              type: 'left'
            }
          })
        }
        if (l.target != null) {
          eles.push({
            group: 'edges',
            data: {
              source: 'l-' + l.lid,
              target: 'c-' + l.target,
              type: 'right'
            }
          })
        }
      })
      // let currentEles = kitbuild.canvas.getCy().nodes();
      // if (currentEles.length) {
      //   let confirm = kitbuild.gui.confirm('You currently have existing map data on canvas. Loading saved draft map data will <strong class="text-danger">REPLACE</strong> current map on canvas and across <strong class="text-danger">ALL users</strong> in current room.<br>Continue?', {
      //     'positive-callback': function () {
      //       confirm.modal('hide');
      //       kitbuild.canvas.clearCanvas();
      //       kitbuild.canvas.getCy().add(eles);
      //       kitbuild.gui.notify('Concept map loaded from last saved drafts.')
      //       kitbuild.kit.sendMap(eles);
      //       $('#bt-center').click();
      //     }
      //   })
      // } else {
      kitbuild.canvas.clearCanvas();
      kitbuild.canvas.getCy().add(eles);
      kitbuild.gui.notify('Concept map loaded from last saved drafts.')
      kitbuild.kit.sendMap(eles);
      kitbuild.kit.sendKit(kitbuild.goalmap)
      $('#bt-center').click();
      if (typeof callback == "function") callback();
      // }
    }).fail(function (response) {
      kitbuild.gui.notify(response.responseText, {
        type: 'danger'
      });
    })
  }

  loadKit(gmid, callback) {
    let kitbuild = this;
    $.ajax({
      url: baseUrl + 'kitbuildApi/openKit/' + gmid,
    }).done(function (response) { // console.log(response)
      if (!response.status) {
        kitbuild.gui.notify(response.error ? response.error : response.responseText, {
          type: 'danger'
        })
        return;
      }
      let goalmap = response.result.goalmap;
      let concepts = response.result.concepts;
      let links = response.result.links;
      // kitbuild.goalmap.gmid = goalmap.gmid;
      // kitbuild.goalmap.name = goalmap.name;
      // console.log(goalmap);
      kitbuild.setGoalmap(goalmap.gmid, goalmap.name);
      kitbuild.session.set('gmid', kitbuild.goalmap.gmid);
      let nodes = [];
      concepts.forEach(c => {
        nodes.push({
          group: "nodes",
          data: {
            id: "c-" + c.cid,
            name: c.label,
            type: 'concept'
          },
          position: {
            x: parseInt(c.locx),
            y: parseInt(c.locy)
          }
        })
      })
      links.forEach(l => {
        nodes.push({
          group: "nodes",
          data: {
            id: "l-" + l.lid,
            name: l.label,
            type: 'link'
          },
          position: {
            x: parseInt(l.locx),
            y: parseInt(l.locy)
          }
        })
      })
      // console.log(nodes)
      kitbuild.getCanvas().getCy().elements().remove();
      kitbuild.getCanvas().getCy().add(nodes);
      kitbuild.getCanvas().getCy().layout({
        name: 'grid',
        fit: false,
        condense: true,
        stop: function () {
          let nodes = kitbuild.getCanvas().getCy().nodes().toArray(); // console.log(nodes);
          let eles = [];
          nodes.forEach(node => {
            eles.push({
              group: 'nodes',
              data: {
                id: node.id(),
                name: node.data('name'),
                type: node.data('type')
              },
              position: {
                x: node.position().x,
                y: node.position().y
              }
            })
          })
          kitbuild.kit.sendKit(goalmap);
          kitbuild.kit.sendMap(eles);
          $('#bt-center').click();
        }
      }).run();
      if (typeof callback == 'function') callback();
    }).fail(function (response) { // console.log(response)
      kitbuild.gui.notify(response.responseText, {
        type: 'danger'
      })
    })
  }

}

jQuery.fn.center = function (parent) {
  parent = parent ? $(parent) : window
  this.css({
    "position": "absolute",
    "top": ((($(parent).height() - this.outerHeight()) / 2) + $(parent).scrollTop() + "px"),
    "left": ((($(parent).width() - this.outerWidth()) / 2) + $(parent).scrollLeft() + "px")
  });
  return this;
}
<?php

class HomeController extends GeneralController {

  function __construct()
  {
    parent::__construct('home');
  }

  function index() {
    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('bs-notify');

    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');

    $this->ui->addScript('js/general/logger.js');

    $this->ui->addScript('js/home/home.js', '?t=' . time());
    $this->ui->addStyle('css/home.css');
    $this->ui->view('home.php');
  }

  function register($rid, $fid) {
    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('bs-notify');

    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');

    $this->ui->addScript('js/general/logger.js');

    $this->ui->addScript('js/home/home.register.js', '?t=' . time());
    $this->ui->addStyle('css/general/base.css');

    $groupService = new GroupService();
    $data['group'] = $groupService->getGroupByFid($fid);
    $roleService = new RoleService();
    $data['role'] = $roleService->selectRole($rid);
    if(!$data['group']) throw CoreError::instance('Invalid Friendly ID');
    if(!$data['role']) throw CoreError::instance('Invalid Role ID');
    $this->ui->view('home/home.register.php', $data);
  }

  function p2() {

    // echo "A";

    $this->_check('p2', 'p2');
   
    $user              = (object) $_SESSION['user'];
    $materialService   = new MaterialService();
    $roomService       = new RoomService();
    $gids              = isset($user->gids) ? $user->gids : [];
    $data['materials'] = $materialService->getMaterialsWithGids($gids);
    $data['rooms']     = $roomService->selectRoomsByGids($gids);

    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('animate');
    $this->ui->addPlugin('bs-notify');

    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/general/ajax.js');

    $this->ui->addStyle('css/home.css');

    $view = '';

    switch($user->role_id) {
      case 'CON':
        $view = 'home/home.teacher.p2.php';
        $this->ui->addScript('js/home/home.teacher.p2.js', '?t=' . time());
      break;
      case 'KIT':
        $view = 'home/home.student.p2.php';
        $this->ui->addScript('js/home/home.student.p2.js', '?t=' . time());
      break;
      case 'IKB':
        $view = 'home/home.individualkb.p2.php';
        $this->ui->addScript('js/home/home.individualkb.p2.js', '?t=' . time());
      break;
    }
    $this->ui->addScript('vendors/socket.io.slim.js', '?t=' . time());
    $this->ui->addScript('js/collab/collab.kit.support.js');
    $this->ui->addScript($this->ui->location('admin/supportscript'), '?t=' . time());
    $this->ui->view($view, $data);
  }

  function cmap() { // exit;

    $this->_check('cmap', 'cmap');

    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('jqui');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('animate');
    $this->ui->addPlugin('bs-notify');
    $this->ui->addPlugin('tippy');
    $this->ui->addPlugin('cytoscape');
    $this->ui->addPlugin('kbui', '?t=' . time());

    $this->ui->addScript('vendors/socket.io.slim.js', '?t=' . time());
    $this->ui->addScript('js/collab/collab.channel.tool.js', '?t=' . time());
    $this->ui->addScript('js/collab/collab.message.js', '?t=' . time());
    $this->ui->addScript('js/collab/collab.kit.js', '?t=' . time());

    $this->ui->addStyle('css/chat/chat.css', '?t=' . time());

    $this->ui->addStyle('css/general/three-dots.css', '?t=' . time());
    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/general/eventlistener.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');

    $this->ui->addScript('js/home/home.teacher.cmap.app.js', '?t=' . time());
    $this->ui->addScript('js/home/home.teacher.cmap.js', '?t=' . time());
    $this->ui->addScript($this->ui->location('home/tscript'), '?t=' . time());

    $this->ui->addStyle('css/home/home.cmap.css', '?t=' . time());
    $this->ui->view('home/home.cmap.php');
  }

  function tscript() {
    header('Content-Type: text/javascript');
    echo '/*' . "\n";
    echo print_r($_SESSION);
    echo "\n" . '*/' . "\n";
    echo '$(function(){' . "\n";
    if (isset($_SESSION['user'])) {
      $u    = (object) $_SESSION['user'];
      $gids = [];
      foreach ($u->gids as $gid) {
        $gids[] = "'$gid'";
      }
      echo "let gids = [" . implode(",", $gids) . "];\n";
      echo "BRIDGE.app.kit.setUserWithName('$u->uid','$u->username','$u->name','$u->role_id', gids);\n";
      echo "BRIDGE.app.signIn($u->uid,'$u->username', '$u->role_id', gids);\n";
    }
    if (isset($_SESSION['chat-window-open']) && $_SESSION['chat-window-open']) {
      echo "BRIDGE.collabKit.chatWindowOpen = " . $_SESSION['chat-window-open'] . ";\n";
    }

    if (isset($_SESSION['mid'])) {
      echo "BRIDGE.app.loadMaterial('$_SESSION[mid]');\n";
      echo "BRIDGE.logger.setMid('$_SESSION[mid]');\n";
    }

    if (isset($_SESSION['rid'])) {
      echo "BRIDGE.app.kit.loadRoom('$_SESSION[rid]', function(room){
        BRIDGE.app.kit.joinRoom(room);
        BRIDGE.logger.setRid('$_SESSION[rid]');
      });\n";
    }

    // if (isset($_SESSION['room'])) {
    //   $r = $_SESSION['room'];
    //   echo "BRIDGE.collabKit.joinRoom({rid:'$r[rid]',name:'$r[name]'});\n";
    // }
    echo '})';
  }

  function kb() {

    $this->_check('kb', 'kb');

    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('jqui');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('animate');
    $this->ui->addPlugin('bs-notify');
    $this->ui->addPlugin('tippy');
    $this->ui->addPlugin('cytoscape');
    $this->ui->addPlugin('kbui', '?t=' . time());

    $this->ui->addScript('vendors/socket.io.slim.js', '?t=' . time());
    
    $this->ui->addScript('js/collab/collab.channel.tool.js', '?t=' . time());
    $this->ui->addScript('js/collab/collab.message.js', '?t=' . time());
    $this->ui->addScript('js/collab/collab.kit.js', '?t=' . time());

    $this->ui->addStyle('css/chat/chat.css', '?t=' . time());

    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/general/eventlistener.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');

    $this->ui->addScript('js/home/home.student.kb.app.js', '?t=' . time());
    $this->ui->addScript('js/home/home.student.kb.js', '?t=' . time());
    $this->ui->addScript($this->ui->location('home/sscript'), '?t=' . time());

    $this->ui->addStyle('css/kb.css', '?t=' . time());
    $this->ui->view('home/home.kb.php');
  }

  function sscript() {
    header('Content-Type: text/javascript');
    echo '/*' . "\n";
    echo print_r($_SESSION);
    echo "\n" . '*/' . "\n";
    echo '$(function(){' . "\n";
    if (isset($_SESSION['user'])) {
      $u    = (object) $_SESSION['user'];
      $gids = [];
      if(property_exists($u, 'gids'))
      foreach ($u->gids as $gid) {
        $gids[] = "'$gid'";
      }
      echo "let gids = [" . implode(",", $gids) . "];\n";
      echo "BRIDGE.app.kit.setUser('$u->uid','$u->username', '$u->role_id', gids);\n";
      echo "BRIDGE.app.signIn($u->uid,'$u->username', '$u->role_id', gids);\n";
    }
    if (isset($_SESSION['chat-window-open']) && $_SESSION['chat-window-open']) {
      echo "BRIDGE.collabKit.chatWindowOpen = " . $_SESSION['chat-window-open'] . ";\n";
    }

    if (isset($_SESSION['mid'])) {
      echo "BRIDGE.app.loadMaterial('$_SESSION[mid]');\n";
      // echo "BRIDGE.logger.setMid('$_SESSION[mid]');\n";
    }

    if (isset($_SESSION['gmid'])) {
      echo "BRIDGE.app.loadKit('$_SESSION[gmid]');\n";
      echo "BRIDGE.logger.setGmid('$_SESSION[gmid]');\n";
    }

    if (isset($_SESSION['rid'])) {
      echo "BRIDGE.app.kit.loadRoom('$_SESSION[rid]', function(room){
        BRIDGE.app.kit.joinRoom(room);
        BRIDGE.logger.setRid('$_SESSION[rid]');
      });\n";
    }

    echo '})';
  }

  function ikb() {

    $this->_check('ikb', 'ikb');

    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('jqui');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('animate');
    $this->ui->addPlugin('bs-notify');
    $this->ui->addPlugin('tippy');
    $this->ui->addPlugin('cytoscape');
    $this->ui->addPlugin('kbui', '?t=' . time());

    // $this->ui->addScript('vendors/socket.io.slim.js', '?t=' . time());
    
    // $this->ui->addScript('js/collab/collab.channel.tool.js', '?t=' . time());
    // $this->ui->addScript('js/collab/collab.message.js', '?t=' . time());
    // $this->ui->addScript('js/collab/collab.kit.js', '?t=' . time());

    $this->ui->addStyle('css/chat/chat.css', '?t=' . time());

    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/general/eventlistener.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');
    $this->ui->addScript('js/home/home.student.ikb.js', '?t=' . time());
    $this->ui->addScript($this->ui->location('home/iscript'), '?t=' . time());
    $this->ui->addStyle('css/kb.css', '?t=' . time());
    $this->ui->view('home/home.ikb.php');
  }

  function iscript() {
    header('Content-Type: text/javascript');
    echo '/*' . "\n";
    echo print_r($_SESSION);
    echo "\n" . '*/' . "\n";
    echo '$(function(){' . "\n";
    if (isset($_SESSION['user'])) {
      $u    = (object) $_SESSION['user'];
      $gids = [];
      foreach ($u->gids as $gid) {
        $gids[] = "'$gid'";
      }
      echo "let gids = [" . implode(",", $gids) . "];\n";
      // echo "BRIDGE.app.kit.setUser('$u->uid','$u->username', '$u->role_id', gids);\n";
      echo "BRIDGE.app.signIn($u->uid,'$u->username', '$u->role_id', gids);\n";
    }
    // if (isset($_SESSION['chat-window-open']) && $_SESSION['chat-window-open']) {
    //   echo "BRIDGE.collabKit.chatWindowOpen = " . $_SESSION['chat-window-open'] . ";\n";
    // }

    if (isset($_SESSION['mid'])) {
      echo "BRIDGE.app.loadMaterial('$_SESSION[mid]');\n";
    }

    if (isset($_SESSION['gmid'])) {
      echo "BRIDGE.app.loadKit('$_SESSION[gmid]');\n";
      echo "BRIDGE.logger.setGmid('$_SESSION[gmid]');\n";
    }

    // if (isset($_SESSION['rid'])) {
    //   echo "BRIDGE.app.kit.loadRoom('$_SESSION[rid]', function(room){
    //     BRIDGE.app.kit.joinRoom(room);
    //     BRIDGE.logger.setRid('$_SESSION[rid]');
    //   });\n";
    // }

    echo '})';
  }

  function imap() {

    $this->_check('imap', 'imap');

    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('jqui');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('animate');
    $this->ui->addPlugin('bs-notify');
    $this->ui->addPlugin('tippy');
    $this->ui->addPlugin('cytoscape');
    $this->ui->addPlugin('kbui', '?t=' . time());

    // $this->ui->addScript('vendors/socket.io.slim.js', '?t=' . time());
    
    // $this->ui->addScript('js/collab/collab.channel.tool.js', '?t=' . time());
    // $this->ui->addScript('js/collab/collab.message.js', '?t=' . time());
    // $this->ui->addScript('js/collab/collab.kit.js', '?t=' . time());

    $this->ui->addStyle('css/chat/chat.css', '?t=' . time());

    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/logger.js');
    $this->ui->addScript('js/general/eventlistener.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');
    $this->ui->addScript('js/home/home.student.imap.js', '?t=' . time());
    $this->ui->addScript($this->ui->location('home/imapscript'), '?t=' . time());
    $this->ui->addStyle('css/kb.css', '?t=' . time());
    $this->ui->view('home/home.imap.php');
  }

  function imapscript() {
    header('Content-Type: text/javascript');
    echo '/*' . "\n";
    echo print_r($_SESSION);
    echo "\n" . '*/' . "\n";
    echo '$(function(){' . "\n";
    if (isset($_SESSION['user'])) {
      $u    = (object) $_SESSION['user'];
      $gids = [];
      foreach ($u->gids as $gid) {
        $gids[] = "'$gid'";
      }
      echo "let gids = [" . implode(",", $gids) . "];\n";
      // echo "BRIDGE.app.kit.setUser('$u->uid','$u->username', '$u->role_id', gids);\n";
      echo "BRIDGE.app.signIn($u->uid,'$u->username', '$u->role_id', gids);\n";
    }
    // if (isset($_SESSION['chat-window-open']) && $_SESSION['chat-window-open']) {
    //   echo "BRIDGE.collabKit.chatWindowOpen = " . $_SESSION['chat-window-open'] . ";\n";
    // }

    if (isset($_SESSION['mid'])) {
      echo "BRIDGE.app.loadMaterial('$_SESSION[mid]');\n";
    }

    if (isset($_SESSION['gmid'])) {
      echo "BRIDGE.logger.setGmid('$_SESSION[gmid]');\n";
      if(isset($_SESSION['lmid'])) echo "BRIDGE.app.loadLearnermap('$_SESSION[lmid]');\n";
    }

    // if (isset($_SESSION['rid'])) {
    //   echo "BRIDGE.app.kit.loadRoom('$_SESSION[rid]', function(room){
    //     BRIDGE.app.kit.joinRoom(room);
    //     BRIDGE.logger.setRid('$_SESSION[rid]');
    //   });\n";
    // }

    echo '})';
  }

  function finish() {

    $this->_check('finish', 'finish');
    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addStyle('css/home/home.finish.css', '?t=' . time());
    $this->ui->view('home/home.finish.php');
  }

  function questionnaire() {

    $user = (object) $_SESSION['user'];

    $gid         = 22; // $user->gids[0];
    $type        = 'questionnaire';
    $qsetService = new QsetService();
    $qset        = $qsetService->selectQsetWithQuestionsByGidAndTypeAndCustomId($gid, $type, 'A');

    $data['qset'] = $qset;

    $this->ui->addPlugin('jquery');
    $this->ui->addPlugin('bootstrap');
    $this->ui->addPlugin('bs-notify');

    $this->ui->addScript('js/general/notification.js');
    $this->ui->addScript('js/general/gui.js');
    $this->ui->addScript('js/general/session.js');
    $this->ui->addScript('js/general/ajax.js');
    $this->ui->addScript('js/general/logger.js');

    // $this->ui->addStyle('css/read.css');

    $this->ui->addScript('js/home/home.questionnaire.js', '?t=' . time());
    $this->ui->addScript($this->ui->location('home/questionnairescript/' . $qset->qsid), '?t=' . time());
    $this->ui->view('home/home.questionnaire.php', $data);
  }

  function questionnairescript($qsid) {
    header('Content-Type: text/javascript');
    echo '$(function(){' . "\n";
    if (isset($_SESSION['user'])) {
      $user  = (object) $_SESSION['user'];
      $gids  = $user->gids;
      $grups = $user->grups;
      echo "BRIDGE.app.gid = $gids[0]\n";
      echo "BRIDGE.app.grup = '$grups[0]'\n";
      echo "BRIDGE.app.qsid = $qsid\n";
    }
    echo '})';
  }

  function e() {
    throw CoreError::instance('Error example.');
  }

}
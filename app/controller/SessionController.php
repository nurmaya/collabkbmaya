<?php

class SessionController extends CoreController {

  function set() {

    $key = isset($_POST['key']) ? $_POST['key'] : null;
    $data = isset($_POST['data']) ? $_POST['data'] : null;
    if($key) $_SESSION[$key] = $data;
    else {
      foreach($_POST as $k => $v) $_SESSION[$k] = $v;
    }
    try {
      // if(isset($_SESSION['user']))
      // {
      //   $user = (object)$_SESSION['user'];
      //   $uid = $user->uid;
      //   $sessionService = new SessionService();
      //   $sessionService->setSessionData($uid, $_SESSION);
      // }
      CoreResult::instance(true)->show();
    } catch(Exception $ex) {
      CoreResult::instance($ex->getMessage())->show();
    }
  }

  function get() {
    $key = (isset($_POST['key']) && !empty($_POST['key'])) ? $_POST['key'] : null;
    $data = $key !== null ? (isset($_SESSION[$key]) ? $_SESSION[$key] : null) : $_SESSION;
    echo json_encode($data);
  }

  function unset() {
    $key = (isset($_POST['key'])) ? $_POST['key'] : null;
    if($key !== null) unset($_SESSION[$key]);
    echo json_encode(true);
  }

  function destroy() {
    session_destroy();
    echo json_encode(true);
  }

  function dump() {
    header('Content-Type:text/plain');
    var_dump($_SESSION);
  }

}
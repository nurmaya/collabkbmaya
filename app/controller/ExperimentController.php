<?php

class ExperimentController extends CoreController {

  protected $controller = null;

  public function __construct($controller)
  {
    $this->controller = $controller;
    parent::__construct();
  }

  protected function _check($page, $allowedSources = "") {
    // if (!isset($_SESSION['user']) || !isset($_SESSION['page'])) {
    //   $this->redirect($this->controller);
    //   exit;
    // }
    // if (in_array($_SESSION['page'], explode(':', $allowedSources))) {
    //   $_SESSION['page'] = $page;
    //   return true;
    // }
    // $this->redirect($this->controller . '/' . $_SESSION['page']);
    // exit;
  }

  function signOut() {
    try {
      $user = isset($_SESSION['user']) ? (object) $_SESSION['user'] : null;
      if ($user and $uid = $user->uid) {
        $sessionService = new SessionService();
        $sessionService->clearSessionData($uid);
      }
    } catch (Exception $ex) {
      CoreError::instance(false)->show();
      exit;
    }
    session_destroy();
    $this->redirect($this->controller);
    exit;
  }

}
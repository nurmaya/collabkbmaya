<!-- Modal Create Qset -->
<div class="modal" id="modal-create-qset" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Create New Question Set</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Cancel">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group mb-2" style="position:relative">
          <label>Set Name</label>
          <input type="text" class="form-control input-qset-name" required>
          <small class="modal-helper-text">Enter a qset name. Question Set name cannot empty.</small>
        </div>
        <div class="form-group mb-2" style="position:relative">
          <label>Type</label>
          <select type="text" class="form-control input-qset-type">
            <option value="pre">Pre Test</option>
            <option value="post">Post Test</option>
            <option value="mid">Mid Test</option>
            <option value="delay">Delay Test</option>
            <option value="final">Final Test</option>
            <option value="questionnaire">Questionnaire</option>
            <option value="custom" selected="selected">Custom Test</option>
          </select>
        </div>
        <div class="form-group mb-2" style="position:relative">
          <label>Custom ID for Type</label>
          <input type="text" class="form-control input-qset-customid">
        </div>
        <div class="form-group mb-2" style="position:relative">
          <label>Randomizing Questions</label>
          <select type="text" class="form-control input-qset-randomize">
            <option value="all" selected="selected">Question and Option</option>
            <option value="question">Question Only</option>
            <option value="option">Option Only</option>
            <option value="none">No Randomize</option>
          </select>
        </div>
        <div class="form-group mb-2" style="position:relative">
          <label>Assign Material</label>
          <select type="text" class="form-control input-qset-material"></select>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-primary bt-dialog bt-ok">OK</button>
        <button type="button" class="btn btn-sm btn-secondary bt-dialog bt-close" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal Create Qset -->

<!-- Modal Edit Qset -->
<div class="modal" id="modal-edit-qset" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit Question Set</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Cancel">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group mb-2" style="position:relative">
          <label>Set Name</label>
          <input type="text" class="form-control input-qset-name" required>
          <small class="modal-helper-text">Enter a question set name. Question Set name cannot empty.</small>
        </div>
        <div class="form-group mb-2" style="position:relative">
          <label>Type</label>
          <select type="text" class="form-control input-qset-type">
            <option value="pre">Pre Test</option>
            <option value="post">Post Test</option>
            <option value="mid">Mid Test</option>
            <option value="delay">Delay Test</option>
            <option value="final">Final Test</option>
            <option value="questionnaire">Questionnaire</option>
            <option value="custom">Custom Test</option>
          </select>
        </div>
        <div class="form-group mb-2" style="position:relative">
          <label>Custom ID for Type</label>
          <input type="text" class="form-control input-qset-customid">
        </div>
        <div class="form-group mb-2" style="position:relative">
          <label>Randomizing Questions</label>
          <select type="text" class="form-control input-qset-randomize">
            <option value="all" selected="selected">Question and Option</option>
            <option value="question">Question Only</option>
            <option value="option">Option Only</option>
            <option value="none">No Randomize</option>
          </select>
        </div>
        <div class="form-group mb-2" style="position:relative">
          <label>Assign Material</label>
          <select type="text" class="form-control input-qset-material"></select>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-primary bt-dialog bt-ok">OK</button>
        <button type="button" class="btn btn-sm btn-secondary bt-dialog bt-close" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal Edit Qset -->

<!-- Modal Create Question -->
<div class="modal" id="modal-create-question" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Create New Question</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Cancel">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group mb-2" style="position:relative">
          <label>Question</label>
          <textarea type="text" class="form-control input-question-question" required></textarea>
        </div>
        <div class="form-group mb-2" style="position:relative">
          <label>Type</label>
          <select type="text" class="form-control input-question-type">
            <option value="multi" selected="selected">Multiple Choice</option>
            <option value="multi-answer">Multiple Answers</option>
            <option value="essay">Essay</option>
          </select>
        </div>
        <hr>
        <div class="mb-2 row justify-content-between" style="position:relative">
          <div class="col"><label>Answer Options</label> <button class="bt-clear-options btn btn-outline-danger btn-sm"><i class="fas fa-trash-alt"></i></button></div>
          <div class="col">
            <!-- <div class="btn-group">
              <button class="bt-list-option btn btn-sm btn-outline-primary"><i class="fas fa-sync"></i></button>
            </div> -->
          </div>
        </div>
        <div>
          <div class="input-group input-sm">
            <input type="text" class="form-control form-control-sm input-option-option" placeholder="Option text">
            <button class="bt-create-option btn btn-sm btn-outline-primary"><i class="fas fa-plus"></i> Add Option</button>
          </div>
          
        </div>
        <hr>
        <div class="list-option list-container"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-primary bt-dialog bt-ok">OK</button>
        <button type="button" class="btn btn-sm btn-secondary bt-dialog bt-close" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal Create Question -->

<!-- Modal Edit Question -->
<div class="modal" id="modal-edit-question" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit Question</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Cancel">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group mb-2" style="position:relative">
          <label>Question</label>
          <textarea type="text" class="form-control input-question-question" required></textarea>
        </div>
        <div class="form-group mb-2" style="position:relative">
          <label>Type</label>
          <select type="text" class="form-control input-question-type">
            <option value="multi">Multiple Choice</option>
            <option value="multianswer">Multiple Answers</option>
            <option value="essay">Essay</option>
          </select>
        </div>
        <hr>
        <div class="mb-2 row justify-content-between" style="position:relative">
          <div class="col"><label>Answer Options</label></div>
          <div class="col">
            <!-- <div class="btn-group">
              <button class="bt-list-option btn btn-sm btn-outline-primary"><i class="fas fa-sync"></i></button>
            </div> -->
          </div>
        </div>
        <div>
          <div class="input-group input-sm">
            <input type="text" class="form-control form-control-sm input-option-option" placeholder="Option text">
            <button class="bt-create-option btn btn-sm btn-outline-primary"><i class="fas fa-plus"></i> Add Option</button>
          </div>
          
        </div>
        <hr>
        <div class="list-option list-container"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-primary bt-dialog bt-ok">OK</button>
        <button type="button" class="btn btn-sm btn-secondary bt-dialog bt-close" data-dismiss="modal">Cancel</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal Edit Question -->

<?php $this->view('admin/test/test.modal.relation.php'); ?>
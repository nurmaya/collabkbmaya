<?php $this->view('base/header.php'); ?>

<div class="cover-container d-flex h-100 p-3 mx-auto flex-column" style="width: 42em;">
  <header class="masthead">
    <div class="inner">
      <h3 class="masthead-brand">Kit-Build</h3>
      <nav class="nav nav-masthead justify-content-center">
        <!-- <a class="btn btn-danger" href="<?php echo $this->location('home/signOut'); ?>">Sign Out</a> -->
        <!-- <a class="nav-link" href="#">Features</a>
        <a class="nav-link" href="#">Contact</a> -->
      </nav>
    </div>
  </header>

  <main role="main" class="inner cover text-center justify-content-center">

    <div id="section-kit" class="mt-5">
      <p class="lead" style="font-weight: 500;">Please select a kit:</p>
      <div class="row justify-content-center">
        <div id="list-goalmap" class="list col-sm-6">
          <em class="text-secondary">Select a kit...</em>
        </div>
      </div>
    </div>
   
    <button id="bt-continue" class="btn btn-lg btn-primary mt-5"
      <?php if(isset($_SESSION['mid'])) echo 'data-mid="' . $_SESSION['mid'] . '"'; ?>
      <?php if(isset($_SESSION['rid'])) echo 'data-rid="' . $_SESSION['rid'] . '"'; ?>>Continue</button>
  </main>

  <footer class="mastfoot mt-5">
    
  </footer>

</div>

<?php $this->view('home/home.modal.php'); ?>
<?php $this->view('general/general.ui.php'); ?>
<?php $this->view('base/footer.php'); ?>
<?php $this->view('base/header.php'); ?>

<div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
  <header class="masthead mb-auto">
    <div class="inner">
      <nav class="nav nav-masthead justify-content-center">
        
      </nav>
    </div>
  </header>

  <main role="main" class="inner cover text-left mx-auto" style="width: 42em;">
    <h1 class="h3">Thank You!</h1> 
    <h1 class="h4 cover-heading text-info">ありがとうございました！</h1>
    <hr>
    <p>Anda baru saja mengerjakan peta konsep yang membantu Anda mendeskripsikan pemahaman Anda terkait materi/topik yang diberikan. Selanjutnya Anda diminta untuk menjawab soal post test untuk menguji pemahaman Anda.</p>
    <p>Anda diberi waktu <strong class="text-danger">10 menit</strong> untuk menjawab seluruh pertanyaan yang diberikan. Selama menjawab soal post-test, Anda tidak diperbolehkan untuk melihat dan membaca materi yang diberikan.</p>
    <hr>
    <p>Tunggu instruksi yang diberikan sebelum Anda melanjutkan ke tahap selanjutnya.</p>
    <button id="bt-continue" class="btn btn-primary btn-lg" data-next="<?php echo $nextPage; ?>">Let's Go!</button>
  </main>

  <footer class="mastfoot mt-auto">
    <div class="inner text-center">
    </div>
  </footer>
</div>

<?php $this->view('general/general.ui.php'); ?>
<?php $this->view('base/footer.php'); ?>
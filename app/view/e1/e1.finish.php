<?php $this->view('base/header.php'); ?>

<div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
  <header class="masthead mb-auto">
    <div class="inner">
      <h3 class="masthead-brand">Kit-Build</h3>
      <nav class="nav nav-masthead justify-content-center">
        <a class="btn btn-danger" href="<?php echo $this->location('e1/signOut'); ?>">Sign Out</a>
        <!-- <a class="nav-link" href="#">Features</a>
        <a class="nav-link" href="#">Contact</a> -->
      </nav>
    </div>
  </header>

  <main role="main" class="inner cover text-center mx-auto" style="width: 42em;">
    <h1 class="h1">Thank You!</h1> 
    <h1 class="cover-heading text-info">ありがとうございました！</h1>
    <p>Thank you for participating in this experiment. Hope you are enjoying the experience in concept mapping using Kit-Build.</p>
    <hr>
    <p>Please kindly fill the following questionnaire by clicking the button below.</p>
    <button class="btn btn-primary btn-lg">Let's Go!</button>
  </main>

  <footer class="mastfoot mt-auto">
    <div class="inner text-center">
      <p>This page is a modified template page from <a href="https://getbootstrap.com/">Bootstrap</a>, by <a
          href="https://twitter.com/mdo">@mdo</a>. Part of the implementation of this system is made beautifully using <a href="https://js.cytoscape.org/">Cytoscape.js</a>. <a href="https://opensource.org/licenses/MIT">MIT</a> licensed.</p>
    </div>
  </footer>
</div>

<?php $this->view('general/general.ui.php'); ?>
<?php $this->view('base/footer.php'); ?>
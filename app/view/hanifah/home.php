<?php $this->view('base/header.php');?>

<div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
  <header class="masthead mb-auto">
    <div class="inner">
      <!-- <h3 class="masthead-brand">Kit-Build</h3>
      <nav class="nav nav-masthead justify-content-center">
        <a class="nav-link active" href="#">Home</a>
        &nbsp;
      </nav> -->
    </div>
  </header>

  <main role="main" class="inner cover text-center">
    <h1 class="cover-heading">Kit-Build Concept Map</h1>
    <p>Kit-Build Concept Map is a digital tool for supporting a concept map strategy to represent instructor’s
      expectation and assess learners' current understanding as well as for formative assessment and visualizing the
      gaps between the understanding of learners and instructor’s expectation.</p>
    <hr style="border-color: #ccc">
    <p class="lead" style="font-weight: 500">This is an experimental page of the implementation of Collaborative
      Kit-Build Concept Map Framework in collaborative context where learners and/or teacher collaborate in their concept mapping activities.</p>
    <hr style="border-color: #ccc">
    <p class="lead font-italic text-info" style="font-weight: 500">So, what are we doing now?</p>
    <div class="row justify-content-center">
      <div class="col d-flex flex-column">
        <button href="#" id="bt-cmap" class="btn btn-lg btn-primary"><strong>Concept Mapping</strong></button>
      </div>
      <div class="col d-flex flex-column">
        <button href="#" id="bt-kb" class="btn btn-lg btn-info"><strong>Collaborative Kit-Building</strong></button>
      </div>
      <!-- <div class="col d-flex flex-column">
        <a href="#" id="bt-ikb" class="btn btn-lg btn-secondary"><strong>Individual<br>Kit-Building</strong></a>
      </div> -->
    </div>
    <hr>
    <h4 class="text-center mb-3">Multimedia Interaktif</h4>
    <div class="row justify-content-center">
      <div class="col d-flex flex-column">
        <button href="#" id="bt-icmap" class="btn btn-lg btn-warning"><strong>Individual Concept Mapping</strong></button>
      </div>
      <div class="col d-flex flex-column">
        <button href="#" id="bt-ikb" class="btn btn-lg btn-danger"><strong>Individual Kit-Building</strong></button>
      </div>
      <!-- <div class="col d-flex flex-column">
        <a href="#" id="bt-ikb" class="btn btn-lg btn-secondary"><strong>Individual<br>Kit-Building</strong></a>
      </div> -->
    </div>
    <p class="mt-3">Please prepare username and password given to you and use any of the following recommended web browser for full compatibility.</p>
    <p>
      <a href="https://www.google.com/chrome/"><img src="<?php echo $this->assets('images/chrome.png'); ?>" style="height: 64px"></a>
      <a href="https://www.microsoft.com/en-us/edge"><img src="<?php echo $this->assets('images/edge.png'); ?>" style="height: 64px"></a>
      <a href="https://www.mozilla.org/en-US/firefox/new/"><img src="<?php echo $this->assets('images/firefox.png'); ?>" style="height: 64px"></a>
    </p>
  </main>

  <footer class="mastfoot mt-auto">
    <div class="inner text-center">
      <p>This page is a modified template page from <a href="https://getbootstrap.com/">Bootstrap</a>, by <a
          href="https://twitter.com/mdo">@mdo</a>. Part of the implementation of this system is made beautifully using
        <a href="https://js.cytoscape.org/">Cytoscape.js</a>. <a href="https://opensource.org/licenses/MIT">MIT</a>
        licensed.</p>
    </div>
  </footer>
</div>

<?php $this->view('home/home.modal.php');?>
<?php $this->view('general/general.ui.php');?>
<?php $this->view('base/footer.php');?>
<?php $this->view('base/header.php');?>

<div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
  <header class="masthead mb-auto">
    <div class="inner">
      <nav class="nav nav-masthead justify-content-center">
        <!-- <a class="nav-link" href="#">Features</a>
        <a class="nav-link" href="#">Contact</a> -->
      </nav>
    </div>
  </header>

  <main role="main" class="inner cover mx-auto mt-5 pt-5">
    <h1 class="cover-heading mb-5 text-center">Instructions and Consents</h1>
    <p>Greetings! We are happy to invite you to participate in a research study titled: “Investigation of concept
      mapping activity in an online real-time collaboration environment with Kit-Build.” This study is held by Aryo
      Pinandito and Lia Sadita from Learning Engineering Laboratory, Department of Information Engineering, Hiroshima
      University.</p>
    <p>The purpose of this research study is to investigate a collaborative concept mapping activity using Kit-Build in
      comprehending reading material. If you agree to take part in this study, you will be asked to complete tasks
      related with concept mapping. You will be requested to read a text, answer questions related with the text, build
      the map, fill in some questionnaires, and then answering the questions again.</p>
    <p>Every task in this experiment has a time limit, so please give the experiment your full attention if you choose
      to participate. You may not directly gain benefit from participating in this research; however, we hope that your
      participation in this study may improve the use of concept maps in schools or other teaching and learning
      environments.</p>
    <p>We believe that there are no known risks associated with this research study; however, as with any online related
      activity, the risk of a breach of confidentiality is always possible. To the best of our ability, your answers
      related with this study will be kept confidential. The results of this study may be published as a part of a
      journal paper, conference paper, or book chapter. All the records will be used solely for educational purposes. We
      will not include any information in any report that we may publish that would make it possible to identify you.
    </p>
    <p>Your participation in this study is completely voluntary and you can withdraw at any time. If you have any
      further questions about the study, at any time feel free to contact me, Aryo Pinandito at
      aryo@lel.hiroshima-u.ac.jp.</p>
    <p>By advancing to the next screen you are indicating that you are at least 18 years old, have read and understood
      this consent form and agree to participate in this research study.</p>
  </main>

  <div class="mb-5 pb-5">
  <hr>
  <button id="bt-continue" class="btn btn-lg btn-primary">Agree and Continue</button>
  <button class="ml-3 btn btn-lg btn-danger" href="<?php echo $this->location('e2/signOut'); ?>">I Disagree</button>
  </div>

  <footer class="mastfoot mt-auto">
    <div class="inner text-center">
      
    </div>
  </footer>
</div>

<?php $this->view('general/general.ui.php');?>
<?php $this->view('base/footer.php');?>
<?php $this->view('base/header.php'); ?>

<div class="cover-container d-flex h-100 p-3 mx-auto flex-column">
  <header class="masthead mb-auto">
    <div class="inner">
      <!-- <h3 class="masthead-brand">Kit-Build</h3> -->
      <nav class="nav nav-masthead justify-content-center">
        <!-- <a class="btn btn-danger" href="<?php echo $this->location('home/signOut'); ?>">Sign Out</a> -->
        <!-- <a class="nav-link" href="#">Features</a>
        <a class="nav-link" href="#">Contact</a> -->
      </nav>
    </div>
  </header>

  <main role="main" class="inner cover text-left mx-auto" style="width: 42em;">
    <h1 class="h3">Thank You!</h1> 
    <h1 class="h4 cover-heading text-info">ありがとうございました！</h1>
    <hr>
    <p>Thank you for you answers. The next session will be the concept mapping session.</p>
    <p>You are asked to construct a concept map from a kit, that is concept map components (concepts and links).</p>
    <p>You have <strong class="text-danger">30 menit</strong> to construct a concept map that should <strong class="text-danger">represents your understanding</strong> according to the text. During the concept map construction, you are allowed to make notes, read the text, and accessing the Internet.</p>
    <?php 
      $user = (object) $_SESSION['user'];
      $grups = $user->grups;
      $nextPage = 'kbcollab';
    ?>
    <p>You will construct the concept map <strong class="text-danger">in pair with your partners</strong> collaboratively in real-time. Any changes you made to the concept map will be reflected to all of your partners.</p> 
    <p>Please ensure to <strong class="text-danger">use chat messaging or discuss </strong> features to discuss the concepts, links, and propositions with your partners.</p>
    <p>All conversations made during the concept mapping activity will be recorded for analysis.</p>
    <hr>
    <p>Please wait for an instruction to continue to the next step to start concept mapping.</p>
    <button id="bt-continue" class="btn btn-primary btn-lg" data-next="<?php echo $nextPage; ?>">Let's Go!</button>
  </main>

  <footer class="mastfoot mt-auto">
    <div class="inner text-center">
      &nbsp;
    </div>
  </footer>
</div>

<?php $this->view('general/general.ui.php'); ?>
<?php $this->view('base/footer.php'); ?>
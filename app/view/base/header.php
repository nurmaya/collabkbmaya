<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

<?php foreach($this->styles as $s) echo '    <link rel="stylesheet" href="'.$s.'">' . "\n"; ?>

    <script language="javascript">
      var assetsUrl = '<?php echo $this->assets(); ?>';
      var baseUrl = '<?php echo $this->location(); ?>';
      var sessId = '<?php echo session_id(); ?>';
      var uid = parseInt('<?php echo isset($_SESSION["user"]) ? ((object)$_SESSION["user"])->uid : null; ?>');
      var seq = parseInt('<?php echo isset($_SESSION["seq"]) ? $_SESSION["seq"] : 0; ?>');
    </script>

    <title>Kit-Build</title>
  </head>
  <body>

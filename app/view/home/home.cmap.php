<?php $this->view('base/header.php');?>

<div id="main-container">
  <div id="app-toolbar">

    <div class="separator"></div>

    <div class="btn-group">
      <!-- <button id="bt-login" class="btn btn-sm btn-primary" data-tippy-content="Sign In">
        <i class="fas fa-sign-in-alt"></i> Sign In
      </button> -->
      <button id="bt-logout" class="btn btn-sm btn-danger" data-tippy-content="Sign Out">
        Sign Out <span id="bt-logout-username" class="font-weight-bold"></span>
      </button>
    </div>

    <div class="btn-group">
      <!-- <button id="bt-open-kit" data-tippy-content="Open Material" class="btn btn-sm btn-outline-primary" disabled><i
          class="fas fa-folder-open"></i></button> -->
      <button id="bt-show-material" data-tippy-content="Show Material" class="btn btn-sm btn-outline-primary"
        disabled><i class="fas fa-eye"></i> <span class="d-none d-sm-inline-block">Material</span></button>
    </div>

    <div class="btn-group">
      <button id="bt-sync" class="btn btn-sm btn-outline-primary" data-tippy-content="Synchronize map" disabled>
        <i class="fas fa-sync"></i> <span class="d-none d-sm-inline-block">Sync</span>
      </button>
    </div>

    <div class="btn-group">
      <button id="bt-open-map" class="btn btn-sm btn-outline-info" data-tippy-content="Open Map">
        <i class="fas fa-folder-open"></i>
      </button>
      <button id="bt-save" class="btn btn-sm btn-outline-info" data-tippy-content="Save map" disabled>
        <i class="fas fa-save"></i> <span class="d-none d-sm-inline-block">Save</span>
      </button>
      <button id="bt-load-draft" class="btn btn-sm btn-outline-info" data-tippy-content="Load draft map" disabled>
        <i class="fas fa-upload"></i>
      </button>
    </div>
    <div class="btn-group">
      <button id="bt-finalize" class="btn btn-sm btn-danger" data-tippy-content="Finalize map" disabled>
        <i class="fas fa-save"></i> Finalize
      </button>
    </div>

  </div>

  <!-- Material Modal -->
  <div class="modal" id="modal-kit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <h5>Open Material</h4>
            <hr>
            <div class="row">
              <div class="col">
                <h6>Material</h6>
                <hr>
                <div class="material-list list-container"></div>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-primary bt-dialog bt-open">Open</button>
          <button type="button" class="btn btn-sm btn-secondary bt-dialog bt-cancel"
            data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>
  <!-- /Material Modal -->

  <!-- Map name Modal -->
  <div class="modal" id="modal-map-name" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <div class="form-row pr-2 pl-2">
            <label>Please give your map a name</label>
            <input type="text" class="form-control map-name-input">
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-primary bt-dialog bt-ok pl-5 pr-5">OK</button>
          <button type="button" class="btn btn-sm btn-secondary bt-dialog bt-cancel pl-5 pr-5"
            data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>
  <!-- /Map name Modal -->

  <!-- Open Map Modal -->
  <div class="modal" id="modal-open-map" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <h5>Open Saved Maps</h5>
          <hr>
          <div class="row">
            <div class="col">
              <div class="map-list list-container"></div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-secondary bt-dialog bt-cancel"
            data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>
  <!-- /Open Map Modal -->

  <?php $this->view('kbui/kbui.canvas.php');?>
  <?php $this->view('chat/chat.window.php');?>

  <div id="popup-material" style="display: none; border: 2px solid #ccc; border-radius: 0.5rem;">
    <div class="row justify-content-between pl-3 pr-3">
      <span class="material-title h4"></span>
      <span class="text-secondary align-items-center">
        <input id="popup-material-check-open" type="checkbox" class="check-open mr-2">
        <label for="popup-material-check-open"><small><i class="fas fa-eye-slash"></i> Keep open </small></label>
        <span class="bt-close badge badge-danger ml-3">
          <i class="fas fa-times"></i> Close
        </span>
      </span>
    </div>
    <hr>
    <div class="material-content"></div>
    <button class="btn btn-sm btn-danger mt-3 bt-top"><i class="fas fa-chevron-up"></i> Top</button>
    <button class="btn btn-sm btn-primary mt-3 bt-more"><i class="fas fa-chevron-down"></i> More</button>
  </div>

  <div id="popup-map" class="popup p-3" style="display: none; min-width: 500px; background: #fff">
    <div class="row justify-content-between pl-3 pr-3">
      <span class="material-title">Concept Map</span>
      <button class="bt-cancel btn btn-danger btn-sm"><i class="fas fa-times"></i> Cancel Finalize</button>
    </div>
    <hr>
    <div class="text-center">
      <img class="cmap" style="margin: inherit auto; max-width: 800px; max-height:400px;">
    </div>
    <hr>
    <div class="row justify-content-between align-items-center p-3">
      <button class="bt-download btn btn-secondary mt-3"><i class="fas fa-save"></i> Download as image</button>
      <button class="bt-continue btn btn-primary mt-3 ml-3">OK, Continue Finalize &nbsp; <i
          class="fas fa-chevron-right"></i></button>
    </div>
  </div>

  <div id="popup-selection" class="" style="display: none;">
    <div class="menu-item" data-type="concept">Create new concept: <strong class="selected-text"></strong></div>
    <div class="menu-item" data-type="link">Create new link: <strong class="selected-text"></strong></div>
  </div>

</div> <!-- /Main Container -->

<?php $this->view('general/general.ui.php');?>
<?php $this->view('base/footer.php');?>
<?php $this->view('base/header.php'); ?>

<div id="main-container">
  <div id="app-toolbar">

    <div class="separator"></div>

    <div class="btn-group">
      <button id="bt-logout" class="btn btn-sm btn-danger" data-tippy-content="Sign Out" style="display: none">
        Sign Out <span id="bt-logout-username" class="font-weight-bold"></span>
      </button>
    </div>

    <div class="btn-group">
      <!-- <button id="bt-open-kit" class="btn btn-sm btn-outline-primary" disabled><i class="fas fa-folder-open"></i></button> -->
      <button id="bt-show-material" class="btn btn-sm btn-outline-primary" disabled><i class="fas fa-eye"></i>
        Material</button>
    </div>

    <!-- <div class="btn-group">
      <button id="bt-sync" class="btn btn-sm btn-outline-primary" data-tippy-content="Synchronize map" disabled>
        <i class="fas fa-sync"></i> Sync
      </button>
    </div> -->

    <!-- <div class="btn-group">
      <button id="bt-save" class="btn btn-sm btn-outline-info" data-tippy-content="Save map" disabled>
        <i class="fas fa-save"></i> Save
      </button>
      <button id="bt-load-draft" class="btn btn-sm btn-outline-info" data-tippy-content="Load draft map" disabled>
        <i class="fas fa-upload"></i>
      </button>
    </div>
    <div class="btn-group">
      <button id="bt-finalize" class="btn btn-sm btn-danger" data-tippy-content="Finalize map" disabled>
        <i class="fas fa-save"></i> Finalize
      </button>
    </div> -->

  </div>

  <!-- Material Modal -->
  <div class="modal" id="modal-kit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-body">
          <h5>Open Kit</h4>
            <hr>
            <div class="row">
              <div class="col">
                <h6>Material</h6>
                <hr>
                <div class="material-list list-container"></div>
              </div>
              <div class="col">
                <h6>Kit</h6>
                <hr>
                <div class="goalmap-list list-container"></div>
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-primary bt-dialog bt-open">Open</button>
          <button type="button" class="btn btn-sm btn-secondary bt-dialog bt-cancel"
            data-dismiss="modal">Cancel</button>
        </div>
      </div>
    </div>
  </div>
  <!-- /Material Modal -->

  <?php $this->view('kbui/kbui.canvas.php'); ?>
  <?php // $this->view('chat/chat.window.php'); ?>

  <div id="popup-material" class="popup" style="display: none">
    <div class="row justify-content-between pl-3 pr-3">
      <span class="material-title h4"></span>
      <span class="text-secondary align-items-center">
        <input id="popup-material-check-open" type="checkbox" class="check-open mr-2">
        <label for="popup-material-check-open"><small><i class="fas fa-eye-slash"></i> Keep open </small></label>
        <span class="bt-close badge badge-danger ml-3">
          <i class="fas fa-times"></i> Close
        </span>
      </span>
    </div>
    <hr>
    <div class="material-content"></div>
    <button class="btn btn-sm btn-primary mt-3"><i class="fas fa-chevron-down"></i> More</button>
  </div>

</div> <!-- /Main Container -->

<?php $this->view('general/general.ui.php'); ?>
<?php $this->view('base/footer.php'); ?>
<script>

</script>
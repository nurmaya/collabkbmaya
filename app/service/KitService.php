<?php

class KitService extends CoreService {

  // public function getMaterials() {
  //   $db = $this->getInstance('kb');
  //   $sql = "SELECT mid, date, uid, name, source_type, "
  //     . "(SELECT COUNT(gm.gmid) FROM goalmaps gm WHERE m.mid = gm.mid) AS goalmaps "
  //     . "FROM materials m ORDER BY mid DESC";
  //   return $db->query($sql);
  // }

  // public function getMaterialsWithMeta($keywords = null) {
  //   $db = $this->getInstance('kb');
  //   $keywords = $db->escape($keywords);
  //   $sql = "SELECT m.mid AS mid, date, uid, name, source_type, state, "
  //     . "(SELECT COUNT(gm.gmid) FROM goalmaps gm WHERE m.mid = gm.mid) AS goalmaps, "
  //     . "LENGTH(mm.content) AS clength, LENGTH(mm.nlp) AS nlength "
  //     . "FROM materials m "
  //     . "LEFT JOIN materials_meta mm ON m.mid = mm.mid ";
  //   if ($keywords != null) {
  //     $sql .= " AND m.name LIKE '%$keywords% ";
  //   }

  //   $sql .= "ORDER BY m.mid DESC ";
  //   return $db->query($sql);
  // }

  // public function getMaterialsWithMetaByUid() {
  //   $db = $this->getInstance('kb');
  //   $sql = "SELECT m.mid AS mid, date, uid, name, source_type, "
  //     . "(SELECT COUNT(gm.gmid) FROM goalmaps gm WHERE m.mid = gm.mid) AS goalmaps, "
  //     . "LENGTH(mm.content) AS clength, LENGTH(mm.nlp) AS nlength "
  //     . "FROM materials m "
  //     . "LEFT JOIN materials_meta mm ON m.mid = mm.mid "
  //     . "WHERE mm.state = 1 "
  //     . "ORDER BY m.mid DESC";
  //   return $db->query($sql);
  // }

  // public function getMaterialById($mid, $includeMeta = false) {
  //   $db = $this->getInstance('kb');
  //   $sql = "SELECT m.mid AS mid, date, uid, name, source_type, "
  //   . "(SELECT COUNT(gm.gmid) FROM goalmaps gm WHERE m.mid = gm.mid) AS goalmaps, "
  //   . "mm.content, mm.nlp, mm.canvas "
  //   . "FROM materials m LEFT JOIN materials_meta mm ON m.mid = mm.mid "
  //   . "WHERE m.mid = '" . QB::esc($mid) . "' ORDER BY m.mid DESC";
  //   // echo $sql;
  //   return $db->query($sql);
  // }

  // public function insertMaterial($name, $uid = 1, $source_type = 0) {

  //   $material = new Material();
  //   $material->name = $name;
  //   $material->uid = $uid;
  //   $material->source_type = $source_type;

  //   // var_dump($material);

  //   $db = $this->getInstance('kb');
  //   $cols = array('name', 'uid', 'source_type');
  //   $insertId =
  //   QB::instance($db)->table('materials')->insertModel($material, $cols)->execute()->getInsertId();
  //   $material->mid = $insertId;
  //   return $material;
  // }

  // public function insertMaterialMeta($material, $content, $nlp = null, $canvas = null) {
  //   $materialMeta = new MaterialMeta($material);
  //   $materialMeta->content = QB::esc($content);
  //   $materialMeta->nlp = QB::esc($nlp);
  //   $materialMeta->canvas = QB::esc($canvas);

  //   $db = $this->getInstance('kb');
  //   $cols = array('mid', 'content');
  //   if (empty($nlp) != false) {
  //     $cols[] = 'nlp';
  //   }

  //   if (empty($canvas) != false) {
  //     $cols[] = 'canvas';
  //   }

  //   $insertId =
  //   QB::instance($db)->table('materials_meta')->insertModel($materialMeta, $cols)->execute()->getInsertId();
  //   $materialMeta->mid = $insertId;
  //   return $materialMeta;
  // }

  // public function deleteMaterialWithMeta($mid) {
  //   $db = $this->getInstance('kb');
  //   $qb = QB::instance($db);
  //   try {
  //     $db->begin();
  //     $sql = "DELETE FROM materials_meta WHERE mid = '$mid'";
  //     $db->query(
  //       $qb->table('materials_meta')->delete()
  //         ->where('mid', $mid)->get()
  //     );
  //     $qb->clear();
  //     $db->query(
  //       $qb->table('materials')->delete()
  //         ->where('mid', $mid)
  //         ->get()
  //     );
  //     $db->commit();
  //   } catch (Exception $e) {
  //     $db->rollback();
  //     throw $e;
  //   }
  // }

  // public function deleteMaterialMeta($mid, $what = null) {
  //   try {
  //     $db = $this->getInstance('kb');
  //     $qb = QB::instance($db);

  //     $update = [];

  //     if ($what == 'nlp') {
  //       $update['nlp'] = QBRaw::raw('NULL');
  //     } else if ($what == 'content') {
  //       $update['content'] = QBRaw::raw('NULL');
  //     }
  //     if ($what == null) {
  //       $result = $qb->table('materials_meta')->delete()->where('mid', $mid)->execute();
  //     } else {
  //       $result = $qb->table('materials_meta')->update($update)->where('mid', $mid)->execute();
  //       return $result;
  //     }
  //   } catch (Exception $e) {
  //     throw $e;
  //   }
  // }

  // public function setMaterialState($mid, $state) {
  //   try {
  //     $db = $this->getInstance('kb');
  //     $qb = QB::instance($db);
  //     $update = [];
  //     $update['state'] = $db->escape($state);
  //     $result = $qb->table('materials_meta')->update($update)->where('mid', $mid)->execute();
  //     return $result;
  //   } catch (Exception $e) {
  //     throw $e;
  //   }
  // }

  // public function getGoalmapsByMapId($mid) {

  //   $db = $this->getInstance('kb');
  //   $goalmaps = QB::instance($db)->table('goalmaps g')->selectRaw('g.date, g.gmid, g.name, g.uid, g.mid, g.modify_uid, g.modify_date,
  //   (SELECT COUNT(*) FROM goalmaps_concepts c WHERE c.gmid = g.gmid AND c.mid = g.mid) AS num_concepts, (SELECT COUNT(*) FROM goalmaps_links l WHERE l.gmid = g.gmid AND l.mid = g.mid) AS num_links')->where('g.mid', $mid)->executeQuery();
  //   // var_dump($goalmaps);
  //   // QB::instance($db)->table('goalmaps')->select()
  //   // ->where('mid', $mid)
  //   // ->map(new Goalmap());
  //   return $goalmaps;
  // }
  
  // public function getGoalmapsWithLearnermapsInfoByMapId($mid = null)
  // {
  //   $db = $this->getInstance('kb');
  //   $goalmaps = QB::instance($db)->table('goalmaps g')->selectRaw('g.date, g.gmid, g.name, g.uid, g.mid, g.modify_uid, g.modify_date,
  //   (SELECT COUNT(*) FROM goalmaps_concepts c WHERE c.gmid = g.gmid AND c.mid = g.mid) AS num_concepts, 
  //   (SELECT COUNT(*) FROM goalmaps_links l WHERE l.gmid = g.gmid AND l.mid = g.mid) AS num_links,
  //   (SELECT COUNT(*) FROM learnermaps lm WHERE lm.gmid = g.gmid AND lm.finalmap = 1) AS num_learnermaps_final,
  //   (SELECT COUNT(*) FROM learnermaps lm WHERE lm.gmid = g.gmid AND lm.finalmap = 0) AS num_learnermaps_temp'
  //   )
  //   ->where('g.mid', $mid)->executeQuery();
  //   // var_dump($goalmaps);
  //   // QB::instance($db)->table('goalmaps')->select()
  //   // ->where('mid', $mid)
  //   // ->map(new Goalmap());
  //   return $goalmaps;
  // }

  // public function getFinalGoalmapsByMapId($mid) {

  //   $db = $this->getInstance('kb');
  //   $goalmaps = QB::instance($db)
  //     ->table('goalmaps g')
  //     ->selectRaw('g.date, g.gmid, g.name, g.uid, g.mid, g.modify_uid, g.modify_date,
  //   (SELECT COUNT(*) FROM goalmaps_concepts c WHERE c.gmid = g.gmid AND c.mid = g.mid) AS num_concepts, (SELECT COUNT(*) FROM goalmaps_links l WHERE l.gmid = g.gmid AND l.mid = g.mid) AS num_links')
  //   ->where('g.mid', $mid)
  //   ->whereRaw("g.name NOT LIKE 'temp-%'")
  //   ->executeQuery();
  //   // var_dump($goalmaps);
  //   // QB::instance($db)->table('goalmaps')->select()
  //   // ->where('mid', $mid)
  //   // ->map(new Goalmap());
  //   return $goalmaps;
  // }

  // public function getGoalmapsByUid($uid) {
  //   $db = $this->getInstance('kb');
  //   $goalmaps = QB::instance($db)->table('goalmaps g')->selectRaw('g.date, g.gmid, g.name, g.uid, g.mid, g.modify_uid, g.modify_date,
  //   (SELECT COUNT(*) FROM goalmaps_concepts c WHERE c.gmid = g.gmid AND c.mid = g.mid) AS num_concepts, (SELECT COUNT(*) FROM goalmaps_links l WHERE l.gmid = g.gmid AND l.mid = g.mid) AS num_links')->where('g.uid', $uid)->executeQuery();
  //   // var_dump($goalmaps);
  //   // QB::instance($db)->table('goalmaps')->select()
  //   // ->where('mid', $mid)
  //   // ->map(new Goalmap());
  //   return $goalmaps;
  // }

  // public function getUserGoalmaps($mid, $uid) {
  //   $db = $this->getInstance('kb');
  //   $goalmaps = QB::instance($db)->table('goalmaps g')
  //     ->selectRaw('g.date, g.gmid, g.name, g.uid, g.mid, g.modify_uid, g.modify_date,
  //   (SELECT COUNT(*) FROM goalmaps_concepts c WHERE c.gmid = g.gmid AND c.mid = g.mid) AS num_concepts, (SELECT COUNT(*) FROM goalmaps_links l WHERE l.gmid = g.gmid AND l.mid = g.mid) AS num_links')
  //     ->where('g.uid', $uid)
  //     ->where('g.mid', $mid)
  //     ->executeQuery();
  //   return $goalmaps;
  // }

  // public function getUserGoalmap($gmid, $mid) {
  //   return $this->getGoalmapById($gmid);
  // }

  // public function getGoalmapEsb($gmid) {

  //   $select = "state, screenshot, snapshot";

  //   $db = $this->getInstance('kb');
  //   $esbs = QB::instance($db)
  //     ->table('goalmaps_esb g')
  //     ->selectRaw($select)
  //     ->where('g.gmid', $gmid)
  //     ->executeQuery();
  //   return $esbs;
  // }

  // public function getUserLastGoalMap($uid, $mid, $map = null) {
  //   $db = $this->getInstance('kb');
  //   $mid = $db->escape($mid);
  //   $uid = $db->escape($uid);

  //   if ($map) {
  //     $map = $db->escape($map);
  //   }

  //   $msql = "SELECT mid, date, uid, name, source_type "
  //     . "FROM materials WHERE mid = $mid";

  //   $gsql = "SELECT date, gm.gmid AS gmid, name, uid, mid, modify_uid, "
  //     . "modify_date, ge.state AS state "
  //     . "FROM goalmaps gm "
  //     . "LEFT JOIN goalmaps_esb ge ON gm.gmid = ge.gmid "
  //     . "WHERE ge.state = 'construct' AND mid = $mid AND gm.gmid = (
  //       SELECT gmid FROM goalmaps WHERE uid = $uid AND mid = $mid AND name LIKE '$map%' ORDER BY date DESC LIMIT 1
  //     )";

  //   $result = new stdClass;
  //   $result->material = $db->getRow($msql);

  //   try {
  //     if (!$map) {
  //       throw new Exception('Unspecified map type');
  //     }

  //     $result->goalmap = $db->getRow($gsql);
  //     if (!$result->goalmap) {
  //       throw new Exception('Empty goal map');
  //     }

  //     $gmid = $result->goalmap->gmid;
  //     $csql = "SELECT cid, mid, gmid, label, locx, locy "
  //       . "FROM goalmaps_concepts "
  //       . "WHERE mid = $mid AND gmid = $gmid";

  //     $lsql = "SELECT lid, mid, gmid, label, locx, locy, source, target "
  //       . "FROM goalmaps_links "
  //       . "WHERE mid = $mid AND gmid = $gmid";

  //     $result->concepts = $db->query($csql);
  //     $result->links = $db->query($lsql);
  //   } catch (Exception $e) {
  //     $result->goalmap = null;
  //     $result->concepts = null;
  //     $result->links = null;
  //   }

  //   return $result;
  // }

  // public function getUserLastTemporaryGoalmap($uid, $mid) {

  //   $db = $this->getInstance('kb');
  //   $mid = $db->escape($mid);
  //   $uid = $db->escape($uid);

  //   $gsql = "SELECT `date`, gm.gmid AS gmid, name, uid, mid, modify_uid, "
  //     . "modify_date "
  //     . "FROM goalmaps gm "
  //     . "WHERE mid = $mid AND gm.gmid = (
  //         SELECT gmi.gmid FROM goalmaps gmi
  //         WHERE gmi.mid = '$mid' AND gmi.uid = '$uid' AND gmi.name LIKE CONCAT('temp-','$uid','%')
  //         ORDER BY gmi.gmid DESC LIMIT 1
  //       )";

  //   $result = new stdClass;
  //   try {
  //     $result->goalmap = $db->getRow($gsql);
  //     if (!$result->goalmap) {
  //       throw new Exception('Empty goal map');
  //     }
  //     $gmid = $result->goalmap->gmid;
  //     $csql = "SELECT cid, mid, gmid, label, locx, locy "
  //       . "FROM goalmaps_concepts "
  //       . "WHERE mid = $mid AND gmid = $gmid";
  //     $lsql = "SELECT lid, mid, gmid, label, locx, locy, source, target "
  //       . "FROM goalmaps_links "
  //       . "WHERE mid = $mid AND gmid = $gmid";
  //     $result->concepts = $db->query($csql);
  //     $result->links = $db->query($lsql);
  //   } catch (Exception $e) {
  //     $result->goalmap = null;
  //     $result->concepts = null;
  //     $result->links = null;
  //   }
  //   return $result;
  // }

  // public function getLearnerLastTemporaryMap($gmid, $uid) {

  //   $db = $this->getInstance('kb');
  //   $gmid = $db->escape($gmid);
  //   $uid = $db->escape($uid);

  //   $gsql = "SELECT lmi.lmid, lmi.date, gm.name AS gmname, m.name AS `name` 
  //             FROM learnermaps lmi 
  //               LEFT JOIN goalmaps gm ON gm.gmid = lmi.gmid 
  //               LEFT JOIN materials m ON gm.mid = m.mid 
  //             WHERE lmi.gmid = '$gmid' AND lmi.uid = '$uid' AND finalmap = 0 
  //             ORDER BY lmid DESC LIMIT 1"; // echo $gsql;
  //   $result = new stdClass;

  //   try {
  //     $result->learnermap = $db->getRow($gsql); // var_dump($result);
  //     if (!$result->learnermap) {
  //       throw new Exception('Empty learnermap');
  //     }
  //     $lmid = $result->learnermap->lmid;
  //     $csql = "SELECT gc.cid, gc.mid, gc.gmid, gc.label, lc.locx, lc.locy 
  //               FROM learnermaps_concepts lc
  //               LEFT JOIN goalmaps_concepts gc ON gc.gmid = lc.gmid AND gc.cid = lc.cid
  //                 WHERE lc.lmid = '$lmid'";
  //     $lsql = "SELECT gl.lid, gl.mid, gl.gmid, gl.label, ll.locx, ll.locy, ll.source, ll.target 
  //               FROM learnermaps_links ll
  //               LEFT JOIN goalmaps_links gl ON gl.gmid = ll.gmid AND gl.lid = ll.lid
  //                 WHERE ll.lmid = '$lmid';";
  //                 // var_dump($csql);
  //                 // var_dump($lsql);
  //     $result->concepts = $db->query($csql);
  //     $result->links = $db->query($lsql);
  //   } catch (Exception $e) {
  //     $result->error = $e;
  //     $result->goalmap = null;
  //     $result->concepts = null;
  //     $result->links = null;
  //   }
  //   return $result;
  // }

  // public function deleteAllTemporaryGoalmap($uid, $mid) {
  //   $db = $this->getInstance('kb');
  //   $mid = $db->escape($mid);
  //   $uid = $db->escape($uid);
  //   $sql = "DELETE FROM goalmaps gmi
  //             WHERE gmi.mid = '$mid' AND gmi.uid = '$uid' AND gmi.name LIKE CONCAT('temp-','$uid','%')";
  //   $result = $db->query($sql);
  //   // var_dump($result);
  //   return $result;
  // }

  // public function getLearnermaps($gmid) {
  //   $db = $this->getInstance('kb');

  //   $sql = "SELECT lm.lmid, u.fullname, u.username, lm.finalmap FROM learnermaps lm 
  //     LEFT JOIN users u ON u.uid = lm.uid
  //     WHERE lm.gmid = '$gmid'";
  //   // echo $sql;
  //   $result = $db->query($sql);
  //   // var_dump($result);
  //   return $result;
  // }

  // public function getSavedLearnermaps($gmid, $uid) {
  //   $db = $this->getInstance('kb');

  //   $sql = "SELECT lm.lmid, lm.gmid, lm.uid, u.fullname, u.username, lm.finalmap FROM learnermaps lm 
  //     LEFT JOIN users u ON u.uid = lm.uid
  //     WHERE lm.gmid = '$gmid' AND lm.uid = '$uid'";
  //   // echo $sql;
  //   $result = $db->query($sql);
  //   // var_dump($result);
  //   return $result;
  // }

  // public function getLearnermap($lmid) {
  //   $db = $this->getInstance('kb');
  //   $gmid = $db->escape($gmid);
  //   $uid = $db->escape($uid);

  //   $gsql = "SELECT lmi.lmid, lmi.date, gm.name AS gmname, m.name AS `name` 
  //             FROM learnermaps lmi 
  //               LEFT JOIN goalmaps gm ON gm.gmid = lmi.gmid 
  //               LEFT JOIN materials m ON gm.mid = m.mid 
  //             WHERE lmi.lmid = '$lmid' "; // echo $gsql;
  //   $result = new stdClass;

  //   try {
  //     $result->learnermap = $db->getRow($gsql); // var_dump($result);
  //     if (!$result->learnermap) {
  //       throw new Exception('Empty learnermap');
  //     }
  //     $lmid = $result->learnermap->lmid;
  //     $csql = "SELECT gc.cid, gc.mid, gc.gmid, gc.label, lc.locx, lc.locy 
  //               FROM learnermaps_concepts lc
  //               LEFT JOIN goalmaps_concepts gc ON gc.gmid = lc.gmid AND gc.cid = lc.cid
  //                 WHERE lc.lmid = '$lmid'";
  //     $lsql = "SELECT gl.lid, gl.mid, gl.gmid, gl.label, ll.locx, ll.locy, ll.source, ll.target 
  //               FROM learnermaps_links ll
  //               LEFT JOIN goalmaps_links gl ON gl.gmid = ll.gmid AND gl.lid = ll.lid
  //                 WHERE ll.lmid = '$lmid';";
  //                 // var_dump($csql);
  //                 // var_dump($lsql);
  //     $result->concepts = $db->query($csql);
  //     $result->links = $db->query($lsql);
  //   } catch (Exception $e) {
  //     $result->error = $e;
  //     $result->goalmap = null;
  //     $result->concepts = null;
  //     $result->links = null;
  //   }
  //   return $result;
  // }

  // public function getFinalMaps($uid, $mid, $map = null) {
  //   $db = $this->getInstance('kb');

  //   $sql = $map ? "SELECT '$map' AS type, g.gmid, g.date, g.name, ge.screenshot, ge.snapshot, ge.state FROM goalmaps g LEFT JOIN goalmaps_esb ge ON g.gmid = ge.gmid WHERE g.uid = '$uid' AND g.mid = '$mid' AND ge.state = 'final' AND g.name LIKE '$map%' ORDER BY g.gmid DESC LIMIT 1" : "(SELECT 'esb' AS type, g.gmid, g.date, g.name, ge.screenshot, ge.snapshot, ge.state
  //   FROM goalmaps g
  //   LEFT JOIN goalmaps_esb ge ON g.gmid = ge.gmid
  //   WHERE g.uid = '$uid' AND g.mid = '$mid' AND ge.state = 'final' AND g.name LIKE 'esb%'
  //   ORDER BY g.gmid DESC LIMIT 1)
  //   UNION
  //   (SELECT 'eckb' AS type, g.gmid, g.date, g.name, ge.screenshot, ge.snapshot, ge.state
  //   FROM goalmaps g
  //   LEFT JOIN goalmaps_esb ge ON g.gmid = ge.gmid
  //   WHERE g.uid = '$uid' AND g.mid = '$mid' AND ge.state = 'final' AND g.name LIKE 'eckb%'
  //   ORDER BY g.gmid DESC LIMIT 1)";
  //   // echo $sql;
  //   $result = $db->query($sql);
  //   // var_dump($result);
  //   return $result;
  // }

  // public function getKits() {
  //   $db = $this->getInstance('kb');

  //   $sql = "SELECT date, gmid, name, uid, mid, modify_uid, modify_date, "
  //     . "(SELECT COUNT(c.gmid) FROM goalmaps_concepts c WHERE c.mid = g.mid AND g.gmid = c.gmid) AS concepts, "
  //     . "(SELECT COUNT(l.gmid) FROM goalmaps_links l WHERE l.mid = g.mid AND g.gmid = l.gmid) AS links "
  //     . "FROM goalmaps g";

  //   return $db->query($sql);
  // }

  // public function getMaterialAndMapById($mid, $gmid) {
  //   $db = $this->getInstance('kb');
  //   $mid = $db->escape($mid);
  //   $gmid = $db->escape($gmid);

  //   $msql = "SELECT mid, date, uid, name, source_type "
  //     . "FROM materials WHERE mid = $mid";

  //   $gsql = "SELECT date, gmid, name, uid, mid, modify_uid, "
  //     . "modify_date "
  //     . "FROM goalmaps WHERE mid = $mid AND gmid = $gmid";

  //   $csql = "SELECT cid, mid, gmid, label, locx, locy "
  //     . "FROM goalmaps_concepts "
  //     . "WHERE mid = $mid AND gmid = $gmid";

  //   $lsql = "SELECT lid, mid, gmid, label, locx, locy, source, target "
  //     . "FROM goalmaps_links "
  //     . "WHERE mid = $mid AND gmid = $gmid";

  //   $result = new stdClass;
  //   $result->material = $db->getRow($msql);
  //   $result->goalmap = $db->getRow($gsql);
  //   $result->concepts = $db->query($csql);
  //   $result->links = $db->query($lsql);
  //   return $result;
  // }

  // public function getGoalmapById($gmid) {
  //   $db = $this->getInstance('kb');
  //   $gmid = $db->escape($gmid);

  //   $gsql = "SELECT date, gmid, name, uid, mid, modify_uid, "
  //     . "modify_date "
  //     . "FROM goalmaps WHERE gmid = $gmid";

  //   $csql = "SELECT cid, mid, gmid, label, locx, locy "
  //     . "FROM goalmaps_concepts "
  //     . "WHERE gmid = $gmid";

  //   $lsql = "SELECT lid, mid, gmid, label, locx, locy, source, target "
  //     . "FROM goalmaps_links "
  //     . "WHERE gmid = $gmid";

  //   $result = new stdClass;
  //   $result->goalmap = $db->getRow($gsql);
  //   $result->concepts = $db->query($csql);
  //   $result->links = $db->query($lsql);
  //   return $result;
  // }

  // public function getGoalmapModById($gmid) {
  //   $db = $this->getInstance('kb');
  //   $gmid = $db->escape($gmid);

  //   $gsql = "SELECT date, gmid, name, uid, mid, modify_uid, "
  //     . "modify_date "
  //     . "FROM goalmaps WHERE gmid = $gmid";

  //   $csql = "SELECT cid, mid, gmid, label, locx, locy "
  //     . "FROM goalmaps_concepts_mod "
  //     . "WHERE gmid = $gmid";

  //   $lsql = "SELECT lid, mid, gmid, label, locx, locy, source, target "
  //     . "FROM goalmaps_links_mod "
  //     . "WHERE gmid = $gmid";

  //   $result = new stdClass;
  //   $result->goalmap = $db->getRow($gsql);
  //   $result->concepts = $db->query($csql);
  //   $result->links = $db->query($lsql);
  //   return $result;
  // }

  // public function getGoalmapExcelData($uid, $mid, $sesi) {
  //   $db = $this->getInstance('kb');
  //   $uid = $db->escape($uid);
  //   $mid = $db->escape($mid);
  //   $sesi = $db->escape($sesi);

  //   $data = QB::instance($db)->table('goalmaps_data gd')
  //     ->select(array('did', 'dnc', 'dnl', 'dnp', 'tools'))
  //     ->where('uid', $uid)
  //     ->where('mid', $mid)
  //     ->where('sesi', $sesi)
  //     ->executeQuery();
  //   if (is_array($data) and count($data)) {
  //     return $data[0];
  //   }

  //   // var_dump($score);
  //   return $data;
  // }

  public function getKitById($gmid) {
    $db = $this->getInstance('workshop');
    $gmid = $db->escape($gmid);

    $gsql = "SELECT date, gmid, name, uid, mid, modify_uid, "
      . "modify_date "
      . "FROM goalmaps WHERE gmid = $gmid";
    $csql = "SELECT cid, mid, gmid, label "
      . "FROM goalmaps_concepts "
      . "WHERE gmid = $gmid";
    $lsql = "SELECT lid, mid, gmid, label "
      . "FROM goalmaps_links "
      . "WHERE gmid = $gmid";

    $result = new stdClass;
    $result->goalmap = $db->getRow($gsql);
    $result->concepts = $db->query($csql);
    $result->links = $db->query($lsql);

    $result->elements = [];

    if($result->concepts) 
    foreach($result->concepts as $c) {
      $e = new stdClass;
      $e->group = 'nodes';
      $e->data = new stdClass;
      $e->data->id = "c-" . $c->cid;
      $e->data->name = $c->label;
      $e->data->type = 'concept';
      $e->position = new stdClass;
      $e->position->x = 0;
      $e->position->y = 0;
      // var_dump($c);
      $result->elements[] = $e;
    }

    if($result->links) 
    foreach($result->links as $l) {
      $e = new stdClass;
      $e->group = 'nodes';
      $e->data = new stdClass;
      $e->data->id = "l-" . $l->lid;
      $e->data->name = $l->label;
      $e->data->type = 'link';
      $e->position = new stdClass;
      $e->position->x = 0;
      $e->position->y = 0;
      $result->elements[] = $e;
    }

    return $result;
  }

  /*

  public function saveGoalMap($mid, $gmid, $concepts, $links, $uid) {

    $db = $this->getInstance('kb');
    $mid = $db->escape($mid);
    $gmid = $db->escape($gmid);

    $dcsql = "DELETE FROM goalmaps_concepts "
      . "WHERE mid = $mid AND gmid = $gmid";

    $dlsql = "DELETE FROM goalmaps_links "
      . "WHERE mid = $mid AND gmid = $gmid";

    $csql = "INSERT INTO goalmaps_concepts () VALUES ";
    $lsql = "INSERT INTO goalmaps_links () VALUES ";

    $qb = QB::instance($db, 'goalmaps_concepts');

    $where['mid'] = $mid;
    $where['gmid'] = $gmid;

    // throw new CoreError($mid . ":". $gmid . ":");

    try {
      $db->begin();
      $db->query(
        $qb->table('goalmaps_links')->delete()
          ->where('mid', $mid)->where('gmid', $gmid)
          ->get()
      );
      $qb->clear();
      $db->query(
        $qb->table('goalmaps_concepts')->delete()
          ->where('mid', $mid)->where('gmid', $gmid)
          ->get()
      );
      if ($concepts) {
        $qb->clear();
        $db->query(
          $qb->insertModel($concepts)->table('goalmaps_concepts')
            ->get()
        );
      }
      if ($links) {
        $qb->clear();
        $db->query(
          $qb->insertModel($links)->table('goalmaps_links')
            ->get()
        );
      }
      $qb->clear();

      $update['modify_uid'] = $uid;

      $db->query(
        $qb->table('goalmaps')->update($update)->update(QBRaw::raw('modify_date = NOW()'))->where('gmid', $gmid)->get()
      );
      // throw new CoreError($qb->insertModel($links)->table('goalmaps_links')
      // ->get());
      $db->commit();
      return true;
    } catch (Exception $ex) {
      $lastSQL = $db->getLastQuery();
      $db->rollback();
      throw new CoreError($ex->getMessage() . " SQL: " . $lastSQL);
    }
  }

  public function saveGoalMapAs($mid, $gmname, $concepts, $links, $uid) {

    $db = $this->getInstance('kb');
    $mid = $db->escape($mid);

    $gm = new Goalmap();
    $gm->name = $gmname;
    $gm->mid = $mid;
    $gm->uid = $uid;
    $gm->modify_uid = $uid;
    $gm->modify_date = date('Y-m-d H:i:s');

    $qb = QB::instance($db, 'goalmaps');

    // throw new CoreError($mid . ":". $gmid . ":");

    try {
      $db->begin();

      $db->query(
        $qb->table('goalmaps')
          ->insertModel($gm, array('name', 'mid', 'uid', 'modify_uid', 'modify_date'))
          ->get()
      );

      $gmid = $db->getInsertId();

      foreach ($concepts as $c) {
        $c->gmid = $gmid;
      }

      foreach ($links as $l) {
        $l->gmid = $gmid;
      }

      // var_dump($concepts);exit;
      // $qb->clear();
      // $db->query(
      //   $qb->table('goalmaps_links')->delete()
      //   ->where('mid', $mid)->where('gmid', $gmid)
      //   ->get()
      // );
      // $qb->clear();
      // $db->query(
      //   $qb->table('goalmaps_concepts')->delete()
      //   ->where('mid', $mid)->where('gmid', $gmid)
      //   ->get()
      // );
      if (count($concepts)) {
        $qb->clear();
        $db->query(
          $qb->insertModel($concepts)->table('goalmaps_concepts')
            ->get()
        );
      }
      if (count($links)) {
        $qb->clear();
        $db->query(
          $qb->insertModel($links)->table('goalmaps_links')
            ->get()
        );
      }

      if(strpos($gmname, 'temp-') === false) {
        $dsql = "DELETE FROM goalmaps WHERE `name` LIKE 'temp-%' AND mid = '$mid' AND uid = '$uid'";
        $db->query($dsql);
      }

      // throw new CoreError($qb->insertModel($links)->table('goalmaps_links')
      // ->get());
      $db->commit();
      return $gmid;
    } catch (Exception $ex) {
      $lastSQL = $db->getLastQuery();
      $db->rollback();
      throw new CoreError($ex->getMessage() . " SQL: " . $lastSQL);
    }
  }

  public function deleteGoalmap($gmid = null) {
    $db = $this->getInstance('kb');
    $qb = QB::instance($db);
    try {
      $db->query(
        $qb->table('goalmaps')
          ->delete()
          ->where('gmid', $gmid)
          ->get()
      );
    } catch (Exception $e) {
      throw $e;
    }
  }

  public function updateKitMod($gmid, $did, $concepts, $links, $nc, $nl, $np) {

    $db = $this->getInstance('kb');
    $did = $db->escape($did);
    $gmid = $db->escape($gmid);

    // $dcsql = "DELETE FROM goalmaps_concepts_mod "
    //   . "WHERE gmid = $gmid";
    // $dlsql = "DELETE FROM goalmaps_links_mod "
    //   . "WHERE gmid = $gmid";
    // $csql = "INSERT INTO goalmaps_concepts_mod () VALUES ";
    // $lsql = "INSERT INTO goalmaps_links_mod () VALUES ";

    $qb = QB::instance($db, 'goalmaps_concepts_mod');

    $where['gmid'] = $gmid;

    // throw new CoreError($mid . ":". $gmid . ":");

    try {
      $db->begin();
      $db->query(
        $qb->table('goalmaps_links_mod')->delete()
          ->where('gmid', $gmid)
          ->get()
      );
      $qb->clear();
      $db->query(
        $qb->table('goalmaps_concepts_mod')->delete()
          ->where('gmid', $gmid)
          ->get()
      );
      if ($concepts) {
        $qb->clear();
        $db->query(
          $qb->insertModel($concepts)->table('goalmaps_concepts_mod')
            ->get()
        );
      }
      if ($links) {
        $qb->clear();
        $db->query(
          $qb->insertModel($links)->table('goalmaps_links_mod')
            ->get()
        );
      }

      $update['gmid'] = $gmid;
      $update['mnc'] = $nc;
      $update['mnl'] = $nl;
      $update['mnp'] = $np;

      $qb->clear();
      $db->query(
        $qb->table('goalmaps_data')->update($update)->where('did', $did)->get()
      );

      // $qb->clear();
      // $update['modify_uid'] = $uid;
      // $db->query(
      //   $qb->table('goalmaps')->update($update)->update(QBRaw::raw('modify_date = NOW()'))->where('gmid', $gmid)->get()
      // );

      // throw new CoreError($qb->insertModel($links)->table('goalmaps_links')
      // ->get());
      $db->commit();
      return true;
    } catch (Exception $ex) {
      $lastSQL = $db->getLastQuery();
      $db->rollback();
      throw new CoreError($ex->getMessage() . " SQL: " . $lastSQL);
    }
  }

  public function finalizeMap($gmid, $screenshot, $snapshot) {
    try {
      $db = $this->getInstance('kb');
      $gmid = $db->escape($gmid);
      $screenshot = $db->escape($screenshot);
      // throw new Exception(var_dump(serialize($snapshot)));
      $snapshot = $db->escape($snapshot);
      // $snapshot = $db->escape($snapshot);
      $sql = "INSERT INTO `goalmaps_esb` (`gmid`, `state`, `screenshot`, `snapshot`) "
        . "VALUES ('$gmid', 'final', '$screenshot', '$snapshot') "
        . "ON DUPLICATE KEY UPDATE `state`='final', `screenshot`='$screenshot', `snapshot`='$snapshot' ";
      // throw new Exception($sql);
      $db->query($sql);
      return true;
    } catch (Exception $ex) {
      $lastSQL = $db->getLastQuery();
      throw new CoreError($ex->getMessage() . " SQL: " . $lastSQL);
    }
  }

  public function saveSnapshot($gmid, $screenshot, $snapshot) {
    try {
      $db = $this->getInstance('kb');
      $gmid = $db->escape($gmid);
      $screenshot = $db->escape($screenshot);
      // throw new Exception(var_dump(serialize($snapshot)));
      $snapshot = $db->escape($snapshot);
      // $snapshot = $db->escape($snapshot);
      $sql = "INSERT INTO `goalmaps_esb` (`gmid`, `state`, `screenshot`, `snapshot`) "
        . "VALUES ('$gmid', 'construct', '$screenshot', '$snapshot') "
        . "ON DUPLICATE KEY UPDATE `state`='construct', `screenshot`='$screenshot', `snapshot`='$snapshot' ";
      // throw new Exception($sql);
      $db->query($sql);
      return true;
    } catch (Exception $ex) {
      $lastSQL = $db->getLastQuery();
      throw new CoreError($ex->getMessage() . " SQL: " . $lastSQL);
    }
  }

  public function saveContent($mid, $content) {

    $db = $this->getInstance('kb');
    $mid = $db->escape($mid);

    $meta = new MaterialMeta(new Material());
    $meta->mid = $mid;
    $meta->content = $content;

    $qb = QB::instance($db, 'materials_meta');

    // throw new CoreError($mid . ":". $gmid . ":");

    try {
      $sql = "INSERT INTO `materials_meta` (`mid`, `content`) VALUES ('$mid', '" . $db->escape($content) . "') ";
      $sql .= "ON DUPLICATE KEY UPDATE `content` = '" . $db->escape($content) . "'";
      $db->query($sql);
      return true;
    } catch (Exception $ex) {
      $lastSQL = $db->getLastQuery();
      $db->rollback();
      throw new CoreError($ex->getMessage() . " SQL: " . $lastSQL);
    }
  }

  public function saveNlp($mid, $nlp) {

    $db = $this->getInstance('kb');
    $mid = $db->escape($mid);
    $meta = new MaterialMeta(new Material());
    $meta->mid = $mid;
    $meta->nlp = $nlp;

    $qb = QB::instance($db, 'materials_meta');

    // throw new CoreError($mid . ":". $gmid . ":");

    try {
      $sql = "INSERT INTO `materials_meta` (`mid`, `nlp`) VALUES ('$mid', '" . $db->escape($nlp) . "') ";
      $sql .= "ON DUPLICATE KEY UPDATE `nlp` = '" . $db->escape($nlp) . "'";
      $db->query($sql);
      return true;
    } catch (Exception $ex) {
      $lastSQL = $db->getLastQuery();
      $db->rollback();
      throw new CoreError($ex->getMessage() . " SQL: " . $lastSQL);
    }
  }

  public function saveCanvas($mid, $canvas) {

    $db = $this->getInstance('kb');
    $mid = $db->escape($mid);
    $meta = new MaterialMeta(new Material());
    $meta->mid = $mid;
    $meta->canvas = $canvas;

    $qb = QB::instance($db, 'materials_meta');
    try {
      $sql = "INSERT INTO `materials_meta` (`mid`, `nlp`) VALUES ('$mid', '" . $db->escape($canvas) . "') ";
      $sql .= "ON DUPLICATE KEY UPDATE `canvas` = '" . $db->escape($canvas) . "'";
      $db->query($sql);
      return true;
    } catch (Exception $ex) {
      $lastSQL = $db->getLastQuery();
      $db->rollback();
      throw new CoreError($ex->getMessage() . " SQL: " . $lastSQL);
    }
  }

  public function saveLearnerMaps($gmid, $uid, $finalmap, $concepts, $links) {

    $db = $this->getInstance('kb');
    // $mid = $db->escape($gmid);

    $lm = new Learnermap();
    $lm->gmid = $gmid;
    $lm->uid = $uid;
    $lm->finalmap = $finalmap;

    $qb = QB::instance($db, 'learnermaps');

    // throw new CoreError($mid . ":". $gmid . ":");

    try {
      $db->begin();

      if($finalmap) {
        $dsql = "DELETE FROM learnermaps 
                  WHERE gmid = '".$gmid."' AND uid = '".$uid."'";
        $db->query($dsql);
      }

      $db->query(
        $qb->table('learnermaps')
          ->insertModel($lm, array('gmid', 'uid', 'finalmap'))
          ->get()
      );
      $lmid = $db->getInsertId();

      foreach ($concepts as $c) {
        $c->lmid = $lmid;
      }

      foreach ($links as $l) {
        $l->lmid = $lmid;
      }

      // var_dump($concepts);exit;
      // $qb->clear();
      // $db->query(
      //   $qb->table('goalmaps_links')->delete()
      //   ->where('mid', $mid)->where('gmid', $gmid)
      //   ->get()
      // );
      // $qb->clear();
      // $db->query(
      //   $qb->table('goalmaps_concepts')->delete()
      //   ->where('mid', $mid)->where('gmid', $gmid)
      //   ->get()
      // );
      if (count($concepts)) {
        $qb->clear();
        $db->query(
          $qb->insertModel($concepts)->table('learnermaps_concepts')
            ->get()
        );
      }
      if (count($links)) {
        $qb->clear();
        $db->query(
          $qb->insertModel($links)->table('learnermaps_links')
            ->get()
        );
      }
      // throw new CoreError($qb->insertModel($links)->table('goalmaps_links')
      // ->get());
      
      $db->commit();
      return $lmid;
    } catch (Exception $ex) {
      $lastSQL = $db->getLastQuery();
      $db->rollback();
      throw new CoreError($ex->getMessage() . " SQL: " . $lastSQL);
    }
  }

  public function deleteLearnermap($lmid = null) {
    $db = $this->getInstance('kb');
    $qb = QB::instance($db);
    try {
      $db->query(
        $qb->table('learnermaps')
          ->delete()
          ->where('lmid', $lmid)
          ->get()
      );
    } catch (Exception $e) {
      throw $e;
    }
  }

  */

}

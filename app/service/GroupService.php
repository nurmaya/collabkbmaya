<?php

class GroupService extends CoreService {

  public function selectGroups() {
    $db     = $this->getInstance('kb-collab');
    $result = QB::instance($db)
      ->table('grups g')
      ->select(array('g.gid', 'g.fid', 'g.name', 'g.type', 'g.grade', 'g.class', 'g.note', 'g.creator_id', 'g.create_time'))
      ->executeQuery(true);
    return $result;
  }

  public function selectGroupsByIds($ids = []) {
    $db     = $this->getInstance('kb-collab');
    $result = QB::instance($db)
      ->table('grups g')
      ->select(array('g.gid', 'g.fid', 'g.name', 'g.type', 'g.grade', 'g.class', 'g.note', 'g.creator_id', 'g.create_time'))
      ->whereIn('g.gid', $ids)
      ->executeQuery(true);
    return $result;
  }

  public function selectGroupsWithCount() {
    $db     = $this->getInstance('kb-collab');
    $result = QB::instance($db)
      ->table('grups g')
      ->select(array('g.gid', 'g.fid', 'g.name', 'g.type', 'g.grade', 'g.class', 'g.note', 'g.creator_id', 'g.create_time'))
      ->selectRaw("(SELECT COUNT(*) FROM grups_has_qsets WHERE gid = g.gid) AS cqsets")
      ->selectRaw("(SELECT COUNT(*) FROM users_in_grups WHERE gid = g.gid) AS cusers")
      ->selectRaw("(SELECT COUNT(*) FROM grups_has_materials WHERE gid = g.gid) AS cmaterials")
      ->selectRaw("(SELECT COUNT(*) FROM rooms WHERE gid = g.gid) AS crooms")
      ->executeQuery(true);
    return $result;
  }

  public function getGroupById($gid) {
    $db     = $this->getInstance('kb-collab');
    $result = QB::instance($db)
      ->table('grups g')
      ->select(array('g.gid', 'g.fid', 'g.name', 'g.type', 'g.grade', 'g.class', 'g.note', 'g.creator_id', 'g.create_time'))
      ->where('gid', $gid)
      ->executeQuery(true);
    return $result;
  }

  public function getGroupByFid($fid) {
    $db     = $this->getInstance('kb-collab');
    $result = QB::instance($db)
      ->table('grups g')
      ->select(array('g.gid', 'g.fid', 'g.name', 'g.type', 'g.grade', 'g.class', 'g.note', 'g.creator_id', 'g.create_time'))
      ->where('fid', $fid)
      ->executeQuery(true);
    if (count($result)) {
      return $result[0];
    } else {
      return null;
    }

  }

  public function insertGroup($name, $fid, $type, $grade, $class, $note) {
    $db              = $this->getInstance('kb-collab');
    $groups['name']  = QB::esc($name);
    $groups['fid']   = QB::esc($fid);
    $groups['type']  = QB::esc($type);
    $groups['grade'] = QB::esc($grade);
    $groups['class'] = QB::esc($class);
    $groups['note']  = QB::esc($note);
    $qb              = QB::instance($db);
    try {
      $qb->table('grups')
        ->insert($groups)
        ->execute(true);
      return $qb->insertId();
    } catch (Exception $ex) {
      throw $ex;
    }
  }

  public function deleteGroup($gid) {
    $db = $this->getInstance('kb-collab');
    $qb = QB::instance($db)
      ->table('grups')
      ->delete()
      ->where('gid', QB::esc($gid))
      ->execute(true);
    return $qb->getAffectedRows();
  }

  public function updateGroup($gid, $name, $fid, $type, $grade, $class, $note) {
    $db              = $this->getInstance('kb-collab');
    $update['gid']   = QB::esc($gid);
    $update['name']  = QB::esc($name);
    $update['fid']   = QB::esc($fid);
    $update['type']  = QB::esc($type);
    $update['grade'] = QB::esc($grade);
    $update['class'] = QB::esc($class);
    $update['note']  = QB::esc($note);
    $qb              = QB::instance($db);
    try {
      $qb->table('grups')
        ->update($update)
        ->where('gid', QB::esc($gid))
        ->execute(true);
      return $qb->getAffectedRows();
    } catch (Exception $ex) {
      throw $ex;
    }
  }

// Qset Group

  public function getQsetGroup($gid) {
    $db        = $this->getInstance('kb-collab');
    $gid       = QB::esc($gid);
    $qb        = QB::instance($db);
    $qsets     = new stdClass;
    $qsets->in = $qb->table('question_sets q')
      ->selectRaw('q.qsid, q.name, q.type')
      ->whereRaw("q.qsid IN (SELECT gq.qsid FROM grups_has_qsets gq WHERE gq.gid = '$gid')")
      ->executeQuery(true);
    $qb->clear();
    $qsets->notin = $qb->table('question_sets q')
      ->selectRaw('q.qsid, q.name, q.type')
      ->whereRaw("q.qsid NOT IN (SELECT gq.qsid FROM grups_has_qsets gq WHERE gq.gid = '$gid')")
      ->executeQuery(true);
    return $qsets;
  }

  public function addQsetToGroup($qsid, $gid) {
    $db             = $this->getInstance('kb-collab');
    $qsid           = QB::esc($qsid);
    $gid            = QB::esc($gid);
    $qb             = QB::instance($db);
    $insert['qsid'] = $qsid;
    $insert['gid']  = $gid;
    $qb->table('grups_has_qsets gq')
      ->insert($insert, true)
      ->execute();
    return $qb->getAffectedRows();
  }

  public function removeQsetFromGroup($qsid, $gid) {
    $db   = $this->getInstance('kb-collab');
    $qsid = QB::esc($qsid);
    $gid  = QB::esc($gid);
    $qb   = QB::instance($db);
    $res  = $qb->table('grups_has_qsets gq')
      ->delete()
      ->where('qsid', $qsid)
      ->where('gid', $gid)
      ->execute();
    return $qb->getAffectedRows();
  }

  // User Group

  public function getUserGroup($gid) {
    $db        = $this->getInstance('kb-collab');
    $gid       = QB::esc($gid);
    $qb        = QB::instance($db);
    $users     = new stdClass;
    $users->in = $qb->table('users u')
      ->selectRaw('u.uid, u.name, u.username')
      ->whereRaw("u.uid IN (SELECT ug.uid FROM users_in_grups ug WHERE ug.gid = '$gid')")
      ->executeQuery(true);
    $qb->clear();
    $users->notin = $qb->table('users u')
      ->selectRaw('u.uid, u.name, u.username')
      ->whereRaw("u.uid NOT IN (SELECT ug.uid FROM users_in_grups ug WHERE ug.gid = '$gid')")
      ->executeQuery(true);
    return $users;
  }

  public function addUserToGroup($uid, $gid) {
    $db            = $this->getInstance('kb-collab');
    $uid           = QB::esc($uid);
    $gid           = QB::esc($gid);
    $qb            = QB::instance($db);
    $insert['uid'] = $uid;
    $insert['gid'] = $gid;
    $qb->table('users_in_grups ug')
      ->insert($insert, true)
      ->execute();
    return $qb->getAffectedRows();
  }

  public function removeUserFromGroup($uid, $gid) {
    $db  = $this->getInstance('kb-collab');
    $uid = QB::esc($uid);
    $gid = QB::esc($gid);
    $qb  = QB::instance($db);
    $res = $qb->table('users_in_grups ug')
      ->delete()
      ->where('uid', $uid)
      ->where('gid', $gid)
      ->execute();
    return $qb->getAffectedRows();
  }

  // Material Group

  public function getMaterialGroup($gid) {
    $db            = $this->getInstance('kb-collab');
    $gid           = QB::esc($gid);
    $qb            = QB::instance($db);
    $materials     = new stdClass;
    $materials->in = $qb->table('materials m')
      ->selectRaw('m.mid, m.name')
      ->whereRaw("m.mid IN (SELECT gm.mid FROM grups_has_materials gm WHERE gm.gid = '$gid')")
      ->executeQuery(true);
    $qb->clear();
    $materials->notin = $qb->table('materials m')
      ->selectRaw('m.mid, m.name')
      ->whereRaw("m.mid NOT IN (SELECT gm.mid FROM grups_has_materials gm WHERE gm.gid = '$gid')")
      ->executeQuery(true);
    return $materials;
  }

  public function addMaterialToGroup($mid, $gid) {
    $db            = $this->getInstance('kb-collab');
    $mid           = QB::esc($mid);
    $gid           = QB::esc($gid);
    $qb            = QB::instance($db);
    $insert['mid'] = $mid;
    $insert['gid'] = $gid;
    $qb->table('grups_has_materials gm')
      ->insert($insert, true)
      ->execute();
    return $qb->getAffectedRows();
  }

  public function removeMaterialFromGroup($mid, $gid) {
    $db  = $this->getInstance('kb-collab');
    $mid = QB::esc($mid);
    $gid = QB::esc($gid);
    $qb  = QB::instance($db);
    $qb->table('grups_has_materials gm')
      ->delete()
      ->where('mid', $mid)
      ->where('gid', $gid)
      ->execute();
    return $qb->getAffectedRows();
  }

  // Room Group

  public function getRoomGroup($gid) {
    $db        = $this->getInstance('kb-collab');
    $gid       = QB::esc($gid);
    $qb        = QB::instance($db);
    $rooms     = new stdClass;
    $rooms->in = $qb->table('rooms r')
      ->select(array('rid', 'name'))
      ->where('r.gid', $gid)
      ->executeQuery(true);
    $qb->clear();
    $rooms->notin = $qb->table('rooms r')
      ->select(array('rid', 'name'))
      ->whereRaw("r.gid IS NULL")
      ->executeQuery(true);
    return $rooms;
  }

  public function addRoomToGroup($rid, $gid) {
    $db            = $this->getInstance('kb-collab');
    $rid           = QB::esc($rid);
    $gid           = QB::esc($gid);
    $qb            = QB::instance($db);
    $update['gid'] = $gid;
    $qb->table('rooms')
      ->update($update)
      ->where('rid', $rid)
      ->execute();
    return $qb->getAffectedRows();
  }

  public function removeRoomFromGroup($rid, $gid) {
    $db            = $this->getInstance('kb-collab');
    $rid           = QB::esc($rid);
    $qb            = QB::instance($db);
    $update['gid'] = null;
    $qb->table('rooms')
      ->update($update)
      ->where('rid', $rid)
      ->execute();
    return $qb->getAffectedRows();
  }
}

<?php

class MessagesService extends CoreService {

  public function loadMessages($rid) {
    $db      = $this->getInstance('kb-collab');
    $qb      = QB::instance($db);
    $result  = new stdClass;
    $messages = $qb->table('messages m')
      ->leftJoin('users u', 'm.uid', 'u.uid')
      ->select(array('m.mid', 'm.message', 'm.mdate', 'm.rid', 'm.uid', 'u.username', 'u.name'))
      ->where('m.rid', $rid)
      ->whereNotIn('m.mid', QB::raw('(SELECT mid FROM messages_channel)'))
      // ->get();
      ->executeQuery(true);
    return $messages;
  }

}